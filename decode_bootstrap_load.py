import os
import pickle
from constants import *
from utils import *
import matplotlib.pyplot as plt
from plotting_style import *
from collections import OrderedDict
import pandas as pd

settings_name = 'aug23'
decoder       = 'dkw'


# plotting parameters
require_for_mean = 7
plot_individual_sessions = False
save_plots = True
plot_format = 'svg'
dpi = 400
plot_folder = os.path.join(DATA_FOLDER, 'plots', 'decode_bootstrap')

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

output_file_name = 'decode_bootstrap_{}_{}.pkl'.format(settings_name, decoder)
output_folder = os.path.join(DATA_FOLDER, 'results', 'dec')
output_full_path = os.path.join(output_folder, output_file_name)

results = pickle.load(open(output_full_path, 'rb'))
rs = results['decoding_results']
pars = results['decoding_pars']

binsize_in_ms = pars['binsize_in_ms']
min_group_size = pars['min_group_size']
group_size_step = pars['group_size_step']


if plot_individual_sessions:
    for sess_ind in GOOD_SESSIONS_BOTH_AREAS:

        f, ax = plt.subplots(1, 1, figsize=[3, 3])
        for area in ['A1', 'V2L']:
            x = rs[area][sess_ind]['group_sizes']
            y = rs[area][sess_ind]['error'].mean(axis=1)
            err = rs[area][sess_ind]['error'].std(axis=1)
            ax.plot(x, y, c=area_colormap[area],label=area)
            ax.scatter(x, y, c=area_colormap[area], s=14)
            ax.fill_between(x, y - err, y + err, alpha=0.2, color=area_colormap[area])
            ax.legend(loc='upper right', frameon=False)

        ax.set_xticks(np.arange(min_group_size, ax.get_xlim()[1], 5))
        ax.set_ylabel('Average error (m)')
        ax.set_xlabel('# neurons')
        plt.tight_layout()
        sns.despine()

        if save_plots:
            if isinstance(sess_ind, int):
                plot_name = 'decode_bootstrap_w_{}_{}ms_session_{:02}.{}'.format(decoder, binsize_in_ms,
                                                                              sess_ind, plot_format)
            f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)




f, ax = plt.subplots(1, 1, figsize=[3, 3])

for area in ['A1', 'V2L']:

    if require_for_mean == 'all':
        min_units = np.min([rs[area][sess_ind]['group_sizes'].max() for sess_ind in GOOD_SESSIONS_BY_AREA[area]])
        x_all = np.arange(min_group_size, min_units+1, group_size_step)

        y_all = np.hstack([rs[area][sess_ind]['error'][0:x_all.shape[0], :]
                           for sess_ind in GOOD_SESSIONS_BY_AREA[area]]).mean(axis=1)

    #this is to plot the mean as long as there are still groups

    elif isinstance(require_for_mean, int):
        n_units = np.hstack([rs[area][sess_ind]['group_sizes'] for sess_ind in GOOD_SESSIONS_BY_AREA[area]])
        vc = pd.value_counts(n_units)

        max_units = vc.index[vc >= require_for_mean].max()
        x_all = np.arange(min_group_size, max_units+1, group_size_step)
        y_all = np.zeros([len(GOOD_SESSIONS_BY_AREA[area]), x_all.shape[0]])
        y_all[:, :] = np.nan

        errors = [rs[area][sess_ind]['error'].mean(axis=1) for sess_ind in GOOD_SESSIONS_BY_AREA[area]]
        print([err.shape for err in errors])

        for jj, err in enumerate(errors):
            y_all[jj, 0:min(err.shape[0], x_all.shape[0])] = err[0:x_all.shape[0]]
        y_all = np.nanmean(y_all, axis=0)

    ax.plot(x_all, y_all, c=area_colormap[area], label=area, lw=3, alpha=1, zorder=10)

    for sess_ind in GOOD_SESSIONS_BY_AREA[area]:
        x = rs[area][sess_ind]['group_sizes']
        y = rs[area][sess_ind]['error'].mean(axis=1)
        ax.plot(x, y, c=area_colormap[area], lw=0.8, alpha=0.8)

handles, labels = plt.gca().get_legend_handles_labels()
by_label = OrderedDict(zip(labels, handles))
ax.legend(by_label.values(), by_label.keys(), loc='upper right',frameon=False)
sns.despine()
ax.set_xticks(np.arange(min_group_size, ax.get_xlim()[1], 5))
sns.despine()
ax.set_ylabel('Average error (m)')
ax.set_xlabel('# neurons')
plt.tight_layout()

if save_plots:
    if isinstance(sess_ind, int):
        plot_name = 'decode_bootstrap_w_{}_{}ms_multiplesessions.{}'.format(decoder, binsize_in_ms,
                                                                      plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)