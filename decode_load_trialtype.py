import os
import pickle
from constants import *
from utils import *
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
import matplotlib.gridspec as gridspec
from sklearn.preprocessing import minmax_scale
from plotting_style import *
from scipy.ndimage.filters import gaussian_filter, gaussian_filter1d
import pandas as pd
from scipy.stats.stats import pearsonr, ttest_rel
from scipy.stats import wilcoxon, mannwhitneyu


global_setting = 'dec14newtrials'

# PLOT 1
settings_names = ['dec6_all',
                  'dec6_visual',
                  'dec6_auditory',
                  'dec6_multisensory',
                  'dec6_correct',
                  'dec6_incorrect']

settings_names = ['dec14newtrials_all',
                  'dec14newtrials_visual',
                  'dec14newtrials_auditory',
                  'dec14newtrials_multisensory',
                  'dec14newtrials_correct',
                  'dec14newtrials_incorrect']

settings_labels = ['All trials',
                  'Visual',
                  'Auditory',
                  'Multisensory',
                  'Correct',
                  'Incorrect']

# PLOT 2
# settings_names = ['dec14newtrials_auditory_high',
#                   'dec14newtrials_auditory_low',
#                   'dec14newtrials_visual_high',
#                   'dec14newtrials_visual_low',
#                   'dec14newtrials_previous_left',
#                   'dec14newtrials_previous_right']
#
# settings_labels = ['Auditory (high)',
#                    'Auditory (low)',
#                    'Visual (high)',
#                    'Visual (low)',
#                    'Left previous',
#                    'Right previous']




decoder        = 'dkw'

# plotting parameters
smooth_confusion_matrices=False
smooth_error_by_bin=True
sigma_smooth_cm = 1
sigma_smooth_err_by_bin=2
running_mean_prediction_plot=False
n_seconds_pred_plot = 60
save_plots = True
plot_format = 'png'
dpi = 400
n_bootstraps = 100

only_one_animal = False
select_animal = 'animal3'

plot_folder = os.path.join(DATA_FOLDER, 'plots', 'decode',
                           '{}_{}'.format(global_setting, decoder))

if only_one_animal:
    plot_folder = os.path.join(DATA_FOLDER, 'plots', 'decode'
                           '{}_{}'.format(global_setting, decoder), select_animal)


if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

use_maze_distance=False

if use_maze_distance:
    error_string = 'maze_error'
else:
    error_string = 'error'


bin_distance_file = os.path.join(DATA_FOLDER, 'bin_distance.pkl')
bin_distance = pickle.load(open(bin_distance_file, 'rb'))


# VERSION 1 WITH BOOTSTRAPS AND DIAGONAL OF CONFUSION MATRIX

dr = pd.DataFrame(columns=['settings_name', 'label', 'area', 'session','animal', 'mean_error'])

for settings_name, label in zip(settings_names, settings_labels):

    results_file_name = 'decode_{}_{}.pkl'.format(settings_name, decoder)
    results_path = os.path.join(DATA_FOLDER, 'results', 'dec', results_file_name)
    results = pickle.load(open(results_path, 'rb'))

    df_full = results['decoding_results']
    pars    = results['decoding_pars']

    n_samp = df_full.shape[0]

    bin_locations = bin_distance[0]['bins']

    for area in AREAS:

        sessions = GOOD_SESSIONS_BY_AREA[area]
        if only_one_animal:
            print(sessions)
            sessions = [s for s in sessions if np.isin(s, sessions_by_animal[select_animal])]
            print(sessions)
        df = df_full[np.isin(df_full['session'], sessions)]
        #df = df_full[np.isin(df_full['session'], 13)]
        y = df['bin'].astype(int).__array__()
        y_pred = df['{}_pred'.format(area)].astype(int).__array__()

        df_sel = df_full.groupby('session').mean()['{}_error'.format(area)]

        for sess, err in df_sel.items():
            anim = animals_by_session[sess]
            dr.loc[dr.shape[0], :] = [settings_name, label, area, sess, anim, err]

for col in ['mean_error']:
    dr[col] = pd.to_numeric(dr[col])

dr.groupby('settings_name').mean()

f, ax = plt.subplots(1, 1, figsize=[5, 4])
sns.barplot(data=dr, x='label', y='mean_error', hue='area',
            palette=area_colormap)
ax.set_xticklabels(labels=settings_labels, rotation=70)
ax.set_xlabel('')
ax.set_ylabel('Mean error (m)')
ax.get_legend().remove()
sns.despine()
plt.tight_layout()

if save_plots:
    plot_name = 'mean_error_simple_{}.{}'.format(global_setting, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)

f, ax = plt.subplots(1, 1, figsize=[5, 4])
sns.barplot(data=dr, x='label', y='mean_error', hue='area',
            palette=area_colormap)
sns.swarmplot(data=dr, x='label', y='mean_error', hue='area',
              marker='o', palette={'A1':sns.xkcd_rgb['dark grey'],
                                   'V2L' : sns.xkcd_rgb['dark grey']}, dodge=2)
# sns.swarmplot(data=dr[dr['animal'] == '1'], x='label', y='mean_error', hue='area',
#               marker='D', color='k', dodge=1)
# sns.swarmplot(data=dr[dr['animal'] == '2'], x='label', y='mean_error', hue='area',
#               marker='s', color='k', dodge=1)
# sns.swarmplot(data=dr[dr['animal'] == '3'], x='label', y='mean_error', hue='area',
#               marker='o', color='k', dodge=1)
#ax.legend().remove()
#ax.set_xticks(np.arange(y.__len__())[::2]+0.5)
ax.set_xticklabels(labels=settings_labels, rotation=70)
ax.set_xlabel('')
ax.set_ylabel('Mean error (m)')
ax.get_legend().remove()
sns.despine()
plt.tight_layout()

if save_plots:
    plot_name = 'mean_error_{}.{}'.format(global_setting, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)

animal_colormap = {'1' : sns.xkcd_rgb['black'],
                   '2' : sns.xkcd_rgb['dark grey'],
                   '3' : sns.xkcd_rgb['grey']}

for area in AREAS:
    f, ax = plt.subplots(1, 1, figsize=[5, 4])
    dr_area = dr[dr['area']==area]
    sns.barplot(data=dr_area, x='label', y='mean_error',
                color=area_colormap[area])
    sns.swarmplot(data=dr[dr['area']==area], x='label', y='mean_error', hue='animal',
                   marker='o', palette=animal_colormap, dodge=0)
    # sns.swarmplot(data=dr_area[dr_area['animal'] == '1'], x='label', y='mean_error',
    #               marker='D', color='k', dodge=1)
    # sns.swarmplot(data=dr_area[dr_area['animal'] == '2'], x='label', y='mean_error',
    #               marker='s', color='k', dodge=1)
    # sns.swarmplot(data=dr_area[dr_area['animal'] == '3'], x='label', y='mean_error',
    #               marker='o', color='k', dodge=1)
    #ax.legend().remove()
    #ax.set_xticks(np.arange(y.__len__())[::2]+0.5)
    ax.set_xticklabels(labels=settings_labels, rotation=70)
    ax.set_xlabel('')
    ax.set_ylabel('Mean error (m)')
    ax.get_legend().remove()
    sns.despine()
    plt.tight_layout()

    if save_plots:
        plot_name = 'mean_error_per_area_{}_{}.{}'.format(area, global_setting, plot_format)
        f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)




# VERSION 2 WITH BOOTSTRAPS AND DIAGONAL OF CONFUSION MATRIX
if False:
    dr = pd.DataFrame(columns=['settings_name', 'label', 'area', 'n_samp', 'boot_indx', 'cm_trace'])

    for settings_name, label in zip(settings_names, settings_labels):

        results_file_name = 'decode_{}_{}.pkl'.format(settings_name, decoder)
        results_path = os.path.join(DATA_FOLDER, 'results', 'dec', results_file_name)
        results = pickle.load(open(results_path, 'rb'))

        df_full = results['decoding_results']
        pars    = results['decoding_pars']

        n_samp = df_full.shape[0]

        bin_locations = bin_distance[0]['bins']

        if pars['coarsify_bins']:
            coarse_bins = np.arange(bin_locations.index[0],
                                    bin_locations.index[-1] + pars['coarse_bin_size'],
                                    pars['coarse_bin_size'])
            bin_locations['coarse_bins'] = pd.cut(bin_locations.index, coarse_bins,
                                                  labels=False, right=False,
                                                  include_lowest=True)
            bin_locations = bin_locations.groupby('coarse_bins').mean()


        # --- PREPARE CONFUSION MATRICES ----------------------------------------------

        for area in AREAS:

            sessions = GOOD_SESSIONS_BY_AREA[area]
            if only_one_animal:
                print(sessions)
                sessions = [s for s in sessions if np.isin(s, sessions_by_animal[select_animal])]
                print(sessions)
            df = df_full[np.isin(df_full['session'], sessions)]
            #df = df_full[np.isin(df_full['session'], 13)]
            y = df['bin'].astype(int).__array__()
            y_pred = df['{}_pred'.format(area)].astype(int).__array__()


            # BOOTSTRAPS BINS
            cm = confusion_matrix(y, y_pred)
            cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

            if smooth_confusion_matrices:
                cm = gaussian_filter(cm, sigma=sigma_smooth_cm)

            for n in range(n_bootstraps):
                indx = np.random.choice(np.arange(cm.shape[0]), size=cm.shape[0], replace=True)
                cm_trace = cm.diagonal()[indx].sum()
                dr.loc[dr.shape[0], :] = [settings_name, label, area, n_samp, n, cm_trace]


    dr['n_samp'] = pd.to_numeric(dr['n_samp'])
    dr.groupby('settings_name').mean()

    y = []
    err = []
    for settings_name in settings_names:
        for area in AREAS:
            #print('\n{} {}'.format(match_with, phase))
            stats = dr[(dr['settings_name'] == settings_name) & (dr['area'] == area)]['cm_trace']
            obs_val = stats.median()
            y.append(obs_val)
            alpha = 0.95
            # p = ((1.0 - alpha) / 2.0) * 100
            # lower = max(0.0, np.percentile(stats, p))
            # p = (alpha + ((1.0 - alpha) / 2.0)) * 100
            # upper = min(1.0, np.percentile(stats, p))

            lower, upper = np.percentile(stats, [2.5, 97.5])
            err.append([obs_val - lower, upper - obs_val])


    f, ax = plt.subplots(1, 1, figsize=[5, 4])

    ax.bar(x=np.arange(y.__len__()),
           height=y,
           yerr=np.array(err).T,
           linewidth=2.2,
           ecolor='0.2', alpha=0.8)

    for i, patch in enumerate(ax.patches) :
        if i % 2 == 0 :
            #patch.set_edgecolor(sns.xkcd_rgb['grey'])
            patch.set_facecolor(area_colormap['A1'])

        else :
            #patch.set_edgecolor(sns.xkcd_rgb['grey'])
            patch.set_facecolor(area_colormap['V2L'])
    #ax.legend().remove()
    ax.set_xticks(np.arange(y.__len__())[::2]+0.5)
    ax.set_xticklabels(labels=settings_labels, rotation=70)
    ax.set_xlabel('')
    ax.set_ylabel('Decoding quality')
    sns.despine()
    plt.tight_layout()

    if save_plots:
        plot_name = 'cm_trace_{}.{}'.format(global_setting, plot_format)
        f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)

    # PLOT NUMBER OF SAMPLES
    f, ax = plt.subplots(1, 1, figsize=[5, 4])
    sns.barplot(data=dr, x='label', y='n_samp', ax=ax,
                ci='sd', color=sns.xkcd_rgb['grey'])
    ax.set_xticklabels(labels=ax.get_xticklabels(), rotation=70)
    ax.set_xlabel('')
    ax.set_ylabel('# of samples')
    sns.despine()
    plt.tight_layout()

    if save_plots:
        plot_name = 'n_samples_{}.{}'.format(global_setting, plot_format)
        f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
