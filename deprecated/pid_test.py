import numpy as np


n = 100
alph = 20
x = np.random.randint(0, alph, n)
y = np.random.randint(0, alph, n)
z = np.random.randint(0, alph, n)
data = Data(np.vstack((x, y, z)), 'ps', normalise=False)
settings = {
    'lags_pid': [[0, 0]],
    'alpha': 0.1,
    'alph_s1': alph,
    'alph_s2': alph,
    'alph_t': alph,
    'max_unsuc_swaps_row_parm': 60,
    'num_reps': 63,
    'max_iters': 1000,
    'pid_estimator': 'SydneyPID'}
targets = [2]
sources = [[0, 1]]
pid_analysis = PartialInformationDecomposition()
results = pid_analysis.analyse_network(settings, data, targets,
                                       sources)

res = results.get_single_target(2)
mutual = res['joint_mi_s1s2_t']
redundant = res['shd_s1_s2']
synergy = res['syn_s1_s2']
unique_1 = res['unq_s1']
unique_2 = res['unq_s2']

assert redundant + synergy + unique_1 + unique_2 == mutual

from pidpy import PIDCalculator
pid = PIDCalculator(np.vstack((x, y)).T, z)

dec = pid.decomposition(debiased=False, n=1,
                               as_percentage=False,
                               return_individual_unique=True,
                               test_significance=False)