
import xgboost as xgb
import numpy as np

X = np.random.randint(0, 30, size=[1000, 5]).astype(float)
Y = np.random.randint(0, 10, size=[100, 1])


dtrain = xgb.DMatrix(X, label=Y)
num_round = 200
model = xgb.train(dtrain, num_round)
