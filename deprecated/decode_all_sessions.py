import os
import numpy as np
import elephant
import neo
import pickle
from constants import *
from utils import *
import quantities as pq
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from prepare_decoding_data import PrepareDecodingData
import pandas as pdd
import matplotlib.gridspec as gridspec
from plotting_style import *
from scipy.ndimage.filters import gaussian_filter

sessions = ALL_SESSIONS
min_spikes = 5
min_speed = 0.1
decoder = 'dkw'
n_folds = 5
binsize_in_ms = 300
coarsify_bins=False
coarse_bin_size=5
smooth_confusion_matrices=True
sigma_smooth_cm = 2
error_correlation_plot = 'heatmap' #heatmaps or scatter
save_plots = True
plot_format = 'png'
dpi = 400
plot_folder = os.path.join(DATA_FOLDER, 'plots', 'decode_two_areas_all_sessions')

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

rs = {}

for sess_ind in sessions:
    # --- LOAD DATA -----------------------------------------------------------
    file_name = 'navig8_session_{}.pkl'.format(sess_ind)
    bl = pickle.load(open(os.path.join(DATA_FOLDER, file_name), 'rb'))
    segment = bl.segments[0]
    A1_ind = [i for i, t in enumerate(segment.spiketrains)
                       if t.annotations['area'] == 'A1']
    V2L_ind = [i for i, t in enumerate(segment.spiketrains)
                       if t.annotations['area'] == 'V2L']
    lin_speed = get_analogsignal_by_name(segment, 'lin_speed')
    lin_bin = get_analogsignal_by_name(segment, 'lin_bin')

    # --- PREPARE DATA FOR DECODING -------------------------------------------

    pdd = PrepareDecodingData(spike_trains=segment.spiketrains,
                              position=lin_bin.__array__()[:, 0],
                              speed=lin_speed.__array__()[:, 0],
                              times=lin_bin.times.rescale(pq.ms))

    pdd.bin_spikes(binsize_in_ms=binsize_in_ms)
    pdd.interpolate_position_and_speed(linearized_position=True)

    pdd.filter_nans()
    pdd.filter_min_speed(min_speed)
    pdd.filter_min_spikes(min_spikes)
    if coarsify_bins:
        pdd.coarsify_bins(new_bin_size=coarse_bin_size)

    X = pdd.binned_spikes
    y = pdd.interp_position
    z = pdd.interp_speed

    # --- DECODE --------------------------------------------------------------

    rs[sess_ind] = {}

    for area, unit_ind in zip(['A1', 'V2L'], [A1_ind, V2L_ind]):

        kf = KFold(n_splits=n_folds, shuffle=False)

        y_true_all, y_pred_all, z_true_all = [], [], []
        for fold, (training_ind, testing_ind) in enumerate(kf.split(X)):
            print('Decoding fold {}'.format(fold))

            X_train = X[training_ind, :]
            X_test  = X[testing_ind, :]

            X_train = X_train[:, unit_ind]
            X_test = X_test[:, unit_ind]

            y_train = y[training_ind]
            y_test  = y[testing_ind]

            z_train = y[training_ind]
            z_test  = y[testing_ind]

            if decoder == 'random_forest':
                dec = RandomForestClassifier(n_estimators=100)

            elif decoder == 'dkw':
                dec = DKW_decoder(bin_size_train=binsize_in_ms,
                                  bin_size_test=binsize_in_ms)

            elif decoder == 'logistic_regression':
                dec = LogisticRegression()

            dec.fit(X_train, y_train)

            y_pred = dec.predict(X_test)

            y_true_all.append(y_test)
            y_pred_all.append(y_pred)
            z_true_all.append(z_test)

        y_true_all = np.hstack(y_true_all)
        y_pred_all = np.hstack(y_pred_all)
        z_true_all = np.hstack(z_true_all)
        cm = confusion_matrix(y_true_all.astype(int), y_pred_all.astype(int))
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

        rs[sess_ind][area] = {}
        rs[sess_ind][area]['cm'] = cm
        rs[sess_ind][area]['y_true_all'] = y_true_all
        rs[sess_ind][area]['y_pred_all'] = y_pred_all
        rs[sess_ind][area]['z_true_all'] = z_true_all




def plot_two_confusion_matrices(cm_a1, cm_v2l, smooth=False, sigma=1):
    f = plt.figure(figsize=(6, 3))

    ax1 = plt.subplot2grid((3, 4), (0, 0), colspan=2, rowspan=2)
    ax2 = plt.subplot2grid((3, 4), (0, 2), colspan=2, rowspan=2)
    #ax3 = plt.subplot2grid((4, 4), (2, 0), colspan=4, rowspan=2)

    if smooth:
        cm_a1 = gaussian_filter(cm_a1, sigma=sigma)
        cm_v2l = gaussian_filter(cm_v2l, sigma=sigma)

    vmin = np.hstack((cm_a1, cm_v2l)).min()
    vmax = np.hstack((cm_a1, cm_v2l)).max()
    cax1 = ax1.imshow(cm_a1, cmap='Blues', vmin=vmin, vmax=vmax)
    cax2 = ax2.imshow(cm_v2l, cmap='Blues', vmin=vmin, vmax=vmax)
    cbar = plt.colorbar(cax2, ax=ax2, pad=0.2,
                        orientation='vertical', label='Prob. density')
    ax1.set(adjustable='box-forced', aspect='equal')


    ax1.set_yticks(ax1.get_xticks()[1: -1])
    ax2.set_yticks(ax2.get_xticks()[1: -1])


    ax1.set_title('A1')
    ax2.set_title('V2L')
    ax1.set_xlabel('Decoded bin')
    ax1.set_ylabel('True bin')
    ax2.set_xlabel('Decoded bin')
    sns.despine()
    plt.tight_layout()
    return f


# cm_a1 = np.sum([rs[i]['A1']['cm'] for i in sessions], axis=0)
# cm_v2l = np.sum([rs[i]['V2L']['cm'] for i in sessions], axis=0)
# f = plot_two_confusion_matrices(cm_a1, cm_v2l)
#
# if save_plots:
#     plot_name = 'dec_all_sessions_cm_averaged_w_{}_{}ms_session_{}.{}'.format(decoder, binsize_in_ms,
#                                                                      sess_ind, plot_format)
#     f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
#


true_a1 = np.hstack([rs[i]['A1']['y_true_all'] for i in sessions])
true_v2l = np.hstack([rs[i]['V2L']['y_true_all'] for i in sessions])
pred_a1 = np.hstack([rs[i]['A1']['y_pred_all'] for i in sessions])
pred_v2l = np.hstack([rs[i]['V2L']['y_pred_all'] for i in sessions])

cm_a1 = confusion_matrix(true_a1.astype(int), pred_a1.astype(int))
cm_a1 = cm_a1.astype('float') / cm_a1.sum(axis=1)[:, np.newaxis]

cm_v2l = confusion_matrix(true_v2l.astype(int), pred_v2l.astype(int))
cm_v2l = cm_v2l.astype('float') / cm_v2l.sum(axis=1)[:, np.newaxis]

f = plot_two_confusion_matrices(cm_a1, cm_v2l, smooth=smooth_confusion_matrices,
                                sigma=sigma_smooth_cm)

if save_plots:
    plot_name = 'dec_all_sessions_cm_from_stacked_pred_w_{}_{}ms_session_{}.{}'.format(decoder, binsize_in_ms,
                                                                     sess_ind, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



y_true = np.hstack([rs[i]['A1']['y_true_all'] for i in sessions])
#speed = rs['A1']['z_true_all']

y_pred_a1 =  np.hstack([rs[i]['A1']['y_pred_all'] for i in sessions])
y_pred_v2l = np.hstack([rs[i]['V2L']['y_pred_all'] for i in sessions])


n_rows, n_cols = 4, 5
f, axes = plt.subplots(n_rows, n_cols, figsize=[n_cols*2, n_rows*2], sharex=True, sharey=True)

locations = np.random.choice(np.unique(y_true), size=axes.shape[0]*axes.shape[1],
                             replace=False)

all_errors = np.hstack((y_pred_v2l - y_true, y_pred_a1 - y_true))
clip = [all_errors.min(), all_errors.max()]

for i, loc in enumerate(locations):
    ax = axes.flatten()[i]
    ax.set_facecolor('#eef5fc')

    ax.set(adjustable='box-forced', aspect='equal')
    ax.set_title('Linearized bin {}'.format(loc))
    ind = y_true == loc
    xx = y_pred_v2l[ind] - y_true[ind]
    yy = y_pred_a1[ind] - y_true[ind]

    if error_correlation_plot == 'heatmap':
        p = sns.kdeplot(xx, yy, ax=ax, shade=True, clip=[clip, clip], cmap='Blues')
    elif error_correlation_plot == 'scatter':
        ax.scatter(xx, yy, edgecolor='k', facecolor='w', zorder=5)

    #ax.axvline(loc, ls=':', color='k', alpha=0.8)
    #ax.axhline(loc, ls=':', color='k', alpha=0.8)
    ax.plot([0, 1], [0, 1], transform=ax.transAxes, ls='--', color='k', alpha=0.8)
    sns.despine()
for ax in axes[:, 0]:
    ax.set_ylabel('A1 error (bins)')
for ax in axes[-1, :]:
    ax.set_xlabel('V2L error (bins)')
plt.tight_layout()

if save_plots:
    plot_name = 'error_corr_two_areas_w_{}_{}ms_session_{}.{}'.format(decoder, binsize_in_ms,
                                                                  sess_ind, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)