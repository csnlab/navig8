import os
import numpy as np
import elephant
import neo
import pickle
from constants import *
from utils import *
import quantities as pq
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from prepare_decoding_data import PrepareDecodingData
from MLencoding import MLencoding
import pandas as pd
from mpl_toolkits.mplot3d import Axes3D
from plotting_style import area_colormap
import pickle
from info_utils import compute_info
import math
from pidpy import PIDCalculator
from idtxl.partial_information_decomposition import PartialInformationDecomposition
from idtxl.data import Data

# encoding parameters

settings_name = 'set5b'
sessions = ALL_SESSIONS
min_speed = 0.1
# TODO DO WE WANT TO FILTER SPEED HERE??
coarsify_bins = True
coarse_bin_size=5
binsize_in_ms = 300
n_condvar_bins = 20
n_surr = 100
condvar_bins_match_pos = True

pars = {'setting_name'  : settings_name,
        'sessions' : sessions,
        'min_speed' : min_speed,
        'binsize_in_ms' : binsize_in_ms,
        'coarsify_bins' : coarsify_bins,
        'coarsify_bin_size' : coarse_bin_size,
        'n_condvar_bins' : n_condvar_bins,
        'condvar_bins_match_pos' : condvar_bins_match_pos,
        'n_surr' : n_surr}


output_file_name   = 'pid_{}.pkl'.format(settings_name)
output_folder      = os.path.join(DATA_FOLDER, 'results', 'pid')
output_full_path   = os.path.join(output_folder, output_file_name)



if not os.path.isdir(output_folder):
    os.makedirs(output_folder)

# --- COMPUTE -----------------------------------------------------------------

df_full = pd.DataFrame(columns=['session', 'area', 'unit_id', 'unit_ind_sess',
                                'unit_ind_area', 'measure', 'value',
                                'pval'])
for sess_ind in sessions:

    file_name = 'navig8_session_{}.pkl'.format(sess_ind)
    bl = pickle.load(open(os.path.join(DATA_FOLDER, file_name), 'rb'))
    segment = bl.segments[0]
    A1_ind = [i for i, t in enumerate(segment.spiketrains)
              if t.annotations['area'] == 'A1']
    V2L_ind = [i for i, t in enumerate(segment.spiketrains)
               if t.annotations['area'] == 'V2L']
    xy_speed = get_analogsignal_by_name(segment, 'xy_speed')
    lin_speed = get_analogsignal_by_name(segment, 'lin_speed')
    lin_bin = get_analogsignal_by_name(segment, 'lin_bin')
    xy_pos = get_analogsignal_by_name(segment, 'xy_pos')
    hd_sincos = get_analogsignal_by_name(segment, 'hd_sincos')
    hd_change = get_analogsignal_by_name(segment, 'hd_change')

    lin_bin_arr = lin_bin.__array__()[:, 0]
    xy_pos_arr = xy_pos.__array__()
    lin_speed_arr = lin_speed.__array__()[:, 0]
    xy_speed_arr = xy_speed.__array__()[:, 0]
    hd_sincos_arr = hd_sincos.__array__()
    hd_change_arr = hd_change.__array__()

    # TODO: use linear speed or 2d speed??
    pdd = PrepareDecodingData(spike_trains=segment.spiketrains,
                              position=xy_pos_arr,
                              linearized_position=lin_bin_arr,
                              speed=xy_speed_arr,
                              head_direction_sincos=hd_sincos_arr,
                              head_direction_sincos_change=hd_change_arr,
                              times=lin_bin.times.rescale(pq.ms))

    if coarsify_bins:
        pdd.coarsify_bins(new_bin_size=coarse_bin_size)

    pdd._build_distance_matrix()
    pdd.bin_spikes(binsize_in_ms=binsize_in_ms)
    pdd.interpolate_position_and_speed()

    pdd.filter_nans()
    pdd.filter_min_speed(min_speed)

    binned_spikes = pdd.binned_spikes
    X_linpos = pdd.interp_linearized_position
    X_speed = np.abs(pdd.interp_speed)

    X_hd_angle = pdd.interp_head_direction
    X_hd_angle_change = pdd.interp_head_direction_change

    if condvar_bins_match_pos:
        n_condvar_bins = np.unique(X_linpos).shape[0]
        print('Automatically setting the number of bins'
              'for the variables to {}'.format(n_condvar_bins))

    X_speed = pd.cut(X_speed.flatten(), bins=n_condvar_bins, labels=False).astype(int)
    X_hd_angle = pd.cut(X_hd_angle.flatten(), bins=n_condvar_bins, labels=False).astype(int)
    X_hd_angle_change = pd.cut(X_hd_angle_change.flatten(), bins=n_condvar_bins, labels=False).astype(int)
    X_linpos = X_linpos.astype(int)

    # TODO important - 1
    if not coarsify_bins:
        X_linpos = X_linpos.astype(int) - 1



    for area, area_unit_ind in zip(['A1', 'V2L'], [A1_ind, V2L_ind]):

        for kk, unit_ind in enumerate(area_unit_ind):

            unit_id = '{:02}_{}_{:02}'.format(sess_ind, area, unit_ind)
            y = binned_spikes[:, unit_ind].astype(int)
            print('\n\n>>> Analysing unit {}\n'.format(unit_id))

            # pid = PIDCalculator(X, y)
            #
            # dec, pvals = pid.decomposition(debiased=True, n=n_surr,
            #                                as_percentage=False,
            #                                return_individual_unique=True,
            #                                test_significance=True)

            alph_sources = n_condvar_bins
            alph_target = int(y.max() + 1)
            data = Data(np.vstack((X_linpos, X_hd_angle, y)), 'ps', normalise=False)
            settings = {
                'lags_pid': [[0, 0]],
                'alpha': 0.1,
                'alph_s1': alph_sources,
                'alph_s2': alph_sources,
                'alph_t': alph_target,
                'max_unsuc_swaps_row_parm': 60,
                'num_reps': 63,
                'max_iters': 1000,
                'pid_estimator': 'SydneyPID'} # TartuPID

            targets = [2]
            sources = [[0, 1]]
            pid_analysis = PartialInformationDecomposition()
            results = pid_analysis.analyse_network(settings, data, targets,
                                                   sources)

            res = results.get_single_target(2)
            mutual = res['joint_mi_s1s2_t']
            redundant = res['shd_s1_s2']
            synergy = res['syn_s1_s2']
            unique_1 = res['unq_s1']
            unique_2 = res['unq_s2']

            df_full.loc[df_full.shape[0]] = [sess_ind, area, unit_id,
                                             unit_ind, kk, 'synergy', synergy, None]

            df_full.loc[df_full.shape[0]] = [sess_ind, area, unit_id,
                                             unit_ind, kk, 'redundant', redundant,
                                             None]

            df_full.loc[df_full.shape[0]] = [sess_ind, area, unit_id,
                                             unit_ind, kk, 'unique_pos', unique_1,
                                             None]

            df_full.loc[df_full.shape[0]] = [sess_ind, area, unit_id,
                                             unit_ind, kk, 'unique_hdir', unique_2,
                                             None]

            df_full.loc[df_full.shape[0]] = [sess_ind, area, unit_id,
                                             unit_ind, kk, 'mutual', mutual,
                                             None]





print('Saving information results to {}'.format(output_full_path))

output = {'df' : df_full,
          'pars' : pars}

pickle.dump(output, open(output_full_path, 'wb'))



f, ax = plt.subplots(1, 1)
sns.barplot(data=df_full, x='measure', y='value', hue='area',
            palette=[area_colormap['A1'], area_colormap['V2L']],
            hue_order=['A1', 'V2L'], ax=ax)