import numpy as np

def destroy_noise_correlations(X, y):

    """
    Perform a shuffling of the columns of X per bin of y.
    """

    X_shuf = np.zeros_like(X)
    bins = np.unique(y)

    for neu_ind in range(X.shape[1]):
        shuffled_neuron = []
        for bin in bins:
            ind = np.where(y == bin)[0]
            ind_shuf = np.random.permutation(ind)
            shuffled_neuron.append(X[ind_shuf, neu_ind])

        X_shuf[:, neu_ind] = np.hstack(shuffled_neuron)

    y_shuf = []
    for bin in bins:
        ind = np.where(y == bin)[0]
        y_shuf.append(y[ind])
    y_shuf = np.hstack(y_shuf)

    return X_shuf, y_shuf


X = np.array([[1, 1], [2, 2], [3, 3], [1, 1], [2, 2], [3, 3]])
y = np.array([2, 2, 2, 1, 1, 1])

destroy_noise_correlations(X,y )