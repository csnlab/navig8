import os
import numpy as np
import elephant
import neo
import pickle
from constants import *
from utils import *
import quantities as pq
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from prepare_decoding_data import PrepareDecodingData
from MLencoding import MLencoding
import pandas as pd
from mpl_toolkits.mplot3d import Axes3D
from plotting_style import area_colormap
import pickle

# encoding parameters

settings_name = 'set1'
sessions = ALL_SESSIONS
min_speed = 0.1
binsize_in_ms = 400
encoding_model = 'random_forest'
n_cv = 5
encoder_params = {'n_estimators' : 100}

pars = {'setting_name'  : settings_name,
        'sessions' : sessions,
        'min_speed' : min_speed,
        'binsize_in_ms' : binsize_in_ms,
        'encoding_model' : encoding_model,
        'n_cv' : n_cv,
        'encoder_params' : encoder_params}


output_file_name_r2   = 'encode_{}_{}.pkl'.format(settings_name, encoding_model)
output_file_name_pred = 'encode_{}_{}_predictions.pkl'.format(settings_name, encoding_model)

output_folder         = os.path.join(DATA_FOLDER, 'results', 'encode_wneural')
output_full_path_r2   = os.path.join(output_folder, output_file_name_r2)
output_full_path_pred = os.path.join(output_folder, output_file_name_pred)


if not os.path.isdir(output_folder):
    os.makedirs(output_folder)


# --- prepare ---

df_full = pd.DataFrame(columns=['session', 'area', 'unit_id', 'unit_ind_sess', 'unit_ind_area',
                                'predictor', 'r2'])

rs = {s : {'A1' : {}, 'V2L' : {}} for s in sessions}

encode_predictors = ['all',
                     'all+other_area',
                     'all+same_area']


# --- ENCODE ------------------------------------------------------------------

for sess_ind in sessions:

    file_name = 'navig8_session_{}.pkl'.format(sess_ind)
    bl = pickle.load(open(os.path.join(DATA_FOLDER, file_name), 'rb'))
    segment = bl.segments[0]
    A1_ind = [i for i, t in enumerate(segment.spiketrains)
              if t.annotations['area'] == 'A1']
    V2L_ind = [i for i, t in enumerate(segment.spiketrains)
               if t.annotations['area'] == 'V2L']
    xy_speed = get_analogsignal_by_name(segment, 'xy_speed')
    lin_speed = get_analogsignal_by_name(segment, 'lin_speed')
    lin_bin = get_analogsignal_by_name(segment, 'lin_bin')
    xy_pos = get_analogsignal_by_name(segment, 'xy_pos')
    hd_sincos = get_analogsignal_by_name(segment, 'hd_sincos')
    hd_change = get_analogsignal_by_name(segment, 'hd_change')

    lin_bin_arr = lin_bin.__array__()[:, 0].astype(int)
    xy_pos_arr = xy_pos.__array__()
    lin_speed_arr = lin_speed.__array__()[:, 0]
    xy_speed_arr = xy_speed.__array__()[:, 0]
    hd_sincos_arr = hd_sincos.__array__()
    hd_change_arr = hd_change.__array__()

    # TODO: use linear speed or 2d speed??
    pdd = PrepareDecodingData(spike_trains=segment.spiketrains,
                              position=xy_pos_arr,
                              linearized_position=lin_bin_arr,
                              speed=xy_speed_arr,
                              head_direction_sincos=hd_sincos_arr,
                              head_direction_sincos_change=hd_change_arr,
                              times=lin_bin.times.rescale(pq.ms))

    pdd._build_distance_matrix()
    pdd.bin_spikes(binsize_in_ms=binsize_in_ms)
    pdd.interpolate_position_and_speed()

    pdd.filter_nans()
    pdd.filter_min_speed(min_speed)

    binned_spikes = pdd.binned_spikes
    X_pos = pdd.interp_position
    X_speed = pdd.interp_speed.reshape(-1, 1)
    X_hd = pdd.interp_head_direction.reshape(-1, 1)
    X_hd_ch = pdd.interp_head_direction_change.reshape(-1, 1)
    X_A1_spikes = pdd.binned_spikes[:, A1_ind]
    X_V2L_spikes = pdd.binned_spikes[:, V2L_ind]

    if not np.all(X_speed > 0):
        raise ValueError



    for area, area_unit_ind in zip(['A1', 'V2L'], [A1_ind, V2L_ind]):
        print('AREA : {}'.format(area))
        for key in ['true_spikes'] + encode_predictors:
            rs[sess_ind][area][key] = np.zeros([len(area_unit_ind), X_pos.shape[0]])

        for kk, unit_ind in enumerate(area_unit_ind):
            print(kk, unit_ind)

            if area == 'A1':
                other_area_units = V2L_ind
                same_area_units = [zz for zz in area_unit_ind if zz != unit_ind]

            elif area == 'V2L':
                other_area_units = A1_ind
                same_area_units = [zz for zz in area_unit_ind if zz != unit_ind]

            X_other_area = pdd.binned_spikes[:, other_area_units]
            X_own_area = pdd.binned_spikes[:, same_area_units]
            predictors = {'all': np.hstack((X_pos, X_speed, X_hd, X_hd_ch)),
                          'all+other_area': np.hstack((X_pos, X_speed, X_hd, X_hd_ch, X_other_area)),
                          'all+same_area': np.hstack((X_pos, X_speed, X_hd, X_hd_ch, X_own_area))}

            unit_id = '{:02}_{}_{:02}'.format(sess_ind, area, unit_ind)
            y = binned_spikes[:, unit_ind]
            rs[sess_ind][area]['true_spikes'][kk, :] = y
            print('\n\n>>> Encoding unit {}\n'.format(unit_id))

            print('index other area : {}'.format(other_area_units))
            print('index own area : {}\n'.format(same_area_units))

            for predictor in encode_predictors:
                X_predictor = predictors[predictor]
                encoder = MLencoding(tunemodel = encoding_model)
                encoder.set_params(encoder_params)
                Y_hat, PR2s = encoder.fit_cv(X_predictor, y, n_cv=n_cv,
                                             verbose=2, continuous_folds=False)
                PR2s = np.mean(PR2s)
                rs[sess_ind][area][predictor][kk, :] = Y_hat

                df_full.loc[df_full.shape[0]] = [sess_ind, area, unit_id,
                                                 unit_ind, kk,
                                                 predictor, PR2s]



print('Saving encoding r2 to {}'.format(output_full_path_r2))
print('Saving encoding predictions to {}'.format(output_full_path_pred))

output_r2 = {'df' : df_full,
             'pars' : pars}
output_pred = {'predictions' : rs,
               'pars' : pars}

pickle.dump(output_pred, open(output_full_path_pred, 'wb'))
pickle.dump(output_r2, open(output_full_path_r2, 'wb'))


