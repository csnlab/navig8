import os
import numpy as np
import elephant
import neo
import pickle
from constants import *
from utils import *
import quantities as pq
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from prepare_decoding_data import PrepareDecodingData
import pandas as pdd
import matplotlib.gridspec as gridspec
from plotting_style import *
from scipy.ndimage.filters import gaussian_filter, gaussian_filter1d
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from scipy.stats.stats import pearsonr
from sklearn.model_selection import train_test_split
from collections import OrderedDict


# decoding parameters
min_spikes = 5
min_speed = 0.1
decoder = 'logistic_regression'
binsize_in_ms = 400
coarsify_bins=True
coarse_bin_size=5
sessions = GOOD_SESSIONS_ONE_AREA
min_units=20

# plotting parameters
save_plots = True
plot_format = 'png'
dpi = 400
plot_folder = os.path.join(DATA_FOLDER, 'plots', 'decode_jackknife')


if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)




# --- DECODE ------------------------------------------------------------------

rs = {area : {s : {} for s in GOOD_SESSIONS_BY_AREA[area]}
      for area in ['A1', 'V2L']}

for sess_ind in sessions:


    # --- LOAD DATA ---------------------------------------------------------------

    file_name = 'navig8_session_{}.pkl'.format(sess_ind)
    bl = pickle.load(open(os.path.join(DATA_FOLDER, file_name), 'rb'))
    segment = bl.segments[0]
    A1_ind = [i for i, t in enumerate(segment.spiketrains)
                       if t.annotations['area'] == 'A1']
    V2L_ind = [i for i, t in enumerate(segment.spiketrains)
                       if t.annotations['area'] == 'V2L']
    area_unit_ind = {'A1' : A1_ind, 'V2L' : V2L_ind}
    lin_speed = get_analogsignal_by_name(segment, 'lin_speed')
    lin_bin = get_analogsignal_by_name(segment, 'lin_bin')
    xy_pos = get_analogsignal_by_name(segment, 'xy_pos')

    lin_bin_arr = lin_bin.__array__()[:, 0]
    xy_pos_arr = xy_pos.__array__()
    lin_speed_arr = lin_speed.__array__()[:, 0]


    # --- PREPARE DATA FOR DECODING -----------------------------------------------

    pdd = PrepareDecodingData(spike_trains=segment.spiketrains,
                              position=xy_pos_arr,
                              linearized_position=lin_bin_arr,
                              speed=lin_speed_arr,
                              times=lin_bin.times.rescale(pq.ms))

    if coarsify_bins:
        pdd.coarsify_bins(new_bin_size=coarse_bin_size)

    pdd._build_distance_matrix()
    pdd.bin_spikes(binsize_in_ms=binsize_in_ms)
    pdd.interpolate_position_and_speed()

    pdd.filter_nans()
    pdd.filter_min_speed(min_speed)
    pdd.filter_min_spikes(min_spikes)

    X = pdd.binned_spikes
    y = pdd.interp_linearized_position
    z = pdd.interp_speed

    # z = gaussian_filter1d(z, sigma=5)
    # y = pd.qcut(z, 10, labels=False, retbins=False)


    # --- DECODE ------------------------------------------------------------------

    for area in GOOD_SESSIONS_ONE_AREA[sess_ind]:
        unit_ind = area_unit_ind[area]

        unit_scores = np.zeros(len(unit_ind))
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2,
                                                            shuffle=False)

        X_train = X_train[:, unit_ind]
        X_test = X_test[:, unit_ind]

        error_all_neurons = compute_decoding_error(X_train, y_train,
                                                   X_test, y_test,
                                                   decoder=decoder,
                                                   binsize_in_ms=binsize_in_ms,
                                                   distance_df=pdd.distance_df)

        for unit in range(X_train.shape[1]):

            X_train_jack = np.delete(X_train, unit, axis=1)
            X_test_jack = np.delete(X_test, unit, axis=1)

            error_all_minus_one = compute_decoding_error(X_train_jack, y_train,
                                                       X_test_jack, y_test,
                                                       decoder=decoder,
                                                       binsize_in_ms=binsize_in_ms,
                                                       distance_df=pdd.distance_df)

            unit_scores[unit] = error_all_minus_one - error_all_neurons

        rs[area][sess_ind]['unit_scores'] = unit_scores
        unit_order = unit_scores.argsort()[::-1]

        error_jack_prog = np.zeros(unit_order.shape[0])
        n_units = []

        for k, rr in enumerate(range(1, unit_order.shape[0]+1)):
            sel_units = unit_order[0:rr]
            print(sel_units)

            X_train_jack = X_train[:, sel_units]
            X_test_jack = X_test[:, sel_units]

            error_jackknifed = compute_decoding_error(X_train_jack, y_train,
                                                       X_test_jack, y_test,
                                                       decoder=decoder,
                                                       binsize_in_ms=binsize_in_ms,
                                                       distance_df=pdd.distance_df)
            error_jack_prog[k] = error_jackknifed
            n_units.append(sel_units.shape[0])

        rs[area][sess_ind]['error'] = error_jack_prog
        rs[area][sess_ind]['n_units'] = np.array(n_units)




f, ax = plt.subplots(1, 1, figsize=[3, 3])
for area in ['A1', 'V2L']:
    min_units = np.min([rs[area][sess_ind]['error'].shape[0] for sess_ind in GOOD_SESSIONS_BY_AREA[area]])
    y_all = np.hstack([rs[area][sess_ind]['error'].reshape(-1, 1)[0:min_units, :]
                       for sess_ind in GOOD_SESSIONS_BY_AREA[area]]).mean(axis=1)
    ax.plot(np.arange(min_units), y_all, c=area_colormap[area], label=area, lw=3, zorder=20)

    for sess_ind in GOOD_SESSIONS_BY_AREA[area]:
        x = rs[area][sess_ind]['n_units']
        y = rs[area][sess_ind]['error']
        ax.plot(x, y, c=area_colormap[area], lw=0.7, alpha=0.8)

handles, labels = plt.gca().get_legend_handles_labels()
by_label = OrderedDict(zip(labels, handles))
ax.legend(by_label.values(), by_label.keys(), loc='upper right', frameon=False)
#ax.set_xticks(np.arange(1, ax.get_xlim()[1], 5))
sns.despine()
ax.set_ylabel('Average error (m)')
ax.set_xlabel('# neurons')
plt.tight_layout()


if save_plots:
    if len(sessions) == 1:
        plot_name = 'jackknife_decode_w_{}_{}ms_session_{:02}.{}'.format(decoder, binsize_in_ms,
                                                                      sess_ind, plot_format)
    else:
        plot_name = 'jackknife_decode_w_{}_{}ms_multiplesessions.{}'.format(decoder, binsize_in_ms, plot_format)

    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



scores_a1 = np.hstack([rs['A1'][sess_ind]['unit_scores'] for sess_ind in GOOD_SESSIONS_BY_AREA['A1']])
scores_v2l = np.hstack([rs['V2L'][sess_ind]['unit_scores'] for sess_ind in GOOD_SESSIONS_BY_AREA['V2L']])

f, ax = plt.subplots(1, 1, figsize=[3, 3])
n_a1, n_v2l = scores_a1.shape[0], scores_v2l.shape[0]
x1 = np.array(range(n_a1))/float(n_a1)
x2 = np.array(range(n_v2l))/float(n_v2l)
ax.plot(np.sort(scores_a1), x1, c=area_colormap['A1'])
ax.plot(np.sort(scores_v2l), x2, c=area_colormap['V2L'])
ax.set_xlabel('Jackknife score')
ax.set_ylabel('Probability')
sns.despine()
plt.tight_layout()

if save_plots:
    if len(sessions) == 1:
        plot_name = 'jackknife_score_empiricalcdf_w_{}_{}ms_session_{:02}.{}'.format(decoder, binsize_in_ms,
                                                                      sess_ind, plot_format)
    else:
        plot_name = 'jackknife_score_empiricalcdf_w_{}_{}ms_multiplesessions.{}'.format(decoder, binsize_in_ms, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



f, ax = plt.subplots(1, 1, figsize=[3, 3])
sns.kdeplot(scores_a1, color=area_colormap['A1'], cumulative=True, ax=ax)
sns.kdeplot(scores_v2l, color=area_colormap['V2L'], cumulative=True, ax=ax)
ax.set_xlabel('Jackknife score')
ax.set_ylabel('Probability density')
sns.despine()
plt.tight_layout()
if save_plots:
    if len(sessions) == 1:
        plot_name = 'jackknife_score_kdecdf_w_{}_{}ms_session_{:02}.{}'.format(decoder, binsize_in_ms,
                                                                      sess_ind, plot_format)
    else:
        plot_name = 'jackknife_score_kdecdf_w_{}_{}ms_multiplesessions.{}'.format(decoder, binsize_in_ms, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



f, ax = plt.subplots(1, 1, figsize=[3, 3])
sns.distplot(scores_a1, color=area_colormap['A1'], ax=ax, rug=False, hist=False,
             kde_kws={'shade':True})
sns.distplot(scores_v2l, color=area_colormap['V2L'], ax=ax, rug=False, hist=False,
             kde_kws={'shade': True})
ax.set_xlabel('Jackknife score')
ax.set_ylabel('Probability density')
sns.despine()
plt.tight_layout()

if save_plots:
    if len(sessions) == 1:
        plot_name = 'jackknife_score_kde_w_{}_{}ms_session_{:02}.{}'.format(decoder, binsize_in_ms,
                                                                      sess_ind, plot_format)
    else:
        plot_name = 'jackknife_score_kde_w_{}_{}ms_multiplesessions.{}'.format(decoder, binsize_in_ms, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



