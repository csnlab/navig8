import os
import numpy as np
import elephant
import neo
import pickle
from constants import *
from utils import *
import quantities as pq
import matplotlib.pyplot as plt
import seaborn as sns

sess_ind = 1
area = 'A1'

file_name = 'navig8_session_{}.pkl'.format(sess_ind)

bl = pickle.load(open(os.path.join(DATA_FOLDER, file_name), 'rb'))
segment = bl.segments[0]

spike_trains = [t for t in segment.spiketrains
                if t.annotations['area'] == area]


binsize = 100
bs = elephant.conversion.BinnedSpikeTrain(spike_trains,
                                binsize=binsize * pq.ms,
                                t_start=spike_trains[0].t_start.rescale(pq.ms),
                                t_stop=spike_trains[0].t_stop.rescale(pq.ms))

X = bs.to_array().T
bin_times = bs.bin_centers

xy_pos = get_analogsignal_by_name(segment, 'xy_pos')

lin_bin = get_analogsignal_by_name(segment, 'lin_bin')


f, ax = plt.subplots(1, 1)
sns.distplot(lin_bin[~np.isnan(lin_bin)], ax=ax)


f, ax = plt.subplots(1, 1)
ax.plot(lin_bin)


n_units = np.zeros([len(ALL_SESSIONS), 2])
for i, sess_ind in enumerate(ALL_SESSIONS):

    file_name = 'navig8_session_{}.pkl'.format(sess_ind)

    bl = pickle.load(open(os.path.join(DATA_FOLDER, file_name), 'rb'))
    segment = bl.segments[0]

    n_units_a1 = len([t for t in segment.spiketrains if t.annotations['area'] == 'A1'])

    n_units_v2l = len([t for t in segment.spiketrains if t.annotations['area'] == 'V2L'])
    n_units[i, 0] = n_units_a1
    n_units[i, 1] = n_units_v2l

import pandas as pd
df = pd.DataFrame(columns=['A1', 'V2L'], data=n_units)

df2 = df.loc[np.logical_or(df['A1'] >= 19, df['V2L'] >= 19)]

print(df2.mean())

print(df2.std())







a = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])

b = np.gradient(a)