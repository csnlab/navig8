import numpy as np
import quantities as pq
import warnings
from scipy.ndimage.filters import gaussian_filter1d


class DKW_decoder():

    def __init__(self, bin_size_train, bin_size_test,
                       smooth_ratemap=False, smooth_sigma=1):

        try:
            bin_size_train.units
        except AttributeError:
            warnings.warn('The parameter bin_size_train was passed without units:'
                          'assuming that units are milliseconds.')
            bin_size_train = bin_size_train * pq.ms

        try:
            bin_size_test.units
        except AttributeError:
            warnings.warn('The parameter bin_size_test was passed without units:'
                          'assuming that units are milliseconds.')
            bin_size_test = bin_size_test * pq.ms


        self.bin_size_train = bin_size_train
        self.bin_size_test  = bin_size_test
        self.smooth_ratemap = smooth_ratemap
        self.smooth_sigma   = smooth_sigma


    def fit(self, X_train, y_train, bins=None):

        if bins is None:
            bins = np.unique(y_train)

        ratemap = np.zeros([X_train.shape[1], bins.shape[0]])
        for bin_ind, bin in enumerate(bins):
            in_bin = y_train == bin
            time_in_bin = (self.bin_size_train * in_bin.sum()).rescale(pq.s)
            spikes_in_bin = X_train[in_bin].sum(axis=0)
            firing_rate_in_bin = spikes_in_bin / time_in_bin
            ratemap[:, bin_ind] = firing_rate_in_bin

        self.bins = bins
        self.ratemap = ratemap

        if self.smooth_ratemap:
            self._smooth_ratemap(self.smooth_sigma)

    def predict_proba(self, X_test, normalize=True):

        proba = np.zeros([X_test.shape[0], self.ratemap.shape[1]])
        for sample_ind, spikes in enumerate(X_test):

            for bin_ind, bin in enumerate(self.bins):
                constant = 1
                first_term = np.product(np.power(self.ratemap[:, bin_ind], spikes))
                second_term = np.exp((-self.bin_size_test.rescale(pq.s) * self.ratemap[:,
                                                                     bin_ind].sum()).item())
                proba[sample_ind, bin_ind] = constant * first_term * second_term

        if normalize:
            prob_sum = proba.sum(axis=1)[:, np.newaxis]
            prob_sum[prob_sum == 0] = 1
            proba = proba /prob_sum

        return proba


    def predict(self, X_test):
        proba = self.predict_proba(X_test)
        return self.bins[proba.argmax(axis=1)]


    def _smooth_ratemap(self, sigma):

        for unit in range(self.ratemap.shape[0]):
            self.ratemap[unit, :] = gaussian_filter1d(self.ratemap[unit, :],
                                                      sigma=sigma)
