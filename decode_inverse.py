import os
import pickle
from constants import *
from utils import *
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
import matplotlib.gridspec as gridspec
from sklearn.preprocessing import minmax_scale
from plotting_style import *
from scipy.ndimage.filters import gaussian_filter, gaussian_filter1d
import pandas as pd
from scipy.stats.stats import pearsonr, ttest_rel
from prepare_decoding_data import PrepareDecodingData
from constants import *
from utils import *
from scipy.stats import wilcoxon

settings_name = 'aug23'
decoder       = 'dkw'
include_sessions = 'good_in_both_areas'

n_folds = 10
kfold_shuffle=True
n_estimators = 100
save_plots  = True
plot_format = 'svg'
dpi         = 400

# --- SET SEED ----------------------------------------------------------------

np.random.seed(15)

# -----------------------------------------------------------------------------


if include_sessions == 'all':
    sessions = list(GOOD_SESSIONS_ONE_AREA.keys())
elif include_sessions == 'good_in_both_areas':
    sessions = GOOD_SESSIONS_BOTH_AREAS

predictors = ['all', 'all_w_other']

# plotting parameters
plot_folder = os.path.join(DATA_FOLDER, 'plots', 'decode',
                           '{}_{}'.format(settings_name, decoder))

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

bin_distance_file = os.path.join(DATA_FOLDER, 'bin_distance.pkl')
bin_distance = pickle.load(open(bin_distance_file, 'rb'))

results_file_name = 'decode_{}_{}.pkl'.format(settings_name, decoder)
results_path = os.path.join(DATA_FOLDER, 'results', 'dec', results_file_name)
results = pickle.load(open(results_path, 'rb'))

df_full = results['decoding_results']
pars    = results['decoding_pars']




df = pd.DataFrame(columns=['area', 'session', 'predictor', 'error'])

for sess_ind in sessions:
    print('Session {}'.format(sess_ind))

    file_name = 'navig8_session_{}.pkl'.format(sess_ind)
    bl = pickle.load(open(os.path.join(DATA_FOLDER, file_name), 'rb'))
    segment = bl.segments[0]

    xy_speed = get_analogsignal_by_name(segment, 'xy_speed')
    lin_speed = get_analogsignal_by_name(segment, 'lin_speed')
    lin_bin = get_analogsignal_by_name(segment, 'lin_bin')
    xy_pos = get_analogsignal_by_name(segment, 'xy_pos')
    hd_sincos = get_analogsignal_by_name(segment, 'hd_sincos')
    hd_change = get_analogsignal_by_name(segment, 'hd_change')

    lin_bin_arr = lin_bin.__array__()[:, 0]
    xy_pos_arr = xy_pos.__array__()
    lin_speed_arr = lin_speed.__array__()[:, 0]
    xy_speed_arr = xy_speed.__array__()[:, 0]
    hd_sincos_arr = hd_sincos.__array__()
    hd_change_arr = hd_change.__array__()

    pdd = PrepareDecodingData(spike_trains=segment.spiketrains,
                              position=xy_pos_arr,
                              linearized_position=lin_bin_arr,
                              speed=lin_speed_arr, # the abs is taken automatically
                              head_direction_sincos=hd_sincos_arr,
                              head_direction_sincos_change=hd_change_arr,
                              times=lin_bin.times.rescale(pq.ms))

    if pars['coarsify_bins']:
        pdd.coarsify_bins(new_bin_size=pars['coarse_bin_size'])

    pdd._build_distance_matrix()
    pdd.bin_spikes(binsize_in_ms=pars['binsize_in_ms'])
    pdd.interpolate_position_and_speed()

    pdd.filter_nans()
    pdd.filter_min_speed(pars['min_speed'])
    pdd.filter_min_spikes(pars['min_spikes'])

    binned_spikes = pdd.binned_spikes

    true_bin = df_full.loc[df_full['session'] == sess_ind, 'bin']
    testing_ind = df_full.loc[df_full['session'] == sess_ind, 'testing_ind']

    y = pdd.interp_linearized_position
    np.testing.assert_array_equal(y[testing_ind], true_bin)

    X_position = pdd.interp_linearized_position.reshape(-1, 1)[testing_ind, :]
    X_speed = pdd.interp_speed.reshape(-1, 1)[testing_ind, :]
    X_hd = pdd.interp_head_direction.reshape(-1, 1)[testing_ind, :]
    X_hd_ch = pdd.interp_head_direction_change.reshape(-1, 1)[testing_ind, :]
    pred_V2L = df_full.loc[df_full['session'] == sess_ind, 'V2L_pred'].as_matrix().reshape(-1, 1)
    pred_A1 = df_full.loc[df_full['session'] == sess_ind, 'A1_pred'].as_matrix().reshape(-1, 1)

    for area in AREAS:
        if area == 'A1':
            y = pred_A1.flatten()
            X_all = np.hstack((X_position, X_speed, X_hd, X_hd_ch))
            X_all_w_other = np.hstack((X_position, X_speed, X_hd, X_hd_ch, pred_V2L))

        elif area == 'V2L':
            y = pred_V2L.flatten()
            X_all = np.hstack((X_position, X_speed, X_hd, X_hd_ch))
            X_all_w_other = np.hstack((X_position, X_speed, X_hd, X_hd_ch, pred_A1))


        for predictor in predictors:

            if predictor == 'all':
                X = X_all
            elif predictor == 'all_w_other':
                X = X_all_w_other

            kf = KFold(n_splits=n_folds, shuffle=kfold_shuffle,
                       random_state=sess_ind)

            y_true_all, y_pred_all = [], []

            np.all(X_all_w_other[:, -1] == y)

            for fold, (training_ind, testing_ind) in enumerate(kf.split(X)):
                print('\n - Decoding area {} -  fold {}'.format(area, fold))

                X_train = X[training_ind, :]
                X_test = X[testing_ind, :]

                y_train = y[training_ind]
                y_test = y[testing_ind]

                dec = RandomForestClassifier(n_estimators=n_estimators,
                                             random_state=92)

                dec.fit(X_train, y_train)

                y_pred = dec.predict(X_test)
                y_true_all.append(y_test)
                y_pred_all.append(y_pred)

            y_true_all = np.hstack(y_true_all)
            y_pred_all = np.hstack(y_pred_all)

            error = np.zeros(y_true_all.shape[0])
            for i, (true_bin, pred_bin) in enumerate(
                    zip(y_true_all, y_pred_all)):
                error[i] = pdd.get_error_distance(true_bin, pred_bin)

            df.loc[df.shape[0], :] = [area, sess_ind, predictor, np.mean(error)]


f, ax = plt.subplots(1, 1)
sns.barplot(data=df, x='area', hue='predictor', y='error', ax=ax)


f, ax = plt.subplots(1, 1, figsize=[small_square_side, small_square_side])
ax.plot([0, 1], [0, 1], transform=ax.transAxes, ls='--', color='k',
        alpha=0.5)

for i, area in enumerate(AREAS):

    #sessions =  GOOD_SESSIONS_BY_AREA[area]
    print(sessions)
    dfx = df[(df['area'] == area) & (np.isin(df['session'], sessions))]

    x = dfx.loc[dfx['predictor'] == 'all', 'error'].as_matrix()
    y = dfx.loc[dfx['predictor'] == 'all_w_other', 'error'].as_matrix()

    ax.scatter(x, y, edgecolor=area_colormap[area], facecolors='w', zorder=3)

ax.set_ylabel('Error using all predictors')
ax.set_xlabel('Error without the predictions \nof the other area')
#ax.set_title('Error in predicting \nA1 and V2L decoded position')
minlim = min(ax.get_xlim()[0], ax.get_ylim()[0])
maxlim = max(ax.get_xlim()[1], ax.get_ylim()[1])
ax.set_ylim([minlim, maxlim])
ax.set_xlim([minlim, maxlim])
plt.locator_params(axis='y', nbins=4)
plt.locator_params(axis='x', nbins=4)
sns.despine()
plt.tight_layout()
ax.axvline(0, c=sns.xkcd_rgb['light blue'], ls=':', alpha=1, zorder=1)
ax.axhline(0, c=sns.xkcd_rgb['light blue'], ls=':', alpha=1, zorder=1)


if save_plots:
    plot_name = 'predict_predictions_{}_{}.{}'.format(settings_name, decoder,
                                                     plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


x = df.loc[df['predictor'] == 'all', 'error'].as_matrix()
y = df.loc[df['predictor'] == 'all_w_other', 'error'].as_matrix()

from scipy.stats import ttest_rel

ttest_rel(x, y)

stat, p_val_w = wilcoxon(x, y)

print('wilcoxon test p-val: {}'.format(p_val_w))