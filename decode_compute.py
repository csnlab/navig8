import os
import numpy as np
import elephant
import neo
import pickle
from constants import *
from utils import *
import quantities as pq
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from prepare_decoding_data import PrepareDecodingData
import pandas as pdd
import matplotlib.gridspec as gridspec
from plotting_style import *
from scipy.ndimage.filters import gaussian_filter, gaussian_filter1d
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from scipy.stats.stats import pearsonr, ttest_rel
from loadmat import loadmat

# decoding parameters

# MAIN
# settings_name = 'aug23'
# sessions = list(GOOD_SESSIONS_ONE_AREA.keys())
# min_spikes = 5
# min_speed = 0.1
# decoder = 'dkw'
# n_folds = 5
# binsize_in_ms = 400
# coarsify_bins=True
# coarse_bin_size = 3
# decode_with_speed=False
# destroy_noise_correlations_flag = False
# kfold_shuffle = True

# MAIN WITH RANDOM FOREST
# settings_name = 'aug23'
# sessions = list(GOOD_SESSIONS_ONE_AREA.keys())
# min_spikes = 5
# min_speed = 0.1
# decoder = 'rf'
# n_folds = 5
# binsize_in_ms = 400
# coarsify_bins=True
# coarse_bin_size = 3
# decode_with_speed=False
# destroy_noise_correlations_flag = False
# kfold_shuffle = True


# KEEP NOISE CORR
settings_name = 'aug23_knc50'
sessions = list(GOOD_SESSIONS_ONE_AREA.keys())
min_spikes = 5
min_speed = 0.1
decoder = 'dkw'
n_folds = 5
binsize_in_ms = 50
coarsify_bins=True
coarse_bin_size = 3
decode_with_speed=False
destroy_noise_correlations_flag = False
kfold_shuffle = True

# DESTROY NOISE CORR
settings_name = 'aug23_dnc50'
sessions = list(GOOD_SESSIONS_ONE_AREA.keys())
min_spikes = 5
min_speed = 0.1
decoder = 'dkw'
n_folds = 5
binsize_in_ms = 50
coarsify_bins=True
coarse_bin_size = 3
decode_with_speed=False
destroy_noise_correlations_flag = True
kfold_shuffle = True


# -----------------------------------------------------------------------------

pars = {'setting_name'      : settings_name,
        'sessions'          : sessions,
        'min_spikes'        : min_spikes,
        'min_speed'         : min_speed,
        'decoder'           : decoder,
        'n_folds'           : n_folds,
        'binsize_in_ms'     : binsize_in_ms,
        'coarsify_bins'     : coarsify_bins,
        'coarse_bin_size'   : coarse_bin_size,
        'decode_with_speed' : decode_with_speed,
        'destroy_noise_correlatios' : destroy_noise_correlations_flag}


if not coarsify_bins:
    maze_distance = loadmat(MAZE_DISTANCE_PATH)

output_file_name = 'decode_{}_{}.pkl'.format(settings_name, decoder)
output_folder    = os.path.join(DATA_FOLDER, 'results', 'dec')
output_full_path = os.path.join(output_folder, output_file_name)

if not os.path.isdir(output_folder):
    os.makedirs(output_folder)

rs = {s : {'A1' : {}, 'V2L' : {}} for s in sessions}

for sess_ind in sessions:
    # --- LOAD DATA ---------------------------------------------------------------
    print('Session {}'.format(sess_ind))
    file_name = 'navig8_session_{}.pkl'.format(sess_ind)
    bl = pickle.load(open(os.path.join(DATA_FOLDER, file_name), 'rb'))
    segment = bl.segments[0]
    A1_ind = [i for i, t in enumerate(segment.spiketrains)
                       if t.annotations['area'] == 'A1']
    V2L_ind = [i for i, t in enumerate(segment.spiketrains)
                       if t.annotations['area'] == 'V2L']
    lin_speed = get_analogsignal_by_name(segment, 'lin_speed')
    lin_bin = get_analogsignal_by_name(segment, 'lin_bin')
    xy_pos = get_analogsignal_by_name(segment, 'xy_pos')

    lin_bin_arr = lin_bin.__array__()[:, 0]
    xy_pos_arr = xy_pos.__array__()
    lin_speed_arr = lin_speed.__array__()[:, 0]

    if not coarsify_bins:
        maze_distance_sess = maze_distance['Distances'][:, :, sess_ind]  * pixels_to_meters * pq.m

    # --- PREPARE DATA FOR DECODING -----------------------------------------------

    pdd = PrepareDecodingData(spike_trains=segment.spiketrains,
                              position=xy_pos_arr,
                              linearized_position=lin_bin_arr,
                              speed=lin_speed_arr,
                              times=lin_bin.times.rescale(pq.ms))

    if coarsify_bins:
        pdd.coarsify_bins(new_bin_size=coarse_bin_size)

    pdd._build_distance_matrix()
    pdd.bin_spikes(binsize_in_ms=binsize_in_ms)
    pdd.interpolate_position_and_speed()

    pdd.filter_nans()
    pdd.filter_min_speed(min_speed)
    pdd.filter_min_spikes(min_spikes)

    X = pdd.binned_spikes
    y = pdd.interp_linearized_position
    z = pdd.interp_speed

    if destroy_noise_correlations_flag:
        if decode_with_speed:
            raise ValueError
        X, y = destroy_noise_correlations(X, y)

    # --- PREPROCESS ----------------------------------------------------------

    kf = KFold(n_splits=n_folds, shuffle=kfold_shuffle, random_state=sess_ind)

    for area, unit_ind in zip(['A1', 'V2L'], [A1_ind, V2L_ind]):

        y_true_all, y_pred_all, z_true_all = [], [], []
        testing_ind_all = []
        for fold, (training_ind, testing_ind) in enumerate(kf.split(X)):
            print('\n - Decoding area {} -  fold {}'.format(area, fold))

            X_train = X[training_ind, :]
            X_test  = X[testing_ind, :]

            X_train = X_train[:, unit_ind]
            X_test  = X_test[:, unit_ind]

            y_train = y[training_ind]
            y_test  = y[testing_ind]

            z_train = z[training_ind]
            z_test  = z[testing_ind]

            print(y_test.min())

            if decode_with_speed:
                X_train = np.hstack((X_train, z_train[:,np.newaxis]))
                X_test = np.hstack((X_test, z_test[:,np.newaxis]))

            if decoder == 'rf':
                dec = RandomForestClassifier(n_estimators=100,
                                             random_state=92)

            elif decoder == 'dkw':
                dec = DKW_decoder(bin_size_train=binsize_in_ms,
                                  bin_size_test=binsize_in_ms)

            elif decoder == 'lr':
                dec = LogisticRegression()

            dec.fit(X_train, y_train)

            y_pred = dec.predict(X_test)
            y_true_all.append(y_test)
            y_pred_all.append(y_pred)
            z_true_all.append(z_test)
            testing_ind_all.append(testing_ind)

            error = np.zeros(y_test.shape[0])
            for i, (true_bin, pred_bin) in enumerate(zip(y_test, y_pred)):
                error[i] = pdd.get_error_distance(true_bin, pred_bin)
            print('  -> Mean error: {:2f}'.format(error.mean()))

        y_true_all = np.hstack(y_true_all)
        y_pred_all = np.hstack(y_pred_all)
        z_true_all = np.hstack(z_true_all)
        testing_ind_all = np.hstack(testing_ind_all)

        error = np.zeros(y_true_all.shape[0])
        for i, (true_bin, pred_bin) in enumerate(zip(y_true_all, y_pred_all)):
            error[i] = pdd.get_error_distance(true_bin, pred_bin)

        if not coarsify_bins:
            maze_error = np.zeros(y_true_all.shape[0])
            for i, (true_bin, pred_bin) in enumerate(zip(y_true_all, y_pred_all)):
                maze_error[i] = maze_distance_sess[int(true_bin-1), int(pred_bin-1)]

        error_2d = np.zeros([y_true_all.shape[0], 2])
        for i, (true_bin, pred_bin) in enumerate(zip(y_true_all, y_pred_all)):
            error_2d[i, :] = pdd.get_error_2d(true_bin, pred_bin).as_matrix()

        rs[sess_ind][area]['y_true_all'] = y_true_all
        rs[sess_ind][area]['y_pred_all'] = y_pred_all
        rs[sess_ind][area]['z_true_all'] = z_true_all
        rs[sess_ind][area]['error'] = error
        rs[sess_ind][area]['testing_ind'] = testing_ind_all
        if not coarsify_bins:
            rs[sess_ind][area]['maze_error'] = maze_error

        rs[sess_ind][area]['error_2d'] = error_2d



# Decoding results to dataframe -----------------------------------------------

df_list = []
for sess_ind in sessions:
    df = pd.DataFrame(columns=['session', 'bin', 'A1_pred', 'V2L_pred'])
    df['bin']       = rs[sess_ind]['A1']['y_true_all']
    df['speed']     = rs[sess_ind]['A1']['z_true_all']
    df['testing_ind'] = rs[sess_ind]['A1']['testing_ind']
    df['A1_pred']   = rs[sess_ind]['A1']['y_pred_all']
    df['V2L_pred']  = rs[sess_ind]['V2L']['y_pred_all']
    df['A1_error']  = rs[sess_ind]['A1']['error']
    df['V2L_error'] = rs[sess_ind]['V2L']['error']
    if not coarsify_bins:
        df['A1_maze_error'] = rs[sess_ind]['A1']['maze_error']
        df['V2L_maze_error'] = rs[sess_ind]['V2L']['maze_error']
    df['A1_error_x']  = rs[sess_ind]['A1']['error_2d'][:, 0]
    df['A1_error_y']  = rs[sess_ind]['A1']['error_2d'][:, 1]
    df['V2L_error_x']  = rs[sess_ind]['V2L']['error_2d'][:, 0]
    df['V2L_error_y']  = rs[sess_ind]['V2L']['error_2d'][:, 1]

    df['session']   = sess_ind

    df['bin'] = df['bin'].astype(int)
    df_list.append(df)

if len(sessions) > 1:
    df_full = pd.concat(df_list, axis=0)
else:
    df_full = df


for area in AREAS:
    df_full['{}_pred'.format(area)] = df_full['{}_pred'.format(area)].astype(int)

output = {'decoding_pars' : pars,
         'decoding_results' : df_full}

print('Saving output to {}'.format(output_full_path))
pickle.dump(output, open(output_full_path, 'wb'))




