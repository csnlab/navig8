import numpy as np
from constants import ORIGINAL_DATA_PATH, ORIGINAL_TRIAL_DATA_PATH, DATA_FOLDER, ALL_SESSIONS
from constants import pixels_to_meters, spatial_bins_to_meters
import pickle
from loadmat import loadmat
import quantities as pq
import elephant
import os
import neo
import pandas as pd

if False:

    trial_navig8 = loadmat(ORIGINAL_TRIAL_DATA_PATH)

    navig8 = loadmat(ORIGINAL_DATA_PATH)

    for sess_indx in ALL_SESSIONS:
        print(sess_indx)

        sess_indx= 1
        area_label = 'VSession'

        trial_navig8[area_label][sess_indx].events.FirstTS
        session_date = trial_navig8[area_label][sess_indx].Date


        # GET START TIME
        start_labels = ['Left Visual ON',
                        'Right Visual ON']

        # get the indices of the visual onsets
        t_start_indx = np.where(np.isin(trial_navig8[area_label][sess_indx].events.EventName, start_labels))[0]

        # get the corresponding trial numbers
        all_trial_num = trial_navig8[area_label][sess_indx].events.TrialNum
        trial_num = all_trial_num[t_start_indx]

        # exclude trials with more than 1 visual onset
        selected_trials = np.unique(pd.value_counts(trial_num)[pd.value_counts(trial_num) == 1].index.__array__())

        # get the indices of the visual onsets excluding the trials with more than 1
        t_start_indx = [t for t in t_start_indx if np.isin(all_trial_num[t], selected_trials)]

        # trial num selected

        trial_num = all_trial_num[t_start_indx]
        # get the start and end times
        t_start = np.sort(np.hstack(trial_navig8[area_label][sess_indx].events.TS[t_start_indx]))
        t_start = t_start * 1e-4 * pq.s
        t_end = np.hstack([t_start[1:], t_start[-1]+20*pq.s])*pq.s

        # TRIAL MODALITY
        trial_modality = trial_navig8[area_label][sess_indx].events.Modality[t_start_indx]
        trial_modality = ['auditory' if i==0 else 'visual' if i==1 else 'multisensory' for i in trial_modality]

        # TRIAL INTENSITY
        trial_type = trial_navig8[area_label][sess_indx].events.TrialType[t_start_indx]
        trial_intensity = ['low' if i==0 else 'high' if i==1 else 'multi' for i in trial_type]

        # DIRECTION CHOSEN
        direction_chosen = trial_navig8[area_label][sess_indx].events.DirectionChosen[t_start_indx]
        direction_chosen = ['left' if i == 0 else 'right' for i in direction_chosen]

        # DIRECTION CUED
        direction_cued = trial_navig8[area_label][sess_indx].events.Direction[t_start_indx]
        if np.isin(2, direction_cued):
            raise ValueError
        direction_cued = ['left' if i == 0 else 'right' for i in direction_cued]

        trial_correct = trial_navig8[area_label][sess_indx].events.Correctness[t_start_indx]
        trial_correct = ['correct' if i==1 else 'incorrect' for i in trial_correct]


        # startofiti = trial_navig8[area_label][sess_indx].events.StartOfITI
        # indx = np.hstack([[1], np.where(np.diff(trial_navig8[area_label][sess_indx].events.StartOfITI)>1)[0]+1])
        # startofiti_check = startofiti[indx]


        # TRIAL MODALITY SANITY CHECK
        # trial_modality_check = trial_navig8[area_label][sess_indx].Behavior.CueModality
        # trial_modality_check = ['auditory' if i==0 else 'visual' if i==1 else 'multisensory' for i in trial_modality_check]
        # np.testing.assert_array_equal(trial_modality, trial_modality_check)
        #print(len(trial_modality), len(trial_modality_check))

        #startofiti = trial_navig8[area_label][sess_indx].events.StartITITS
        #np.testing.assert_array_equal(startofiti, startofiti_check)

        #start_labels = ['Start ITI']

        #startofiti = startofiti * 1e-4 * pq.s

        # total_ts_lag = trial_navig8[area_label][0].events.LastTS - trial_navig8[area_label][0].events.FirstTS
        # (total_ts_lag * 1e-4 * pq.s).rescale(pq.hour)
        # (total_ts_lag * pq.ms /10).rescale(pq.hour)


        dt = pd.DataFrame(columns=['trial_id','t_start', 't_stop', 'duration',
                                   'modality',
                                   'intensity', 'direction_chosen',
                                   'direction_cued', 'correct'])

        dt['trial_id'] = ['{}_{}'.format(session_date, t) for t in trial_num]
        dt['t_start'] = t_start
        dt['t_stop'] = t_end
        dt['duration'] = t_end - t_start
        dt['modality'] = trial_modality
        dt['intensity'] = trial_intensity
        dt['direction_chosen'] = direction_chosen
        dt['direction_cued'] = direction_cued
        dt['correct'] = trial_correct
        dt['previous_direction_chosen'] = np.hstack([['none'], dt['direction_chosen'][:-1]])

        cell_ids = trial_navig8[area_label][sess_indx].CellID

        if area_label == 'ASession':
            cell_ids_decoding = navig8['PaulsData'][sess_indx].A1.CellID
        elif area_label == 'VSession':
            cell_ids_decoding = navig8['PaulsData'][sess_indx].V2L.CellID

        for id in cell_ids_decoding:
            if np.isin(id, cell_ids):
                pass
            else:
                print(id)

            #assert set(cell_ids_decoding).issubset(cell_ids)




def get_trial_df(sess_indx):
    trial_navig8 = loadmat(ORIGINAL_TRIAL_DATA_PATH)

    #navig8 = loadmat(ORIGINAL_DATA_PATH)

    area_label = 'VSession'

    startofiti = trial_navig8[area_label][sess_indx].events.StartOfITI
    indx = np.hstack([[1], np.where(np.diff(trial_navig8[area_label][sess_indx].events.StartOfITI) > 1)[0] + 1])
    startofiti_check = startofiti[indx]

    # TRIAL MODALITY
    trial_modality = trial_navig8[area_label][
        sess_indx].Behavior.CueModality
    trial_modality = ['auditory' if i == 0 else 'visual' if i == 1 else 'multisensory' for i
        in trial_modality]

    # TRIAL MODALITY SANITY CHECK
    trial_modality_check = trial_navig8[area_label][sess_indx].events.Modality[indx]
    trial_modality_check = ['auditory' if i == 0 else 'visual' if i == 1 else 'multisensory' for i in trial_modality_check]
    np.testing.assert_array_equal(trial_modality, trial_modality_check)

    # TRIAL INTENSITY
    trial_type = trial_navig8[area_label][sess_indx].events.TrialType[indx]
    trial_intensity = ['low' if i == 0 else 'high' if i == 1 else 'multi'
                       for i in trial_type]

    # DIRECTION CHOSEN
    direction_chosen = trial_navig8[area_label][
        sess_indx].Behavior.DirectionChosen
    direction_chosen = ['left' if i == 0 else 'right' for i in direction_chosen]

    # DIRECTION CUED
    direction_cued = trial_navig8[area_label][sess_indx].Behavior.DirectionCued
    if np.isin(2, direction_cued) :
        raise ValueError
    direction_cued = ['left' if i == 0 else 'right' for i in direction_cued]

    # TRIAL CORRECT
    trial_correct = trial_navig8[area_label][sess_indx].Behavior.Correctness
    trial_correct = ['correct' if i == 1 else 'incorrect' for i in trial_correct]

    startofiti = trial_navig8[area_label][sess_indx].events.StartITITS
    np.testing.assert_array_equal(startofiti, startofiti_check)

    # GET START TIME
    # start_labels = ['Left Visual ON',
    #                 'Right Visual ON']

    start_labels = ['Start ITI']

    t_start_indx = np.where(np.isin(trial_navig8[area_label][sess_indx].events.EventName,start_labels))[0]
    t_start = np.sort(np.hstack(trial_navig8[area_label][sess_indx].events.TS[t_start_indx]))
    t_start = t_start * 1e-4 * pq.s

    t_end = np.hstack([t_start[1 :], t_start[-1] + 20 * pq.s]) * pq.s

    # startofiti = startofiti * 1e-4 * pq.s

    # total_ts_lag = trial_navig8[area_label][0].events.LastTS - trial_navig8[area_label][0].events.FirstTS
    # (total_ts_lag * 1e-4 * pq.s).rescale(pq.hour)
    # (total_ts_lag * pq.ms /10).rescale(pq.hour)

    dt = pd.DataFrame(columns=['t_start', 't_stop', 'duration',
                               'modality',
                               'intensity', 'direction_chosen',
                               'direction_cued', 'correct'])

    dt['t_start'] = t_start
    dt['t_stop'] = t_end
    dt['duration'] = t_end - t_start
    dt['modality'] = trial_modality
    dt['intensity'] = trial_intensity
    dt['direction_chosen'] = direction_chosen
    dt['direction_cued'] = direction_cued
    dt['correct'] = trial_correct
    dt['previous_direction_chosen'] = None

    for tnum, row in dt.iterrows():
        try:
            dt.loc[tnum, 'previous_direction_chosen'] = dt.loc[tnum-1, 'direction_chosen']
        except:
            dt.loc[tnum, 'previous_direction_chosen'] = 'none'

    # cell_ids = trial_navig8[area_label][sess_indx].CellID
    #
    # if area_label == 'ASession' :
    #     cell_ids_decoding = navig8['PaulsData'][sess_indx].A1.CellID
    # elif area_label == 'VSession' :
    #     cell_ids_decoding = navig8['PaulsData'][sess_indx].V2L.CellID

    # for id in cell_ids_decoding :
    #     if np.isin(id, cell_ids) :
    #         pass
    #     else :
    #         print(id)

    return dt