import os
import pickle
from constants import *
from utils import *
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
import matplotlib.gridspec as gridspec
from sklearn.preprocessing import minmax_scale
from plotting_style import *
from scipy.ndimage.filters import gaussian_filter, gaussian_filter1d
import pandas as pd
from scipy.stats.stats import pearsonr, ttest_rel
from scipy.stats import wilcoxon, mannwhitneyu


settings_name = 'aug23'

settings_name = 'dec6_all'

decoder       = 'dkw'

# plotting parameters
plot_error_correlation_per_location=False
smooth_confusion_matrices=False
smooth_error_by_bin=True
sigma_smooth_cm = 1
sigma_smooth_err_by_bin=2
running_mean_prediction_plot=False
gaussian_filter_prediction_plot=True
n_seconds_pred_plot = 60
save_plots = True
plot_format = 'png'
dpi = 400

only_one_animal = False
select_animal = 'animal3'

plot_folder = os.path.join(DATA_FOLDER, 'plots', 'decode',
                           '{}_{}'.format(settings_name, decoder))

if only_one_animal:
    plot_folder = os.path.join(DATA_FOLDER, 'plots', 'decode'
                           '{}_{}'.format(settings_name, decoder), select_animal)


if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

use_maze_distance=False

if use_maze_distance:
    error_string = 'maze_error'
else:
    error_string = 'error'


bin_distance_file = os.path.join(DATA_FOLDER, 'bin_distance.pkl')
bin_distance = pickle.load(open(bin_distance_file, 'rb'))

results_file_name = 'decode_{}_{}.pkl'.format(settings_name, decoder)
results_path = os.path.join(DATA_FOLDER, 'results', 'dec', results_file_name)
results = pickle.load(open(results_path, 'rb'))

df_full = results['decoding_results']
pars    = results['decoding_pars']


bin_locations = bin_distance[0]['bins']

if pars['coarsify_bins']:
    coarse_bins = np.arange(bin_locations.index[0],
                            bin_locations.index[-1] + pars['coarse_bin_size'],
                            pars['coarse_bin_size'])
    bin_locations['coarse_bins'] = pd.cut(bin_locations.index, coarse_bins,
                                          labels=False, right=False,
                                          include_lowest=True)
    bin_locations = bin_locations.groupby('coarse_bins').mean()


# --- PREPARE CONFUSION MATRICES ----------------------------------------------

cms = {}
for area in AREAS:

    sessions = GOOD_SESSIONS_BY_AREA[area]
    if only_one_animal:
        print(sessions)
        sessions = [s for s in sessions if np.isin(s, sessions_by_animal[select_animal])]
        print(sessions)
    df = df_full[np.isin(df_full['session'], sessions)]
    #df = df_full[np.isin(df_full['session'], 13)]
    y = df['bin'].astype(int)
    y_pred = df['{}_pred'.format(area)].astype(int)
    cm = confusion_matrix(y, y_pred)
    cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    # cmshuf = confusion_matrix(np.random.permutation(y), y_pred)
    # cmshuf = cmshuf.astype('float') / cmshuf.sum(axis=1)[:, np.newaxis]
    # cm = cm - cmshuf

    if smooth_confusion_matrices:
        cm = gaussian_filter(cm, sigma=sigma_smooth_cm)

    cms[area] = cm



# --- STATISTICS --------------------------------------------------------------

a1err = df_full[np.isin(df_full['session'], GOOD_SESSIONS_BY_AREA['A1'])].groupby('session').median().reset_index()
v2lerr = df_full[np.isin(df_full['session'], GOOD_SESSIONS_BY_AREA['V2L'])].groupby('session').median().reset_index()
t, p = mannwhitneyu(a1err['A1_error'], v2lerr['V2L_error'])
print('Comparing median error per session: p={:.4e}'.format(p))


# a1err = df_full[np.isin(df_full['session'], GOOD_SESSIONS_BY_AREA['A1'])].groupby(['session', 'bin']).median().reset_index()
# v2lerr = df_full[np.isin(df_full['session'], GOOD_SESSIONS_BY_AREA['V2L'])].groupby(['session', 'bin']).median().reset_index()
# t, p = mannwhitneyu(a1err['A1_error'], v2lerr['V2L_error'])
# print('Comparing median error per session per bin: p={:.4e}'.format(p))


a1err = df_full[np.isin(df_full['session'], GOOD_SESSIONS_BY_AREA['A1'])].groupby(['bin']).median().reset_index()
v2lerr = df_full[np.isin(df_full['session'], GOOD_SESSIONS_BY_AREA['V2L'])].groupby(['bin']).median().reset_index()
t, p = wilcoxon(a1err['A1_error'], v2lerr['V2L_error'])
print('Comparing median error per bin: p={:.4e}'.format(p))


a1err = df_full[np.isin(df_full['session'], GOOD_SESSIONS_BY_AREA['A1'])].groupby(['bin']).mean().reset_index()
v2lerr = df_full[np.isin(df_full['session'], GOOD_SESSIONS_BY_AREA['V2L'])].groupby(['bin']).mean().reset_index()
t, p = wilcoxon(a1err['A1_error'], v2lerr['V2L_error'])
print('Comparing mean error per bin: p={:.4e}'.format(p))

f, ax = plt.subplots(1, 1)
ax.scatter(a1err['A1_error'], v2lerr['V2L_error'])
ax.set_xlim([0.1, 0.5])
ax.set_ylim([0.1, 0.5])

# --- PREPARE ERROR BY SPATIAL BIN --------------------------------------------
error_per_bin = {a : {} for a in AREAS}
for area in AREAS:
    df = df_full[np.isin(df_full['session'], GOOD_SESSIONS_BY_AREA[area])]
    err_per_bin = df.groupby('bin').mean()

    error_per_bin[area] = pd.DataFrame(err_per_bin['{}_{}'.format(area, error_string)])
    error_per_bin[area].rename(columns={'{}_{}'.format(area, error_string) : 'error'},
                               inplace=True)
    # error_per_bin[area]['index'] = err_per_bin.index

np.testing.assert_array_equal(error_per_bin['A1'].index,
                              error_per_bin['V2L'].index)




# --- PLOT CONFUSION MATRICES -------------------------------------------------

f, ax = plt.subplots(2, 1, figsize=[small_square_side, 2*small_square_side])

vmin = np.hstack((cms['A1'], cms['V2L'])).min()
vmax = np.hstack((cms['A1'], cms['V2L'])).max()
cax1 = ax[0].imshow(cms['A1'], cmap='Blues', vmin=vmin, vmax=vmax)
cax2 = ax[1].imshow(cms['V2L'], cmap='Blues', vmin=vmin, vmax=vmax)

ax[0].set_title('A1')
ax[1].set_title('V2L')
ax[0].set_xlabel('Decoded location')
ax[0].set_ylabel('True location')
ax[1].set_ylabel('True location')
ax[1].set_xlabel('Decoded location')

for axx in ax:
    axx.set_yticks(axx.get_xticks()[1:-1])
    #axx.xaxis.set_ticks_position('none')
    #axx.yaxis.set_ticks_position('none')
    #axx.set_yticks([])
    #axx.set_xticks([])

sns.despine(fig=f, top=True, right=True, trim=False, offset=10)
plt.tight_layout()

if save_plots:
    plot_name = 'confusion_matrices_{}_{}.{}'.format(settings_name, decoder,
                                                     plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


# --- PLOT COLORBAR -----------------------------------------------------------

f, ax = plt.subplots(1, 1, figsize=[small_square_side, small_square_side])
cbar = plt.colorbar(cax2, ax=ax, fraction=0.046, pad=0.05,
                    orientation='vertical', label='Prob. density')
ax.axis('off')
plt.tight_layout()

if save_plots:
    plot_name = 'confusion_matrices_colorbar_{}_{}.{}'.format(settings_name, decoder,
                                                     plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


# --- PREPARE CONFUSION MATRICES PER ANIMAL ----------------------------------------------

cms = {AREAS[0]: {}, AREAS[1]: {}}
n_sess = {AREAS[0]: {}, AREAS[1]: {}}
for area in AREAS:
    sessions = GOOD_SESSIONS_BY_AREA[area]
    for animal in ['animal1', 'animal2', 'animal3']:
        sessions_animal = [s for s in sessions if np.isin(s, sessions_by_animal[animal])]
        print(sessions_animal)
        df = df_full[np.isin(df_full['session'], sessions_animal)]
        #df = df_full[np.isin(df_full['session'], 13)]
        y = df['bin'].astype(int)
        y_pred = df['{}_pred'.format(area)].astype(int)
        cm = confusion_matrix(y, y_pred)
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

        if smooth_confusion_matrices:
            cm = gaussian_filter(cm, sigma=sigma_smooth_cm)

        cms[area][animal] = cm
        n_sess[area][animal] = df['session'].unique().shape[0]

# --- PLOT CONFUSION MATRICES PER ANIMAL -------------------------------------------------

for animal in ['animal1', 'animal2', 'animal3']:
    f, ax = plt.subplots(2, 1, figsize=[small_square_side+1, 2*small_square_side+1])

    if cms['A1'][animal].shape[0] == 0:
        vmin = np.hstack((cms['V2L'][animal])).min()
        vmax = np.hstack((cms['V2L'][animal])).max()
    elif cms['V2L'][animal].shape[0] == 0:
        vmin = np.hstack((cms['A1'][animal])).min()
        vmax = np.hstack((cms['A1'][animal])).max()
    else:
        vmin = np.hstack((cms['A1'][animal], cms['V2L'][animal])).min()
        vmax = np.hstack((cms['A1'][animal], cms['V2L'][animal])).max()

    try:
        cax1 = ax[0].imshow(cms['A1'][animal], cmap='Blues', vmin=vmin, vmax=vmax)
    except:
        pass
    try:
        cax2 = ax[1].imshow(cms['V2L'][animal], cmap='Blues', vmin=vmin, vmax=vmax)
    except:
        pass
    ax[0].set_title('A1 {}\n(N={} sessions)'.format(animal, n_sess[area][animal]))
    ax[1].set_title('V2L {}\n(N={} sessions)'.format(animal, n_sess[area][animal]))
    ax[0].set_xlabel('Decoded location')
    ax[0].set_ylabel('True location')
    ax[1].set_ylabel('True location')
    ax[1].set_xlabel('Decoded location')

    for axx in ax:
        axx.set_yticks(axx.get_xticks()[1:-1])
        #axx.xaxis.set_ticks_position('none')
        #axx.yaxis.set_ticks_position('none')
        #axx.set_yticks([])
        #axx.set_xticks([])

    sns.despine(fig=f, top=True, right=True, trim=False, offset=10)
    plt.tight_layout()

    if save_plots:
        plot_name = 'confusion_matrices_{}_{}_{}.{}'.format(settings_name, decoder,
                                                         animal, plot_format)
        f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)




# --- PLOT ERROR CORRELATION ---------------------------------------------------

np.testing.assert_array_equal(error_per_bin['A1']['error'].index,
                              error_per_bin['V2L']['error'].index)

x = error_per_bin['A1']['error']
y = error_per_bin['V2L']['error']
rho, p_val_err = pearsonr(x, y)

print('corr rho={:.5} p-val={:.4e}'.format(rho,p_val_err))

f, ax = plt.subplots(1, 1, figsize=[small_square_side, small_square_side])
ax.scatter(x, y, edgecolor='k', facecolor='w', zorder=5, s=22)
ax.plot([0, 1], [0, 1], transform=ax.transAxes, ls='--', color='k', alpha=0.8)

ax.set_xlabel('A1 average error (m)')
ax.set_ylabel('V2L average error (m)')
#ax.yaxis.set_label_position("right")
ax.axis('equal')
minlim = min(ax.get_xlim()[0], ax.get_ylim()[0])
maxlim = max(ax.get_xlim()[1], ax.get_ylim()[1])
ax.set_ylim([minlim, maxlim])
ax.set_xlim([minlim, maxlim])
plt.locator_params(axis='y', nbins=3)
plt.locator_params(axis='x', nbins=3)
# ax.set_yticks([0, 0.2, 0.4, 0.6])


corr_text = r'$\rho=${:.2f}'.format(rho)
corr_text = r'$\rho=${:.2f}'.format(rho)

ax.text(minlim+minlim/4, maxlim-maxlim/4, corr_text)
sns.despine()
#ax.grid()
plt.tight_layout()

if save_plots:
    plot_name = 'error_correlation_{}_{}.{}'.format(settings_name, decoder,
                                                     plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


# --- PLOT AVERAGE ERROR PER LOCATION -----------------------------------------


scale_min, scale_max = 2, 35
if pars['coarsify_bins']:
    scale_min, scale_max = 5, 45

f, ax = plt.subplots(1, 1, figsize=[small_square_side, small_square_side])

error_per_bin_scaled = {a : {} for a in AREAS}

for i, area in enumerate(AREAS):
    bins = error_per_bin[area].index
    error_scaled = min_max_scale(error_per_bin[area], scale_min, scale_max)

    error_per_bin_scaled = pd.DataFrame(index=bins, columns=['error'])
    error_per_bin_scaled['error'] = error_scaled

    for bin in bins:
        err = error_per_bin_scaled.loc[bin]['error']
        x, y = bin_locations.loc[bin]
        if area == 'V2L':
            x = x + 0.1
            y = y + 0.05
        ax.scatter(x, y, s=err, facecolor=area_colormap[area], alpha=0.9,
                   edgecolor='none')
    ax.axis('off')


if save_plots:
    plot_name = 'error_on_maze_{}_{}.{}'.format(settings_name, decoder,
                                                     plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


# --- PLOT ERROR KDE ----------------------------------------------------------

f, ax = plt.subplots(1, 1, figsize=[small_square_side, small_square_side])

for area in AREAS:
    df = df_full[np.isin(df_full['session'], GOOD_SESSIONS_BY_AREA[area])]
    sns.kdeplot(df['{}_{}'.format(area, error_string)], shade=True, color=area_colormap[area],
                ax=ax)
ax.set_xlim([0, ax.get_xlim()[1]])
ax.set_ylabel('Prob. density')
ax.set_xlabel('Decoding error (m)')
ax.get_legend().remove()
ax.set_yticks([])
sns.despine(top=True, right=True, left=True)
plt.tight_layout()

if save_plots:
    plot_name = 'error_kde_{}_{}.{}'.format(settings_name, decoder,
                                                     plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


# --- PLOT ERROR BARPLOT ------------------------------------------------------
bin_barplot = error_per_bin['A1'].index
f, ax = plt.subplots(2, 1, figsize=[small_square_side, small_square_side],
                     sharex=True, sharey=True)
ax[0].bar(bin_barplot, error_per_bin['A1']['error'], width=1,
       color=area_colormap['A1'], label='A1', alpha=0.4)
ax[1].bar(bin_barplot, error_per_bin['V2L']['error'], width=1,
       color=area_colormap['V2L'], label='V2L', alpha=0.4)

for axx in ax:
    axx.set_yticks([0, 0.5])
    if not pars['coarsify_bins']:
        axx.set_xlim([1, axx.get_xlim()[1]])
        axx.set_xticks([0, 25, 50, 75, 100])
f.text(0.04, 0.5, 'Mean error', va='center', rotation='vertical')
ax[1].set_xlabel('Location')
sns.despine()

plt.tight_layout(pad=1.8, h_pad=0)

if save_plots:
    plot_name = 'error_barplot_{}_{}.{}'.format(settings_name, decoder,
                                                     plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)

# --- PLOT PREDICTION IN TIME -------------------------------------------------

session_prediction_plot = 6
df = df_full[df_full['session'] == session_prediction_plot]
n_samples = df['bin'].shape[0]
n_samp_pred_plot = int(n_seconds_pred_plot * 1000 / pars['binsize_in_ms'])
st_s = n_samples // 5

true_pos  = df['bin'].__array__()[st_s : st_s + n_samp_pred_plot]
A1_pred   = df['A1_pred'].__array__()[st_s : st_s + n_samp_pred_plot]
V2L_pred  = df['V2L_pred'].__array__()[st_s : st_s + n_samp_pred_plot]

if running_mean_prediction_plot:
    true_pos = running_mean(true_pos, 5)
    A1_pred = running_mean(A1_pred, 5)
    V2L_pred = running_mean(V2L_pred, 5)

if gaussian_filter_prediction_plot:
    true_pos = gaussian_filter1d(true_pos, 1)
    A1_pred = gaussian_filter1d(A1_pred, 1)
    V2L_pred = gaussian_filter1d(V2L_pred, 1)

pred_time_in_s = np.arange(0, A1_pred.shape[0]) * pars['binsize_in_ms'] / 1000



f, ax = plt.subplots(figsize=[2*small_square_side,  0.5*small_square_side])
ax.plot(pred_time_in_s, true_pos, c='k', label='True')
ax.plot(pred_time_in_s, A1_pred, c=area_colormap['A1'], label='A1', alpha=0.7)
ax.plot(pred_time_in_s, V2L_pred, c=area_colormap['V2L'], label='V2L', alpha=0.7)
ax.set_xlabel('Time (s)')
ax.set_ylabel('Location (bin)')
sns.despine()
plt.tight_layout()

if save_plots:
    plot_name = 'location_prediction_over_time_{}_{}.{}'.format(settings_name, decoder,
                                                     plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
