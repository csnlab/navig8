import pickle
from constants import *

sessions = ALL_SESSIONS


for sess_ind in sessions:
    # --- LOAD DATA ---------------------------------------------------------------
    #print('Session {}'.format(sess_ind))
    file_name = 'navig8_session_{}.pkl'.format(sess_ind)
    bl = pickle.load(open(os.path.join(DATA_FOLDER, file_name), 'rb'))
    segment = bl.segments[0]
    A1_ind = [i for i, t in enumerate(segment.spiketrains)
                       if t.annotations['area'] == 'A1']
    V2L_ind = [i for i, t in enumerate(segment.spiketrains)
                       if t.annotations['area'] == 'V2L']

    print('S {:03}: - {:02} - {:02}'.format(sess_ind, len(A1_ind), len(V2L_ind)))

