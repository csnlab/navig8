import os
import pickle
from constants import *
from utils import *
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
import matplotlib.gridspec as gridspec
from sklearn.preprocessing import minmax_scale
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import ticker
from plotting_style import *
from scipy.ndimage.filters import gaussian_filter, gaussian_filter1d
import pandas as pd
from scipy.stats.stats import pearsonr, ttest_rel
from scipy.stats import wilcoxon, mannwhitneyu

settings_name = 'aug23'
decoder       = 'rf'

# plotting parameters
plot_correlation_scatter        = True
plot_correlation_vs_error       = True
plot_error_over_time            = False
running_mean_prediction_plot    = False
gaussian_filter_prediction_plot = True
n_speed_bins                    = 5
sess_ind_error_heatmap          = 0
average_sessions_error_heatmap  = True
n_seconds_pred_plot             = 60
save_plots                      = True
plot_format                     = 'svg'
dpi                             = 400
include_sessions                = 'good_in_both_areas' # 'all' or 'good_in_both_areas'

plot_folder = os.path.join(DATA_FOLDER, 'plots', 'decode',
                           '{}_{}'.format(settings_name, decoder))

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

use_maze_distance=True

if include_sessions == 'all':
    sessions = list(GOOD_SESSIONS_ONE_AREA.keys())
elif include_sessions == 'good_in_both_areas':
    sessions = GOOD_SESSIONS_BOTH_AREAS
elif include_sessions == 'animal1':
    sessions = [0, 1, 2, 3, 4, 5, 6]
elif include_sessions == 'animal2':
    sessions = [7, 8, 9]
elif include_sessions == 'animal3':
    sessions = [15]

error_strings = ['error_x', 'error_y']

bin_distance_file = os.path.join(DATA_FOLDER, 'bin_distance.pkl')
bin_distance = pickle.load(open(bin_distance_file, 'rb'))

results_file_name = 'decode_{}_{}.pkl'.format(settings_name, decoder)
results_path = os.path.join(DATA_FOLDER, 'results', 'dec', results_file_name)
results = pickle.load(open(results_path, 'rb'))

df_full = results['decoding_results']
pars    = results['decoding_pars']

# -----------------------------------------------------------------------------

# SET RANDOM SEED
np.random.seed(15)

# -----------------------------------------------------------------------------

for error_string in error_strings:

    cor_df = pd.DataFrame(columns=['session', 'coordinate', 'rho', 'p_val', 'shuffled'])
    df_w_shuf = []

    for sess_ind in sessions:
        df = df_full[df_full['session'] == sess_ind]
        #df = df_full

        df['speed_binned'] = pd.qcut(np.abs(df['speed']), n_speed_bins, labels=False)

        a1_err = df['A1_{}'.format(error_string)].as_matrix()
        v2l_err = df['V2L_{}'.format(error_string)].as_matrix()

        bins = df['bin'].unique()
        speed_bins = df['speed_binned'].unique()

        v2l_err_shuf = []
        a1_err_not_shuf = []
        for bin in bins:
            for speed_bin in speed_bins:
                ind = np.where(np.logical_and(df['bin'] == bin,
                                              df['speed_binned'] == speed_bin))[0].flatten()
                #print(bin, speed_bin,ind)
                # if len(ind) > 1:

                shuf_ind = np.random.permutation(ind)
                v2l_err_shuf.append(v2l_err[shuf_ind])
                a1_err_not_shuf.append(a1_err[ind])

        v2l_err_shuf = np.hstack(v2l_err_shuf)

        # TODO WATCH OUT FOR THIS
        a1_err_not_shuf = np.hstack(a1_err_not_shuf)

        rho, p_val = pearsonr(a1_err, v2l_err)
        rho_shuf, p_val_shuf = pearsonr(a1_err_not_shuf, v2l_err_shuf)

        cor_df.loc[cor_df.shape[0]] = [sess_ind, error_string, rho, p_val, False]
        cor_df.loc[cor_df.shape[0]] = [sess_ind, error_string, rho_shuf, p_val_shuf, True]

        df['V2L_{}_shuffled'.format(error_string)] = v2l_err_shuf
        df_w_shuf.append(df)

    df_w_shuf = pd.concat(df_w_shuf)


    # --- STATISTICS --------------------------------------------------------------

    measured = cor_df.loc[cor_df['shuffled'] == False, :]
    shuffled = cor_df.loc[cor_df['shuffled'] == True, :]
    measured = measured.sort_values(by='session')
    shuffled = shuffled.sort_values(by='session')

    x = measured['rho']
    y = shuffled['rho']

    stat, p_val_w = wilcoxon(x, y)

    print('\n\nmeasured: {}, shuffled: {}'.format(np.mean(x), np.mean(y)))
    print('For {} wilcoxon test p-val: {}\n\n'.format(error_string, p_val_w))

    # --- PLOT CORRELATION WITH AND WITHOUT BIN-SPEED SHUFFLING -------------------

    if plot_correlation_scatter:
        measured = cor_df.loc[cor_df['shuffled'] == False, :]
        shuffled = cor_df.loc[cor_df['shuffled'] == True, :]
        measured = measured.sort_values(by='session')
        shuffled = shuffled.sort_values(by='session')
        x = measured['rho']
        y = shuffled['rho']

        f, ax = plt.subplots(1, 1, figsize=[small_square_side, small_square_side])
        ax.scatter(x, y, edgecolor='k', facecolor='w', zorder=5, s=28)

        ax.plot([0, 1], [0, 1], transform=ax.transAxes, ls='--', color='k',
                alpha=0.5)
        ax.axis('square')
        # minlim = min(ax.get_xlim()[0], ax.get_ylim()[0])
        # maxlim = max(ax.get_xlim()[1], ax.get_ylim()[1])
        # ax.set_ylim([0.1, 0.5])
        # ax.set_xlim([0.1, 0.5])
        ax.set_ylim([0.1, 0.7])
        ax.set_xlim([0.1, 0.7])
        # plt.locator_params(axis='y', nbins=3)
        # plt.locator_params(axis='x', nbins=3)
        # ax.set_xticks([0.1, 0.2, 0.3, 0.4, 0.5])
        # ax.set_yticks([0.1, 0.2, 0.3, 0.4, 0.5])
        ax.set_xticks([0.2, 0.4, 0.6])
        ax.set_yticks([0.2, 0.4, 0.6])
        ax.set_xlabel('A1 - V2L {} \ncorrelation in time, measured'.format(error_labels[error_string]))
        ax.set_ylabel('A1 - V2L {} \ncorrelation in time, shuffled'.format(error_labels[error_string]))
        sns.despine()
        plt.tight_layout()
        ax.axvline(0, c=sns.xkcd_rgb['light blue'], ls=':', alpha=1, zorder=1)
        ax.axhline(0, c=sns.xkcd_rgb['light blue'], ls=':', alpha=1, zorder=1)


        if save_plots:
            plot_name = '{}_corr_speed_shuffle_{}_{}_{}.{}'.format(error_string, settings_name, decoder,
                                                             include_sessions, plot_format)
            f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



    # --- PLOT CORRELATION AS A FUNCTION OF ERROR ---------------------------------

    if plot_correlation_vs_error:
        sessions_err = measured['session']
        err_per_sess = df_full.loc[np.isin(df_full['session'], sessions_err)].groupby('session').mean().reset_index()

        for area in AREAS:
            area_err_per_sess  = err_per_sess['{}_{}'.format(area, error_string)]

            # a1_err_per_sess  = err_per_sess['A1_{}'.format('error')]
            # v2l_err_per_sess = err_per_sess['V2L_{}'.format('error')]

            y = measured['rho'].as_matrix() - shuffled['rho'].as_matrix()
            x = area_err_per_sess

            rho, p = pearsonr(x, y)
            print('correlation between error correlation and')
            print('{} error, {}: rho={:.4}, p-val={:.4}'.format(area, error_string, rho, p))

            f, ax = plt.subplots(1, 1, figsize=[small_square_side, small_square_side])
            ax.scatter(x, y, edgecolor='k', facecolor='w', zorder=5, s=28)
            ax.plot([0, 1], [0, 1], transform=ax.transAxes, ls='--', color='k',
                    alpha=0.5)
            ax.axis('square')
            minlim = min(ax.get_xlim()[0], ax.get_ylim()[0])
            maxlim = max(ax.get_xlim()[1], ax.get_ylim()[1])
            ax.set_ylim([minlim, maxlim])
            ax.set_xlim([minlim, maxlim])
            plt.locator_params(axis='y', nbins=4)
            plt.locator_params(axis='x', nbins=4)
            sns.despine()
            plt.tight_layout()
            ax.axvline(0, c=sns.xkcd_rgb['light blue'], ls=':', alpha=1, zorder=1)
            ax.axhline(0, c=sns.xkcd_rgb['light blue'], ls=':', alpha=1, zorder=1)

            ax.set_xlabel('{} error {} (m)'.format(area, error_labels[error_string]))
            ax.set_ylabel('A1 - V2L error correlation \nabove shuffled')
            plt.tight_layout()

            if save_plots:
                plot_name = 'error_correlation_vs_{}_{}_{}_{}_{}.{}'.format(
                     area, error_string, settings_name, decoder,include_sessions, plot_format)
                f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)

    # --- PLOT ERROR OVER TIME ----------------------------------------------------


    if plot_error_over_time:
        session_error_over_time_plot = 6
        df = df_w_shuf[df_w_shuf['session'] == session_error_over_time_plot]
        n_samples = df['bin'].shape[0]
        n_samp_pred_plot = int(n_seconds_pred_plot * 1000 / pars['binsize_in_ms'])
        st_s = n_samples // 5

        true_pos  = df['bin'].as_matrix()[st_s : st_s + n_samp_pred_plot]
        A1_err   = df['A1_{}'.format(error_string)].as_matrix()[st_s : st_s + n_samp_pred_plot]
        V2L_err  = df['V2L_{}'.format(error_string)].as_matrix()[st_s : st_s + n_samp_pred_plot]
        V2L_err_shuf  = df['V2L_{}_shuffled'.format(error_string)].as_matrix()[st_s : st_s + n_samp_pred_plot]


        if running_mean_prediction_plot:
            A1_err = running_mean(A1_err, 5)
            V2L_err_shuf = running_mean(V2L_err_shuf, 5)

        if gaussian_filter_prediction_plot:
            A1_err = gaussian_filter1d(A1_err, 1)
            V2L_err = gaussian_filter1d(V2L_err, 1)
            V2L_err_shuf = gaussian_filter1d(V2L_err_shuf, 1)

        pred_time_in_s = np.arange(0, A1_err.shape[0]) * pars['binsize_in_ms'] / 1000

        f, ax = plt.subplots(figsize=[2*small_square_side, 1 * small_square_side])
        ax.plot(pred_time_in_s, A1_err, c=area_colormap['A1'], label='A1', alpha=0.7)
        ax.plot(pred_time_in_s, V2L_err, c=area_colormap['V2L'], label='V2L', alpha=0.7)
        ax.plot(pred_time_in_s, V2L_err_shuf, c='grey',
                label='V2L shuffled', alpha=0.7)
        ax.set_xlabel('Time (s)')
        ax.set_ylabel('{}'.format(error_labels[error_string]))
        sns.despine()
        plt.tight_layout()





    # --- PREPARE ERROR CORRELATION MATRICES --------------------------------------

    nbins = 80
    imshow_lim = 0.3
    smooth = True
    sigma_smooth_joint = 4
    error_mats = {error_string : {s : {} for s in sessions},
                  'xb'      : {s : {} for s in sessions},
                  'yb'      : {s: {} for s in sessions}}

    for sess_ind in sessions:
        df = df_w_shuf[df_w_shuf['session'] == sess_ind]

        ea1 = df['A1_{}'.format(error_string)]
        ev2l = df['V2L_{}'.format(error_string)]
        ev2l_shuf = df['V2L_{}_shuffled'.format(error_string)]

        mat, xb, yb, im = plt.hist2d(ea1, ev2l, bins=(nbins, nbins), cmap=plt.cm.jet)
        mat_shuf, xbs, ybs, ims = plt.hist2d(ea1, ev2l_shuf, bins=(nbins, nbins), cmap=plt.cm.jet)
        mat_diff = mat - mat_shuf

        if smooth:
            mat = gaussian_filter(mat, sigma=sigma_smooth_joint)
            mat_shuf = gaussian_filter(mat_shuf, sigma=sigma_smooth_joint)
            mat_diff = gaussian_filter(mat_diff, sigma=sigma_smooth_joint)

        error_mats[error_string][sess_ind]['mat'] = mat
        error_mats[error_string][sess_ind]['mat_shuf'] = mat_shuf
        error_mats[error_string][sess_ind]['mat_diff'] = mat_diff

        error_mats['xb'][sess_ind] = xb
        error_mats['yb'][sess_ind] = yb



    # --- PLOT ERROR CORRELATION MATRICES (ALL) -----------------------------------

    if False:
        f, ax = plt.subplots(1, 3, figsize=[small_square_side*3, small_square_side])

        if average_sessions_error_heatmap:
            mat =  np.sum([error_mats[error_string][sess_ind]['mat'] for sess_ind
                    in sessions], axis=0) / len(sessions)
            mat_shuff =  np.sum([error_mats[error_string][sess_ind]['mat_shuf'] for sess_ind
                    in sessions], axis=0) / len(sessions)
            mat_diff =  np.sum([error_mats[error_string][sess_ind]['mat_diff'] for sess_ind
                    in sessions], axis=0) / len(sessions)

            xb =  np.sum([error_mats['xb'][sess_ind] for sess_ind
                    in sessions], axis=0) / len(sessions)
            yb =  np.sum([error_mats['yb'][sess_ind] for sess_ind
                    in sessions], axis=0) / len(sessions)


        else:
            mat = error_mats[error_string][sess_ind]['mat']
            mat_shuff = error_mats[error_string][sess_ind]['mat_shuf']
            mat_diff = error_mats[error_string][sess_ind]['mat_diff']
            xb = error_mats['xb'][sess_ind]
            yb = error_mats['yb'][sess_ind]


        extent = (xb[0] - np.diff(xb)[0], xb[-1], yb[0] - np.diff(yb)[0], yb[-1])

        im  = ax[0].imshow(mat.T, cmap=plt.cm.jet, origin='lower', extent=extent)

        ims = ax[1].imshow(mat_shuf.T, cmap=plt.cm.jet, origin='lower',
                               vmin=im.get_clim()[0], vmax=im.get_clim()[1], extent=extent)

        im_diff = ax[2].imshow(mat_diff.T, cmap=plt.cm.jet, origin='lower', extent=extent)

        plt.colorbar(im, ax=ax[0])
        plt.colorbar(ims, ax=ax[1])
        plt.colorbar(im_diff, ax=ax[2])


        for axx in ax.flatten():
            axx.set_xlim(-imshow_lim, imshow_lim)
            axx.set_ylim(-imshow_lim, imshow_lim)

        for axx in ax.flatten():
            axx.set_ylabel('V2L')
            axx.set_xlabel('A1')

        for axx in ax.flatten():
            axx.set_xticks([-imshow_lim, 0, imshow_lim])
            axx.set_yticks([-imshow_lim, 0, imshow_lim])

        plt.tight_layout()

        # if save_plots:
        #     if average_sessions_error_heatmap:
        #         plot_name = 'error_corr_heatmap_{}_{}.{}'.format(settings_name, decoder,
        #                                                          plot_format)
        #     else:
        #         plot_name = 'error_corr_heatmap_{}_{}_s{}.{}'.format(settings_name, decoder,
        #                                                          sess_ind_error_heatmap,
        #                                                              plot_format)
        #     f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



    # --- PLOT ERROR CORRELATION MATRICES (measured - shuffled) -------------------

    f, ax = plt.subplots(1, 1, figsize=[small_square_side, small_square_side])

    if average_sessions_error_heatmap:
        mat_diff =  np.sum([error_mats[error_string][sess_ind]['mat_diff'] for sess_ind
                in sessions], axis=0) / len(sessions)

        xb =  np.sum([error_mats['xb'][sess_ind] for sess_ind
                in sessions], axis=0) / len(sessions)
        yb =  np.sum([error_mats['yb'][sess_ind] for sess_ind
                in sessions], axis=0) / len(sessions)

    else:
        mat_diff = error_mats[error_string][sess_ind_error_heatmap]['mat_diff']
        xb = error_mats['xb'][sess_ind_error_heatmap]
        yb = error_mats['yb'][sess_ind_error_heatmap]

    extent = (xb[0] - np.diff(xb)[0], xb[-1], yb[0] - np.diff(yb)[0], yb[-1])
    im_diff = ax.imshow(mat_diff.T, cmap=plt.cm.jet, origin='lower', extent=extent)

    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)

    cb = plt.colorbar(im_diff, cax=cax)
    cb.set_label('$\Delta$ Prob. density / chance')

    tick_locator = ticker.MaxNLocator(nbins=5)
    cb.locator = tick_locator
    cb.update_ticks()

    ax.set_xlim(-imshow_lim, imshow_lim)
    ax.set_ylim(-imshow_lim, imshow_lim)
    ax.set_ylabel('V2L {} (m)'.format(error_labels[error_string]))
    ax.set_xlabel('A1 {} (m)'.format(error_labels[error_string]))
    ax.set_xticks([-imshow_lim, 0, imshow_lim])
    ax.set_yticks([-imshow_lim, 0, imshow_lim])

    ax.set_title('Measured - shuffled', {'fontsize' : 11})
    sns.despine(fig=f, trim=True, offset=8)
    plt.tight_layout()


    if save_plots:
        if average_sessions_error_heatmap:
            plot_name = '{}_corr_heatmap_{}_{}_{}.{}'.format(error_string, settings_name,
                                                          decoder, include_sessions, plot_format)
        else:
            plot_name = '{}_corr_heatmap_{}_{}_s{}.{}'.format(error_string,
                                                              settings_name, decoder,
                                                             sess_ind_error_heatmap,
                                                                 plot_format)
        f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



