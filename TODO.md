Questions

1. Can we decode spatial location of the animal?

2. Are certain locations easier to predict than others? In other words
how are errors distributed on the maze?

3. Is the distribution of errors on the maze correlated between the
two areas? YES

4. Are the decoding error correlated in time? Test by shuffling time bins
of one area (shuffle per spatial bin and speed quantile). YES

5. How does the decoding performance depend on the number of
neurons? In other words: is for example the fact that V2L seems better
at decoding only due to different number of neurons?
Decode with random subgroups of neurons of different sizes.

6. Does performance rely only a few good neurons? Bootstrapping/jackknifing



## ERROR CORRELATION

1. Error in A1 and V2L, for both x and y direction, are significantly
correlated with pearson correlation coefficients which are higher than
for the shuffled ones (t-test).

2. The measured-shuffled density map (shown for a single position and
averaged across position and sessions) has a diagonal structure.


# THE STORY
A1 has spatial information. V2L does too, but in the case of V2L it could
be explained by visual information, whereas in A1 maybe a bit less. More
importantly, the two areas seem to have a shared representation, the field density
per spatial bin (the amount of firing fields which include a given bin)
is very correlated across the two areas, suggesting a shared spatial
representation that gets broadcasted from the Hippocampus for example.
We need to exclude a prominent effect of speed and head direction though
(otherwise it could simply be that the two areas appear correlated
through speed). Supporting the idea that the areas share the same spatial representation,
decoding errors are correlated in space and time.

Tentative statement: V2L and A1 get the same spatial representation
from somewhere else, but we find apparently more spatial information
in V2L because of visual flow. However, once we condition on head direction,
the difference between A1 and V2L disappears, meaning that head direction
is more important for V2L that for A1.

can we exclude the interaction effect of location and head direction?

If we filter speed, then it seems that position is more informative than
head direction, but that is not the case if we don't filter speed.

# DONE
3. In the example encoding plot, turn the spike count to firing rate.
4. For the encoding, we use 2D position, maybe switch to bins for
consistency. DONE----> RUNNING.
Speed was already set to the absolute value, so the only thing is running
the encoding with the bins. Not so good, but then again other predictors
are not binned, so best to keep as is. So for the encoding nothing
is binned.
6. One possible addition: using the decoded position in one area to predict
the decoded position in the other area! I would have to add the other
predictors to the df_full produced in decode_compute, then I could load
it and predict the predicted location. Use a random forest.
Need to keep track of the time points of the decoding because we
shuffle, so need to add it in there.
7. Switch color code


# NOT DONE
2. Can you improve decoding by further increasing speed threshold?
3. Can you improve the decoding by selecting only neurons with a certain
amount of spikes during the whole period?


# ACTUAL TODO:
1. update the jackknife subplot. Update error computation,
and shuffle kfold. In the jackknife also we don't shuffle! We probably should
9. correlation heatmap per animal



# SUPPLEMENTARY FIGURES:
- Decoding with other decoders
- Distribution of jackknife scores


# IMPORTANT
recheck the shuffling of error correlation, you've made one important
fix because you were accidentally shuffling too much!

Animal 1: sessions 0 - 6
animal 2: sessions 7 - 12
animal 3: sessions 13 - 16