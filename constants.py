import os
import numpy as np
DATA_FOLDER = '/media/pietro/bigdata/neuraldata/navig8'
ORIGINAL_DATA_PATH = '/media/pietro/bigdata/neuraldata/navig8/PaulsDataNew.mat'
MAZE_DISTANCE_PATH = '/media/pietro/bigdata/neuraldata/navig8/BinwiseLinearDistances.mat'

DATA_FOLDER='/Users/pietro/data/navig8'
ORIGINAL_DATA_PATH = '/Users/pietro/data/navig8/navig8.mat'
MAZE_DISTANCE_PATH = '/Users/pietro/data/navig8/BinwiseLinearDistances.mat'
ORIGINAL_TRIAL_DATA_PATH = '/Users/pietro/data/navig8/PaulSpikesBehaviorRatemaps.mat'


user = os.environ['USER']

if user == 'pietro':
    jarLocation = '/home/pietro/code/jidt/infodynamics.jar'
elif user == 'pmarche1':
    jarLocation = '/home/pmarche1/code/jidt/infodynamics.jar'

pixels_to_meters = 0.00205
spatial_bins_to_meters = 0.0328

# Animal 1: sessions 0 - 6
# animal 2: sessions 7 - 12
# animal 3: sessions 13 - 16

ALL_SESSIONS = list(range(17))

AREAS = ['A1', 'V2L']


#GOOD_SESSIONS_BOTH_AREAS = [0, 2, 3, 4, 5, 6, 8, 15]

# GOOD_SESSIONS_ONE_AREA = {0 : ['A1', 'V2L'],
#                           1 : ['V2L'],
#                           2 : ['A1', 'V2L'],
#                           3 : ['A1', 'V2L'],
#                           4 : ['A1', 'V2L'],
#                           5 : ['A1', 'V2L'],
#                           6 : ['A1', 'V2L'],
#                           8 : ['A1', 'V2L'],
#                           9 : ['A1'],
#                           10: ['A1'],
#                           11: ['A1'],
#                           12: ['V2L'],
#                           13: ['A1'],
#                           14: ['A1'],
#                           15: ['A1', 'V2L']}# This V2L has 16 units

sessions_by_animal = {'animal1' : np.array([0, 1, 2, 3, 4, 5, 6]),
                      'animal2' : np.array([7, 8, 9, 10, 11, 12]),
                      'animal3' : np.array([13, 14, 15, 16])}


animals_by_session = {0 : '1',
                      1 : '1',
                      2 : '1',
                      3 : '1',
                      4 : '1',
                      5 : '1',
                      6 : '1',
                      7 : '2',
                      8 : '2',
                      9 : '2',
                      10 : '2',
                      11 : '2',
                      12 : '2',
                      13 : '3',
                      14 : '3',
                      15 : '3',
                      16 : '3'
                      }

GOOD_SESSIONS_BOTH_AREAS = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 15]

GOOD_SESSIONS_ONE_AREA = {0 : ['A1', 'V2L'],
                          1 : ['A1', 'V2L'],
                          2 : ['A1', 'V2L'],
                          3 : ['A1', 'V2L'],
                          4 : ['A1', 'V2L'],
                          5 : ['A1', 'V2L'],
                          6 : ['A1', 'V2L'],
                          7 : ['A1', 'V2L'],
                          8 : ['A1', 'V2L'],
                          9 : ['A1', 'V2L'],
                          10: ['A1'],
                          11: ['A1'],
                          12: ['V2L'],
                          13: ['A1'],
                          14: ['A1'],
                          15: ['A1', 'V2L']}

GOOD_SESSIONS_BY_AREA = {'A1' : [k for k in GOOD_SESSIONS_ONE_AREA.keys()
                                 if np.isin('A1', GOOD_SESSIONS_ONE_AREA[k])],
                         'V2L': [k for k in GOOD_SESSIONS_ONE_AREA.keys()
                                if np.isin('V2L', GOOD_SESSIONS_ONE_AREA[k])]}