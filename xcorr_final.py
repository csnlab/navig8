import os
from loadmat import loadmat
from constants import *
import mat73
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

xcpath = os.path.join(DATA_FOLDER, 'XCData_combo.mat')

xc = mat73.loadmat(xcpath)


plot_folder = os.path.join(DATA_FOLDER, 'plots', 'xcorr')
if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

df = pd.DataFrame(columns=['session_id', 'rat_id', 'AC_id', 'V2L_id'])

df['session_id'] = [int(id) for id in xc['Session_id']]
df['rat_id'] = [int(id) for id in xc['Rat_id']]
df['AC_id'] = [int(id) for id in xc['AC_id']]
df['V2L_id'] = [int(id) for id in xc['V2L_id']]
# df['AC_id'] = ['{}_{}'.format(int(s), int(u)) for s, u in zip(xc['Session_id'], xc['AC_id'])]
# df['V2L_id'] = ['{}_{}'.format(int(s), int(u)) for s, u in zip(xc['Session_id'], xc['V2L_id'])]

xcarr = xc['XCorrs']

t = np.arange(xcarr.shape[1]) - 151

# xc_before = xcarr[:, 140:150]
# xc_after  = xcarr[:, 151:161]

# --- OPTION 1 -----------------------------------------------------------------
xcarr_for_df = np.vstack([np.hstack((t[:, np.newaxis], x[:, np.newaxis])) for x in xcarr])
xcarr_df = pd.DataFrame(xcarr_for_df, columns=['t', 'xcorr'])

f, ax = plt.subplots(1, 1)
sns.lineplot(data=xcarr_df, x='t', y='xcorr', ax=ax)
sns.despine()
plt.tight_layout()


xcarr_df['epoch'] = 'none'
xcarr_df.loc[np.isin(xcarr_df['t'], np.arange(-10, 0)), 'epoch'] = 'before'
xcarr_df.loc[np.isin(xcarr_df['t'], np.arange(1, 11)), 'epoch'] = 'after'




# --- PLOT HIST FULL EPOCH -----------------------------------------------------
min_time = -50
max_time = 50
use_min_max_time = False

if use_min_max_time:
    pass
else:
    min_time = -150
    max_time = 150

du = pd.DataFrame(columns=['AC_id', 'peak_time', 'before'])
for acid in df['AC_id'].unique():
    idx = np.where(df['AC_id'] == acid)[0]

    peak_time = t[xcarr[idx, :].mean(axis=0).argmax()]
    before = peak_time < 0

    du.loc[du.shape[0], :] = [acid, peak_time, before]


du = du[(du['peak_time'] >= min_time) & (du['peak_time'] <= max_time)]

n_before = du.before.value_counts().loc[True]
n_after = du.before.value_counts().loc[False]

f, ax = plt.subplots(1, 1, figsize=[3, 3])
sns.distplot(du['peak_time'], hist=True, kde=False, ax=ax,
             bins=np.arange(min_time, max_time, 10), norm_hist=True,
             color=sns.xkcd_rgb['black'], hist_kws={'color' : 'k', 'alpha' : 0.8})
ax.axvline(0, c='r', ls='--')
ax.set_xlabel('Peak correlation lag (ms)')
ax.set_ylabel('Density')
ax.set_xlim([-150, 150])
ax.set_xticks([-150, 0, 150])
ax.set_yticks([0, 0.01])
sns.despine()
plt.tight_layout()

plot_name = 'peak_hist_full.svg'
f.savefig(os.path.join(plot_folder, plot_name), dpi=400)



# --- OPTION 5 -----------------------------------------------------------------
# look at peak times per unit
min_time = -20
max_time = 20
use_min_max_time = True

if use_min_max_time:
    pass
else:
    min_time = -150
    max_time = 150

du = pd.DataFrame(columns=['AC_id', 'peak_time', 'before'])
for acid in df['AC_id'].unique():
    idx = np.where(df['AC_id'] == acid)[0]

    peak_time = t[xcarr[idx, :].mean(axis=0).argmax()]
    before = peak_time < 0

    du.loc[du.shape[0], :] = [acid, peak_time, before]


du = du[(du['peak_time'] >= min_time) & (du['peak_time'] <= max_time)]

n_before = du.before.value_counts().loc[True]
n_after = du.before.value_counts().loc[False]

f, ax = plt.subplots(1, 1, figsize=[3, 3])
sns.distplot(du['peak_time'], hist=True, kde=False, ax=ax,
             bins=np.arange(min_time, max_time, 1), norm_hist=True,
             color=sns.xkcd_rgb['black'], hist_kws={'color' : 'k', 'alpha' : 0.8})
ax.axvline(0, c='r', ls='--')
ax.set_xlabel('Peak correlation lag (ms)')
ax.set_ylabel('Density')
ax.set_xlim([min_time, max_time])
ax.set_xticks([min_time, 0, max_time])
#ax.set_yticks([0, 0.01])
sns.despine()
plt.tight_layout()

plot_name = 'peak_hist_red.svg'
f.savefig(os.path.join(plot_folder, plot_name), dpi=400)

from statsmodels.stats.proportion import proportions_ztest

stat, pval = proportions_ztest(n_before, nobs=n_before+n_after, value=0.5)

print(pval)
print(pval<0.05)


# --- OPTION 5 -----------------------------------------------------------------
# look at peak times per unit with bootstrap - count
min_time = -20
max_time = 20
use_min_max_time = True

if use_min_max_time:
    pass
else:
    min_time = -150
    max_time = 150

n_boots = 500
ac_units = df['AC_id'].unique()

dbo = pd.DataFrame(columns=['boot', 'n_before', 'n_after', 'n_tot'])

for n in range(n_boots):
    sel_ids = np.random.choice(ac_units, size=len(ac_units), replace=True)

    du = pd.DataFrame(columns=['AC_id', 'peak_time', 'before'])

    for acid in sel_ids:
        idx = np.where(df['AC_id'] == acid)[0]
        peak_time = t[xcarr[idx, :].mean(axis=0).argmax()]
        before = peak_time < 0
        du.loc[du.shape[0], :] = [acid, peak_time, before]

    du = du[(du['peak_time'] > min_time) & (du['peak_time'] < max_time)]
    n_before = du.before.value_counts().loc[True]
    n_after = du.before.value_counts().loc[False]

    dbo.loc[dbo.shape[0], :] = [n, n_before, n_after, n_before+n_after]


q_bef = dbo['n_before'].quantile(q=[0.05, 0.5, 0.95])
q_aft = dbo['n_after'].quantile(q=[0.05, 0.5, 0.95])

yerr = [[q_bef.loc[0.5] - q_bef.loc[0.05], q_aft.loc[0.5] - q_aft.loc[0.05]],
        [q_bef.loc[0.95] - q_bef.loc[0.5], q_aft.loc[0.95] - q_aft.loc[0.5]]]
yerr = np.array(yerr)


f, ax = plt.subplots(1, 1, figsize=[3, 3])

ax.bar(x=[0, 1], height=[q_bef.loc[0.5],q_aft.loc[0.5]], yerr=yerr)
ax.set_xticks([0, 1])
ax.set_xticklabels(['-50 to 0 ms', '0 to 50 ms'])
ax.set_ylabel('# of cells')
sns.despine()
plt.tight_layout()


plot_name = 'peak_bars.jpg'
f.savefig(os.path.join(plot_folder, plot_name), dpi=400)


# --- OPTION 5 -----------------------------------------------------------------
# look at peak times per unit with bootstrap - PERCENTAGE
min_time = -50
max_time = 50
use_min_max_time = True

if use_min_max_time:
    pass
else:
    min_time = -150
    max_time = 150

n_boots = 500
ac_units = df['AC_id'].unique()

dbo = pd.DataFrame(columns=['boot', 'perc_before', 'perc_after', 'n_tot'])

for n in range(n_boots):
    sel_ids = np.random.choice(ac_units, size=len(ac_units), replace=True)

    du = pd.DataFrame(columns=['AC_id', 'peak_time', 'before'])

    for acid in sel_ids:
        idx = np.where(df['AC_id'] == acid)[0]
        peak_time = t[xcarr[idx, :].mean(axis=0).argmax()]
        before = peak_time < 0
        du.loc[du.shape[0], :] = [acid, peak_time, before]

    du = du[(du['peak_time'] > min_time) & (du['peak_time'] < max_time)]
    n_before = du.before.value_counts().loc[True]
    n_after = du.before.value_counts().loc[False]
    n_tot = n_before + n_after
    perc_before = 100 * n_before / n_tot
    perc_after = 100 * n_after / n_tot

    dbo.loc[dbo.shape[0], :] = [n, perc_before, perc_after, n_tot]

q_bef = dbo['perc_before'].quantile(q=[0.05, 0.5, 0.95])
q_aft = dbo['perc_after'].quantile(q=[0.05, 0.5, 0.95])



yerr = [[q_bef.loc[0.5] - q_bef.loc[0.05], q_aft.loc[0.5] - q_aft.loc[0.05]],
        [q_bef.loc[0.95] - q_bef.loc[0.5], q_aft.loc[0.95] - q_aft.loc[0.5]]]
yerr = np.array(yerr)


f, ax = plt.subplots(1, 1, figsize=[3, 3])

ax.bar(x=[0, 1], height=[q_bef.loc[0.5],q_aft.loc[0.5]], yerr=yerr)
ax.set_xticks([0, 1])
ax.set_xticklabels(['-50 to 0 ms', '0 to 50 ms'])
ax.set_ylabel('% of cells')
sns.despine()
plt.tight_layout()

plot_name = 'peak_bars_percentage.jpg'
f.savefig(os.path.join(plot_folder, plot_name), dpi=400)







# --- OPTION 6 -----------------------------------------------------------------
# look at peak times per pair
min_time = -50
max_time = 50
use_min_max_time = True

if use_min_max_time:
    pass
else:
    min_time = -150
    max_time = 150


du = pd.DataFrame(columns=['AC_id', 'V2L_id', 'peak_time', 'before'])
for idx in np.arange(xcarr.shape[0]):

    acid = df['AC_id'].iloc[idx]
    v2id = df['V2L_id'].iloc[idx]
    peak_time = t[xcarr[idx, :].argmax()]
    before = peak_time < 0

    du.loc[du.shape[0], :] = [acid, v2id, peak_time, before]

du = du[(du['peak_time'] > -50) & (du['peak_time'] < 50)]

du.before.value_counts()


f, ax = plt.subplots(1, 1, figsize=[3, 3])
sns.distplot(du['peak_time'], hist=True, kde=False, ax=ax,
             bins=np.arange(-50, 50, 10), norm_hist=True)
ax.axvline(0, c='k', ls='--')
sns.despine()
plt.tight_layout()




