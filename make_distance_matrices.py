import os
import numpy as np
import elephant
import neo
import pickle
from constants import *
from utils import *
import quantities as pq
from prepare_decoding_data import PrepareDecodingData
from loadmat import loadmat


bin_distances = {i : {} for i in ALL_SESSIONS}

output_file = os.path.join(DATA_FOLDER, 'bin_distance.pkl')

maze_distance = loadmat(MAZE_DISTANCE_PATH)
for sess_ind in ALL_SESSIONS:

    # get maze distance
    file_name = 'navig8_session_{}.pkl'.format(sess_ind)
    bl = pickle.load(open(os.path.join(DATA_FOLDER, file_name), 'rb'))
    segment = bl.segments[0]
    lin_speed = get_analogsignal_by_name(segment, 'lin_speed')
    lin_bin = get_analogsignal_by_name(segment, 'lin_bin')
    xy_pos = get_analogsignal_by_name(segment, 'xy_pos')
    lin_bin_arr = lin_bin.__array__()[:, 0]
    xy_pos_arr = xy_pos.__array__()
    lin_speed_arr = lin_speed.__array__()[:, 0]

    maze_distance_sess = maze_distance['Distances'][:, :, sess_ind]  * pixels_to_meters * pq.m

    # get euclidean distance
    pdd = PrepareDecodingData(spike_trains=segment.spiketrains,
                              position=xy_pos_arr,
                              linearized_position=lin_bin_arr,
                              speed=lin_speed_arr,
                              times=lin_bin.times.rescale(pq.ms))

    pdd._build_distance_matrix()
    euclidean_distance_sess = pdd.distance_df

    bin_distances[sess_ind]['maze'] = maze_distance_sess
    bin_distances[sess_ind]['euclidean'] = euclidean_distance_sess
    bin_distances[sess_ind]['bins']  = pdd.bin_df


print('Saving bin distances to {}'.format(output_file))
pickle.dump(bin_distances, open(output_file, 'wb'))