import numpy as np
import elephant
import quantities as pq
from utils import *
from collections import Counter
import pandas as pd
import math

# TODO: when filtering you forget to also select the bin centers

class PrepareDecodingData():

    def __init__(self, spike_trains, position, linearized_position, speed,
                 times, head_direction_sincos=None, head_direction_sincos_change=None):

        self.spike_trains = spike_trains
        self.position = position
        self.linearized_position = linearized_position
        self.speed = np.abs(speed)
        self.head_direction_sincos = head_direction_sincos
        self.head_direction_sincos_change = head_direction_sincos_change
        self.times = times
        self.t_start = times[0]
        self.t_stop = times[-1]

        if head_direction_sincos is not None:
            hdir = [math.atan2(y, x) for y, x in zip(head_direction_sincos[:, 0],
                                                     head_direction_sincos[:, 1])]
            self.head_direction = np.array(hdir)
            self.head_direction_change = np.abs(np.gradient(self.head_direction))


    def bin_spikes(self, binsize_in_ms=100):
        bs = elephant.conversion.BinnedSpikeTrain(self.spike_trains,
                                                  binsize=binsize_in_ms * pq.ms,
                                                  t_start=self.t_start,
                                                  t_stop=self.t_stop)
        self.binned_spikes = bs.to_array().T
        self.bin_centers = bs.bin_centers
        self.bin_edges = bs.bin_edges
        self.bin_tuples = list(zip(bs.bin_edges[0:-1], bs.bin_edges[1:]))


    def interpolate_position_and_speed(self):
        interp_lin_pos = np.zeros(self.bin_centers.shape[0])
        for i, (bin_start, bin_stop) in enumerate(self.bin_tuples):

            ind = np.where((self.times >= bin_start) & (self.times < bin_stop))
            position_bin = self.linearized_position[ind]
            values, counts = np.unique(position_bin, return_counts=True)
            try:
                ind = np.argmax(counts)
                val = values[ind]
            except ValueError:
                val = np.nan
            interp_lin_pos[i] = val

        interp_pos = interpolate_outputs(self.position, self.times, self.bin_centers)
        interp_speed = interpolate_outputs(self.speed[:, np.newaxis],
                                           self.times, self.bin_centers)[:, 0]
        self.interp_position = interp_pos
        # this doesn't work because nans also get cast to int
        # self.interp_linearized_position = interp_lin_pos.astype(int)
        self.interp_linearized_position = interp_lin_pos
        self.interp_speed = interp_speed

        if self.head_direction_sincos is not None:
            interp_head_direction_sincos = interpolate_outputs(self.head_direction_sincos,
                                                        self.times, self.bin_centers)
            self.interp_head_direction_sincos = interp_head_direction_sincos

            interp_head_direction = interpolate_outputs(self.head_direction[:, np.newaxis],
                                                        self.times, self.bin_centers)[:, 0]
            self.interp_head_direction = interp_head_direction

            interp_head_direction_change = interpolate_outputs(self.head_direction_change[:, np.newaxis],
                                                        self.times, self.bin_centers)[:, 0]
            self.interp_head_direction_change = interp_head_direction_change

        else:
            self.interp_head_direction_sincos = None
            self.interp_head_direction = None
            self.interp_head_direction_change = None

        if self.head_direction_sincos_change is not None:
            interp_head_direction_sincos_change = interpolate_outputs(self.head_direction_sincos_change,
                                                               self.times, self.bin_centers)
            self.interp_head_direction_sincos_change = interp_head_direction_sincos_change
        else:
            self.interp_head_direction_sincos_change = None



    def coarsify_bins(self, new_bin_size=5):
        coarse_bins = np.arange(np.nanmin(self.linearized_position),
                                np.nanmax(self.linearized_position) + new_bin_size,
                                new_bin_size)
        self.linearized_position = pd.cut(self.linearized_position,
                                          coarse_bins, labels=False, right=False,
                                          include_lowest=True)
        self.coarse_bins = coarse_bins


    def _build_distance_matrix(self):
        ind = ~np.isnan(self.linearized_position)
        bins = np.unique(self.linearized_position[ind])
        xy_bin = np.zeros([bins.shape[0], 2])

        for i, bin in enumerate(bins):
            xy_bin[i, :] = self.position[self.linearized_position == bin].mean(axis=0)
        bin_df = pd.DataFrame(columns=['bin', 'x', 'y'],
                              data=np.hstack((bins[:, np.newaxis], xy_bin)))
        bin_df['bin'] = bin_df['bin'].astype(int)
        bin_df = bin_df.set_index('bin')

        dist_df = pd.DataFrame()
        for i, bin1 in enumerate(bins):
            xy_bin1 = bin_df.loc[bin1].__array__()
            for j, bin2 in enumerate(bins):
                xy_bin2 = bin_df.loc[bin2].__array__()
                dist_df.loc[int(bin1), int(bin2)] = np.linalg.norm(xy_bin1 - xy_bin2)

        self.bin_df = bin_df
        self.distance_df = dist_df


    def get_error_distance(self, true_bin, pred_bin):
        return self.distance_df.loc[true_bin, pred_bin]


    def get_error_2d(self, true_bin, pred_bin):
        return self.bin_df.loc[true_bin] - self.bin_df.loc[pred_bin]


    def _bin_speed(self):
        pass


    def _apply_filter(self, ind):
        self.binned_spikes = self.binned_spikes[ind, :]
        self.interp_position = self.interp_position[ind]
        self.interp_linearized_position = self.interp_linearized_position[ind]
        self.interp_speed = self.interp_speed[ind]
        if self.interp_head_direction_sincos is not None:
            self.interp_head_direction_sincos = self.interp_head_direction_sincos[ind, :]
        if self.interp_head_direction_sincos_change is not None:
            self.interp_head_direction_sincos_change = self.interp_head_direction_sincos_change[ind, :]
        if self.interp_head_direction is not None:
            self.interp_head_direction = self.interp_head_direction[ind]
        if self.interp_head_direction_change is not None:
            self.interp_head_direction_change = self.interp_head_direction_change[ind]
        self.bin_centers = self.bin_centers[ind]

    def filter_nans(self):
        ind1 = np.logical_and(~np.isnan(self.interp_linearized_position),
                             ~np.isnan(self.interp_speed))
        ind2 = np.logical_and(~np.isnan(self.interp_position[:, 0]),
                             ~np.isnan(self.interp_position[:, 0]))
        ind = np.logical_and(ind1, ind2)

        if self.interp_head_direction_sincos is not None:
            ind3 = ~np.isnan(self.interp_head_direction_sincos.sum(axis=1))
            ind = np.logical_and(ind, ind3)
        if self.interp_head_direction_sincos_change is not None:
            ind4 = ~np.isnan(self.interp_head_direction_sincos_change.sum(axis=1))
            ind = np.logical_and(ind, ind4)
        if self.interp_head_direction is not None:
            ind5 = ~np.isnan(self.interp_head_direction)
            ind = np.logical_and(ind, ind5)
        if self.interp_head_direction_sincos is not None:
            ind6 = ~np.isnan(self.interp_head_direction_change)
            ind = np.logical_and(ind, ind6)

        self._apply_filter(ind)


    def filter_event_times(self, ind):
        self._apply_filter(ind)


    def filter_min_spikes(self, min_spikes=5):
        ind = self.binned_spikes.sum(axis=1) >= min_spikes
        self._apply_filter(ind)


    def filter_min_speed(self, min_speed=0.06):
        ind = np.abs(self.interp_speed) >= min_speed
        self._apply_filter(ind)


    def get_decoding_data(self):
        self._run_checks()

        return self.binned_spikes, self.interp_linearized_position
