import os
import numpy as np
import elephant
import neo
import pickle
from constants import *
from utils import *
import quantities as pq
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from prepare_decoding_data import PrepareDecodingData
from MLencoding import MLencoding
import pandas as pd
from mpl_toolkits.mplot3d import Axes3D
from plotting_style import area_colormap
import pickle
from info_utils import compute_info
import math

# encoding parameters

settings_name = 'final_nostim_shuffled'
sessions = ALL_SESSIONS
min_speed = 0.1
coarsify_bins = True
coarse_bin_size=5
binsize_in_ms = 300
n_condvar_bins = 20
n_surr = 500
condvar_bins_match_pos = True
equipopulated = True
exclude_stimulus_times = True
exclude_stimulus_times_shuffled = True

pars = {'setting_name'  : settings_name,
        'sessions' : sessions,
        'min_speed' : min_speed,
        'binsize_in_ms' : binsize_in_ms,
        'coarsify_bins' : coarsify_bins,
        'coarsify_bin_size' : coarse_bin_size,
        'n_condvar_bins' : n_condvar_bins,
        'condvar_bins_match_pos' : condvar_bins_match_pos,
        'n_surr' : n_surr,
        'equipopulated' : equipopulated}


output_file_name   = 'info_{}.pkl'.format(settings_name)
output_folder      = os.path.join(DATA_FOLDER, 'results', 'info')
output_full_path   = os.path.join(output_folder, output_file_name)


if not os.path.isdir(output_folder):
    os.makedirs(output_folder)

# --- COMPUTE -----------------------------------------------------------------

df_full = pd.DataFrame(columns=['session', 'area', 'unit_id', 'unit_ind_sess',
                                'unit_ind_area', 'targetvar', 'condvar',
                                'cmi', 'cmi_deb', 'cmi_p_val'])
for sess_ind in sessions:

    file_name = 'navig8_session_{}.pkl'.format(sess_ind)
    bl = pickle.load(open(os.path.join(DATA_FOLDER, file_name), 'rb'))
    segment = bl.segments[0]
    A1_ind = [i for i, t in enumerate(segment.spiketrains)
              if t.annotations['area'] == 'A1']
    V2L_ind = [i for i, t in enumerate(segment.spiketrains)
               if t.annotations['area'] == 'V2L']
    xy_speed = get_analogsignal_by_name(segment, 'xy_speed')
    lin_speed = get_analogsignal_by_name(segment, 'lin_speed')
    lin_bin = get_analogsignal_by_name(segment, 'lin_bin')
    xy_pos = get_analogsignal_by_name(segment, 'xy_pos')
    hd_sincos = get_analogsignal_by_name(segment, 'hd_sincos')
    hd_change = get_analogsignal_by_name(segment, 'hd_change')

    lin_bin_arr = lin_bin.__array__()[:, 0]
    xy_pos_arr = xy_pos.__array__()
    lin_speed_arr = lin_speed.__array__()[:, 0]
    xy_speed_arr = xy_speed.__array__()[:, 0]
    hd_sincos_arr = hd_sincos.__array__()
    hd_change_arr = hd_change.__array__()

    # TODO: use linear speed or 2d speed??
    pdd = PrepareDecodingData(spike_trains=segment.spiketrains,
                              position=xy_pos_arr,
                              linearized_position=lin_bin_arr,
                              speed=xy_speed_arr,
                              head_direction_sincos=hd_sincos_arr,
                              head_direction_sincos_change=hd_change_arr,
                              times=lin_bin.times.rescale(pq.ms))

    if coarsify_bins:
        pdd.coarsify_bins(new_bin_size=coarse_bin_size)

    pdd._build_distance_matrix()
    pdd.bin_spikes(binsize_in_ms=binsize_in_ms)
    pdd.interpolate_position_and_speed()

    bin_centers = pdd.bin_centers
    print(sess_ind)
    print(pdd.binned_spikes.shape)

    if exclude_stimulus_times:
        # --- GET STIMULUS ON INTERVALS ---
        ev_lab = segment.events[0].labels
        mask = np.isin(ev_lab, ['Left Visual ON', 'Left Visual OFF',
                                'Right Visual ON', 'Right Visual OFF'])
        sel_ev_lab = ev_lab[mask]
        sel_ev_times = segment.events[0].times[mask]

        on_mask = np.isin(ev_lab, ['Left Visual ON', 'Right Visual ON'])
        off_mask = np.isin(ev_lab, ['Left Visual OFF', 'Right Visual OFF'])
        on_times = segment.events[0].times[on_mask].rescale(pq.ms)
        off_times = segment.events[0].times[off_mask].rescale(pq.ms)
        on_times = on_times - 1000 * pq.ms
        off_times = off_times + 1000 * pq.ms
        intervals = list(zip(on_times, off_times))

        stim_on_mask = np.ones(bin_centers.shape[0])
        for t1, t2 in intervals:
            in_interval = np.logical_and(bin_centers >= t1, bin_centers < t2)
            stim_on_mask[in_interval] = 0
        stim_on_mask = stim_on_mask.astype(bool)
        print('Keeping only {:.2} of the data'.format(stim_on_mask.sum()/stim_on_mask.shape[0]))
        # This where we actually filter
        if exclude_stimulus_times_shuffled:
            stim_on_mask = np.random.permutation(stim_on_mask)
            print('Keeping only {:.2} of the data'.format(stim_on_mask.sum()/stim_on_mask.shape[0]))
        pdd.filter_event_times(stim_on_mask)
        print(pdd.binned_spikes.shape)
        # ---------------------------------


    pdd.filter_nans()
    pdd.filter_min_speed(min_speed)

    binned_spikes = pdd.binned_spikes
    X_linpos = pdd.interp_linearized_position
    X_speed = pdd.interp_speed

    if not np.all(X_speed > 0):
        raise ValueError

    X_hd_angle = pdd.interp_head_direction
    X_hd_angle_change = pdd.interp_head_direction_change

    if condvar_bins_match_pos:
        n_condvar_bins = np.unique(X_linpos).shape[0]
        print('Automatically setting the number of bins '
              'for the variables to {}'.format(n_condvar_bins))

    if equipopulated:
        X_speed = pd.qcut(X_speed.flatten(), q=n_condvar_bins, labels=False).astype(int)
        X_hd_angle = pd.qcut(X_hd_angle.flatten(), q=n_condvar_bins, labels=False).astype(int)
        X_hd_angle_change = pd.qcut(X_hd_angle_change.flatten(), q=n_condvar_bins, labels=False).astype(int)

    else:
        X_speed = pd.cut(X_speed.flatten(), bins=n_condvar_bins, labels=False).astype(int)
        X_hd_angle = pd.cut(X_hd_angle.flatten(), bins=n_condvar_bins, labels=False).astype(int)
        X_hd_angle_change = pd.cut(X_hd_angle_change.flatten(), bins=n_condvar_bins, labels=False).astype(int)

    # TODO important - 1
    if not coarsify_bins:
        X_linpos = X_linpos.astype(int) - 1


    variables = {'speed'       : X_speed,
                 'hdir'        : X_hd_angle,
                 'hdir_change' : X_hd_angle_change,
                 'position'    : X_linpos}

    for area, area_unit_ind in zip(['A1', 'V2L'], [A1_ind, V2L_ind]):

        for kk, unit_ind in enumerate(area_unit_ind):

            unit_id = '{:02}_{}_{:02}'.format(sess_ind, area, unit_ind)
            y = binned_spikes[:, unit_ind].reshape(-1, 1).astype(int)
            print('\n\n>>> Analysing unit {}\n'.format(unit_id))

            base1 = int(y.max() + 1)

            for target_var in ['speed', 'hdir', 'hdir_change', 'position']:

                if target_var == 'position':
                    base2 = int(X_linpos.max() + 1)
                else:
                    base2 = n_condvar_bins

                mi, mi_sm, mi_p_val = compute_info(y, variables[target_var],
                                                      estimator='discrete',
                                                      base1=base1,
                                                      base2=base2,
                                                      n_surr=n_surr)

                df_full.loc[df_full.shape[0]] = [sess_ind, area, unit_id,
                                                 unit_ind, kk, target_var, 'none',
                                                 mi, mi - mi_sm, mi_p_val]

            for cond_var in ['speed', 'hdir', 'hdir_change']:

                base2 = int(X_linpos.max() + 1)

                cmi, cmi_sm, cmi_p_val = compute_info(y, X_linpos,
                                                      estimator='discrete',
                                                      conditional=True,
                                                      condvar=variables[cond_var],
                                                      base1=base1,
                                                      base2=base2,
                                                      condbase=n_condvar_bins,
                                                      n_surr=n_surr)

                df_full.loc[df_full.shape[0]] = [sess_ind, area, unit_id,
                                                 unit_ind, kk, 'position', cond_var,
                                                 cmi, cmi - cmi_sm, cmi_p_val]



            for target_var in ['speed', 'hdir', 'hdir_change']:
                cond_var = 'position'
                base2 = n_condvar_bins
                n_condvar_bins = int(X_linpos.max() + 1)

                cmi, cmi_sm, cmi_p_val = compute_info(y, variables[target_var],
                                                      estimator='discrete',
                                                      conditional=True,
                                                      condvar=variables[cond_var],
                                                      base1=base1,
                                                      base2=base2,
                                                      condbase=n_condvar_bins,
                                                      n_surr=n_surr)

                df_full.loc[df_full.shape[0]] = [sess_ind, area, unit_id,
                                                 unit_ind, kk, target_var, cond_var,
                                                 cmi, cmi - cmi_sm, cmi_p_val]


print('Saving information results to {}'.format(output_full_path))

output = {'df' : df_full,
          'pars' : pars}

pickle.dump(output, open(output_full_path, 'wb'))



