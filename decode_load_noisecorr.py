import os
import pickle
from constants import *
from utils import *
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
import matplotlib.gridspec as gridspec
from sklearn.preprocessing import minmax_scale
from plotting_style import *
from scipy.ndimage.filters import gaussian_filter, gaussian_filter1d
import pandas as pd
from scipy.stats.stats import pearsonr, ttest_rel
from scipy.stats import mannwhitneyu


ms = 50
decoder = 'dkw'
setting_names = ['aug23_knc{}'.format(ms), 'aug23_dnc{}'.format(ms)]

plot_folder = os.path.join(DATA_FOLDER, 'plots', 'decode',
                           '{}_{}_{}'.format(setting_names[0], setting_names[1], decoder))

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

use_maze_distance=False

if use_maze_distance:
    error_string = 'maze_error'
else:
    error_string = 'error'


dfs = []

for settings_name in setting_names:
    print(settings_name)
    #settings_name = 'keepnoisecorr'
    decoder       = 'dkw'

    # plotting parameters
    plot_error_correlation_per_location=False
    smooth_confusion_matrices=False
    smooth_error_by_bin=True
    sigma_smooth_cm = 1
    sigma_smooth_err_by_bin=2
    running_mean_prediction_plot=False
    gaussian_filter_prediction_plot=True
    n_seconds_pred_plot = 60
    save_plots = True
    plot_format = 'svg'
    dpi = 400

    bin_distance_file = os.path.join(DATA_FOLDER, 'bin_distance.pkl')
    bin_distance = pickle.load(open(bin_distance_file, 'rb'))

    results_file_name = 'decode_{}_{}.pkl'.format(settings_name, decoder)
    results_path = os.path.join(DATA_FOLDER, 'results', 'dec', results_file_name)
    results = pickle.load(open(results_path, 'rb'))

    df_full = results['decoding_results']
    df_full['noisecorr'] = settings_name
    pars    = results['decoding_pars']
    dfs.append(df_full)

df = pd.concat(dfs)


# f, ax = plt.subplots(1, 2, figsize=[small_square_side, small_square_side])
#
# for i, area in enumerate(AREAS):
#     dfx = df[np.isin(df['session'], GOOD_SESSIONS_BY_AREA[area])]
#     dfx.groupby(['session', 'bin']).mean().reset_index()
#     sns.barplot(x='noisecorr', y='{}_error'.format(area), data=dfx, ax=ax[i])
# plt.tight_layout()

#bw=0.02
f, ax = plt.subplots(1, 1, figsize=[small_square_side, small_square_side])

for i, area in enumerate(AREAS):
    dfx = df[np.isin(df['session'], GOOD_SESSIONS_BY_AREA[area])]
    dfx.groupby(['session', 'bin']).mean().reset_index()
    x1 = dfx.loc[dfx['noisecorr'] ==setting_names[0], '{}_error'.format(area)]
    sns.kdeplot(x1, ax=ax, c=area_colormap[area])
    x2 = dfx.loc[dfx['noisecorr']==setting_names[1], '{}_error'.format(area)]
    sns.kdeplot(x2, ax=ax, c='grey')
ax.set_xlim([0, 0.5])
ax.set_ylim([0.5, ax.get_ylim()[1]])
#ax.set_yticks([])
ax.get_legend().remove()
sns.despine(top=True, right=True, left=False)
ax.set_ylabel('Prob. density')
ax.set_xlabel('Decoding error (m)')
plt.tight_layout()

if save_plots:
    plot_name = 'noise_corr_error_kde_{}ms_{}.{}'.format(settings_name[-2:], decoder,
                                                     plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


# --- STATISTICS --------------------------------------------------------------



for i, area in enumerate(AREAS):
    x1 = dfx.loc[dfx['noisecorr'] ==setting_names[0], '{}_error'.format(area)]
    x2 = dfx.loc[dfx['noisecorr']==setting_names[1], '{}_error'.format(area)]

    print('Area {}: median error original: {:.6}'.format(area, x1.median()))
    print('Area {}: median error without noise corr: {:.6}'.format(area, x2.median()))

    stat, p = mannwhitneyu(x1, x2)
    print('Mann-Whitney U - {} : {:.4e}'.format(area, p))


# dfxg = dfx.groupby(['session', 'noisecorr']).median().reset_index()
#
# n_sess = dfxg.shape[0]/2
# x1 = dfxg.loc[dfxg['noisecorr'] == setting_names[0], 'A1_error']
# x2 = dfxg.loc[dfxg['noisecorr'] == setting_names[1], 'A1_error']
# f, ax = plt.subplots(1, 1)
# ax.scatter(np.repeat(0, n_sess), x1)
# ax.scatter(np.repeat(1, n_sess), x2)
# for p1, p2 in zip(x1, x2):
#     ax.plot([0, 1], [p1, p2])

# sns.kdeplot(df['{}_{}'.format(area, error_string)], shade=True, c=colormap[settings_name],
#             ax=ax)
#
# ax.set_xlim([0, ax.get_xlim()[1]])
# ax.set_ylabel('Prob. density')
# ax.set_xlabel('Decoding error (m)')
# ax.get_legend().remove()
# ax.set_yticks([])
# sns.despine(top=True, right=True, left=True)
# plt.tight_layout()