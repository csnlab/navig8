import os
import numpy as np
import elephant
import neo
import pickle
from constants import *
from utils import *
import quantities as pq
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from prepare_decoding_data import PrepareDecodingData
from MLencoding import MLencoding
import pandas as pd
from mpl_toolkits.mplot3d import Axes3D
from plotting_style import area_colormap
import pickle

# encoding parameters

settings_name = 'final_nostim_shuffled'
sessions = ALL_SESSIONS
min_speed = 0.1
binsize_in_ms = 400
encoding_model = 'random_forest'
n_cv = 5
encoder_params = {'n_estimators' : 100}
exclude_stimulus_times = True
exclude_stimulus_times_shuffled = True

pars = {'setting_name'  : settings_name,
        'sessions' : sessions,
        'min_speed' : min_speed,
        'binsize_in_ms' : binsize_in_ms,
        'encoding_model' : encoding_model,
        'n_cv' : n_cv,
        'encoder_params' : encoder_params}


output_file_name_r2   = 'encode_{}_{}.pkl'.format(settings_name, encoding_model)
output_file_name_pred = 'encode_{}_{}_predictions.pkl'.format(settings_name, encoding_model)

output_folder         = os.path.join(DATA_FOLDER, 'results', 'encode')
output_full_path_r2   = os.path.join(output_folder, output_file_name_r2)
output_full_path_pred = os.path.join(output_folder, output_file_name_pred)


if not os.path.isdir(output_folder):
    os.makedirs(output_folder)


# --- prepare ---

df_full = pd.DataFrame(columns=['session', 'area', 'unit_id', 'unit_ind_sess', 'unit_ind_area',
                                'predictor', 'r2'])

rs = {s : {'A1' : {}, 'V2L' : {}} for s in sessions}

encode_predictors = ['position',
                     'speed',
                     'hdir',
                     'hdir_change',
                     'position+speed',
                     'position+hdir',
                     'speed+hdir+hdir_change',
                     'position+hdir_change',
                     'position+hdir+hdir_change',
                     'position+speed+hdir',
                     'position+speed+hdir_change',
                     'position+speed+hdir+hdir_change']


# --- ENCODE ------------------------------------------------------------------

for sess_ind in sessions:

    file_name = 'navig8_session_{}.pkl'.format(sess_ind)
    bl = pickle.load(open(os.path.join(DATA_FOLDER, file_name), 'rb'))
    segment = bl.segments[0]
    A1_ind = [i for i, t in enumerate(segment.spiketrains)
              if t.annotations['area'] == 'A1']
    V2L_ind = [i for i, t in enumerate(segment.spiketrains)
               if t.annotations['area'] == 'V2L']
    xy_speed = get_analogsignal_by_name(segment, 'xy_speed')
    lin_speed = get_analogsignal_by_name(segment, 'lin_speed')
    lin_bin = get_analogsignal_by_name(segment, 'lin_bin')
    xy_pos = get_analogsignal_by_name(segment, 'xy_pos')
    hd_sincos = get_analogsignal_by_name(segment, 'hd_sincos')
    hd_change = get_analogsignal_by_name(segment, 'hd_change')
    times = lin_bin.times.rescale(pq.ms)

    lin_bin_arr = lin_bin.__array__()[:, 0].astype(int)
    xy_pos_arr = xy_pos.__array__()
    lin_speed_arr = lin_speed.__array__()[:, 0]
    xy_speed_arr = xy_speed.__array__()[:, 0]
    hd_sincos_arr = hd_sincos.__array__()
    hd_change_arr = hd_change.__array__()

    pdd = PrepareDecodingData(spike_trains=segment.spiketrains,
                              position=xy_pos_arr,
                              linearized_position=lin_bin_arr,
                              speed=xy_speed_arr,
                              head_direction_sincos=hd_sincos_arr,
                              head_direction_sincos_change=hd_change_arr,
                              times=times)

    pdd._build_distance_matrix()
    pdd.bin_spikes(binsize_in_ms=binsize_in_ms)
    pdd.interpolate_position_and_speed()

    bin_centers = pdd.bin_centers
    print(sess_ind)
    print(pdd.binned_spikes.shape)

    if exclude_stimulus_times:
        # --- GET STIMULUS ON INTERVALS ---
        ev_lab = segment.events[0].labels
        mask = np.isin(ev_lab, ['Left Visual ON', 'Left Visual OFF',
                                'Right Visual ON', 'Right Visual OFF'])
        sel_ev_lab = ev_lab[mask]
        sel_ev_times = segment.events[0].times[mask]

        on_mask = np.isin(ev_lab, ['Left Visual ON', 'Right Visual ON'])
        off_mask = np.isin(ev_lab, ['Left Visual OFF', 'Right Visual OFF'])
        on_times = segment.events[0].times[on_mask].rescale(pq.ms)
        off_times = segment.events[0].times[off_mask].rescale(pq.ms)
        on_times = on_times - 1000 * pq.ms
        off_times = off_times + 1000 * pq.ms
        intervals = list(zip(on_times, off_times))

        stim_on_mask = np.ones(bin_centers.shape[0])
        for t1, t2 in intervals:
            in_interval = np.logical_and(bin_centers >= t1, bin_centers < t2)
            stim_on_mask[in_interval] = 0
        stim_on_mask = stim_on_mask.astype(bool)
        print('Keeping only {:.2} of the data'.format(stim_on_mask.sum()/stim_on_mask.shape[0]))
        # This where we actually filter
        if exclude_stimulus_times_shuffled:
            stim_on_mask = np.random.permutation(stim_on_mask)
            print('Keeping only {:.2} of the data'.format(stim_on_mask.sum()/stim_on_mask.shape[0]))
        pdd.filter_event_times(stim_on_mask)
        print(pdd.binned_spikes.shape)
        # ---------------------------------

    pdd.filter_nans()
    pdd.filter_min_speed(min_speed)

    binned_spikes = pdd.binned_spikes
    X_pos = pdd.interp_position
    X_speed = pdd.interp_speed.reshape(-1, 1)
    X_hd = pdd.interp_head_direction.reshape(-1, 1)
    X_hd_ch = pdd.interp_head_direction_change.reshape(-1, 1)

    if not np.all(X_speed > 0):
        raise ValueError

    predictors = {'position'                       : X_pos,
                  'speed'                          : X_speed,
                  'hdir'                           : X_hd,
                  'hdir_change'                    : X_hd_ch,

                  'position+speed'                 : np.hstack((X_pos, X_speed)),
                  'position+hdir'                  : np.hstack((X_pos, X_hd)),
                  'position+hdir_change'           : np.hstack((X_pos, X_hd_ch)),

                  'speed+hdir+hdir_change'         : np.hstack((X_speed, X_hd, X_hd_ch)), # no position
                  'position+hdir+hdir_change'      : np.hstack((X_pos, X_hd, X_hd_ch)), # no speed
                  'position+speed+hdir'            : np.hstack((X_pos, X_speed, X_hd)), # no hdir_change
                  'position+speed+hdir_change'     : np.hstack((X_pos, X_speed, X_hd_ch)), # no hdir

                  'position+speed+hdir+hdir_change': np.hstack((X_pos, X_speed, X_hd, X_hd_ch))} # ALL

    for area, area_unit_ind in zip(['A1', 'V2L'], [A1_ind, V2L_ind]):


        for key in ['true_spikes'] + encode_predictors:
            rs[sess_ind][area][key] = np.zeros([len(area_unit_ind), X_pos.shape[0]])

        for kk, unit_ind in enumerate(area_unit_ind):
            print(kk, unit_ind)

            unit_id = '{:02}_{}_{:02}'.format(sess_ind, area, unit_ind)
            y = binned_spikes[:, unit_ind]
            rs[sess_ind][area]['true_spikes'][kk, :] = y
            print('\n\n>>> Encoding unit {}\n'.format(unit_id))

            for predictor in encode_predictors:
                X_predictor = predictors[predictor]
                encoder = MLencoding(tunemodel = encoding_model)
                encoder.set_params(encoder_params)
                Y_hat, PR2s = encoder.fit_cv(X_predictor, y, n_cv=n_cv,
                                             verbose=2, continuous_folds=False)
                PR2s = np.mean(PR2s)
                rs[sess_ind][area][predictor][kk, :] = Y_hat

                df_full.loc[df_full.shape[0]] = [sess_ind, area, unit_id,
                                                 unit_ind, kk,
                                                 predictor, PR2s]


print('Saving encoding r2 to {}'.format(output_full_path_r2))
print('Saving encoding predictions to {}'.format(output_full_path_pred))

output_r2 = {'df' : df_full,
             'pars' : pars}
output_pred = {'predictions' : rs,
               'pars' : pars}

pickle.dump(output_pred, open(output_full_path_pred, 'wb'))
pickle.dump(output_r2, open(output_full_path_r2, 'wb'))


