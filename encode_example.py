import os
import numpy as np
import elephant
import neo
import pickle
from constants import *
from utils import *
import quantities as pq
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from prepare_decoding_data import PrepareDecodingData
from MLencoding import MLencoding
import pandas as pd
from mpl_toolkits.mplot3d import Axes3D
from plotting_style import area_colormap
from scipy.ndimage import gaussian_filter1d
import pickle
from plotting_style import *

good_cells = ['02_V2L_34', '06_V2L_26', '05_V2L_49', '06_A1_01', '00_A1_24']

# encoding parameters
sessions = ALL_SESSIONS
min_speed = 0.1
binsize_in_ms = 400
encoding_model = 'random_forest'
n_cv = 5
n_seconds_pred_plot = 60
gaussian_filter_prediction_plot = True
encoder_params = {'n_estimators' : 100}

sess_ind = 6
unit_ind = 1
unit_id  = '{:02}_{:02}'.format(sess_ind, unit_ind)


save_plots = True
plot_format = 'svg'
dpi = 400

plot_folder = os.path.join(DATA_FOLDER, 'plots', 'encode', 'examples')
if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)


# -----------------------------------------------------------------------------

encode_predictors = ['position', 'speed', 'hdir', 'hdir_change',
                     'position+speed+hdir+hdir_change']


file_name = 'navig8_session_{}.pkl'.format(sess_ind)
bl = pickle.load(open(os.path.join(DATA_FOLDER, file_name), 'rb'))
segment = bl.segments[0]
A1_ind = [i for i, t in enumerate(segment.spiketrains)
          if t.annotations['area'] == 'A1']
V2L_ind = [i for i, t in enumerate(segment.spiketrains)
           if t.annotations['area'] == 'V2L']
xy_speed = get_analogsignal_by_name(segment, 'xy_speed')
lin_speed = get_analogsignal_by_name(segment, 'lin_speed')
lin_bin = get_analogsignal_by_name(segment, 'lin_bin')
xy_pos = get_analogsignal_by_name(segment, 'xy_pos')
hd_sincos = get_analogsignal_by_name(segment, 'hd_sincos')
hd_change = get_analogsignal_by_name(segment, 'hd_change')

if np.isin(unit_ind, A1_ind):
    area = 'A1'
elif np.isin(unit_ind, V2L_ind):
    area = 'V2L'


lin_bin_arr = lin_bin.__array__()[:, 0]
xy_pos_arr = xy_pos.__array__()
lin_speed_arr = lin_speed.__array__()[:, 0]
xy_speed_arr = xy_speed.__array__()[:, 0]
hd_sincos_arr = hd_sincos.__array__()
hd_change_arr = hd_change.__array__()

pdd = PrepareDecodingData(spike_trains=segment.spiketrains,
                          position=xy_pos_arr,
                          linearized_position=lin_bin_arr,
                          speed=xy_speed_arr,
                          head_direction_sincos=hd_sincos_arr,
                          head_direction_sincos_change=hd_change_arr,
                          times=lin_bin.times.rescale(pq.ms))

pdd._build_distance_matrix()
pdd.bin_spikes(binsize_in_ms=binsize_in_ms)
pdd.interpolate_position_and_speed()

pdd.filter_nans()
pdd.filter_min_speed(min_speed)

binned_spikes = pdd.binned_spikes
X_pos = pdd.interp_position
X_speed = np.abs(pdd.interp_speed.reshape(-1, 1))
X_hd = pdd.interp_head_direction_sincos
X_hd_ch = pdd.interp_head_direction_sincos_change

predictors = {'position'                       : X_pos,
              'speed'                          : X_speed,
              'hdir'                           : X_hd,
              'hdir_change'                    : X_hd_ch,
              'position+speed'                 : np.hstack((X_pos, X_speed)),
              'position+hdir'                  : np.hstack((X_pos, X_hd)),
              'position+hdir_change'           : np.hstack((X_pos, X_hd_ch)),
              'hdir+hdir_change'               : np.hstack((X_hd, X_hd_ch)),
              'position+hdir+hdir_change'      : np.hstack((X_pos, X_hd, X_hd_ch)), # no speed
              'position+speed+hdir'            : np.hstack((X_pos, X_speed, X_hd)), # no hdir_change
              'position+speed+hdir_change'     : np.hstack((X_pos, X_speed, X_hd_ch)), # no hdir
              'position+speed+hdir+hdir_change': np.hstack((X_pos, X_speed, X_hd, X_hd_ch)), # all
              'speed+hdir+hdir_change'         : np.hstack((X_speed, X_hd, X_hd_ch))}  # no position


# --- ENCODE ------------------------------------------------------------------

y = binned_spikes[:, unit_ind]

predictions = {}
for encode_predictor in encode_predictors:
    X_predictor = predictors[encode_predictor]
    encoder = MLencoding(tunemodel = encoding_model)
    encoder.set_params(encoder_params)
    Y_hat, PR2s = encoder.fit_cv(X_predictor, y, n_cv=n_cv,
                                 verbose=2, continuous_folds=True)
    PR2s = np.mean(PR2s)
    predictions[encode_predictor] = Y_hat


# --- PLOT PREDICTION IN TIME -------------------------------------------------

session_prediction_plot = 6
n_samples = y.shape[0]
n_samp_pred_plot = int(n_seconds_pred_plot * 1000 / binsize_in_ms)
st_s = n_samples // 4

true_spikes  = y[st_s : st_s + n_samp_pred_plot].astype(float)

pred_spikes = {}
for predictor in encode_predictors:

    ps = predictions[predictor][st_s : st_s + n_samp_pred_plot]
    if gaussian_filter_prediction_plot:
        ps = gaussian_filter1d(ps, 2)
    pred_spikes[predictor] = ps

if gaussian_filter_prediction_plot:
    true_spikes = gaussian_filter1d(true_spikes, 2)


true_spikes = true_spikes / (binsize_in_ms / 1000)

pred_time_in_s = np.arange(0, true_spikes.shape[0]) * binsize_in_ms / 1000

f, ax = plt.subplots(figsize=[2*small_square_side,  1*small_square_side])
ax.plot(pred_time_in_s, true_spikes, c='k', label='True data', zorder=10)
for predictor in encode_predictors:
    pred_firing_rate = pred_spikes[predictor]/ (binsize_in_ms / 1000)
    ax.plot(pred_time_in_s, pred_firing_rate,
            c=predictors_colormap[predictor], label=predictors_labels_cap[predictor],
            alpha=0.7)

box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), frameon=False)

ax.set_xlabel('Time (s)')
ax.set_ylabel('Firing rate (Hz)')
sns.despine()
plt.tight_layout(rect=[0,0,0.7,1])



if save_plots:
    plot_name = 'encode_example_{}_{}_{}.{}'.format(unit_id, encoding_model, area,
                                                 plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
