import os
from constants import *
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
import pandas as pd
from plotting_style import *
from scipy.stats import mannwhitneyu, wilcoxon


settings_name = 'final'
save_plots = True
dpi = 400
plot_format = 'svg'

output_file_name   = 'info_{}.pkl'.format(settings_name)
output_folder      = os.path.join(DATA_FOLDER, 'results', 'info')
output_full_path   = os.path.join(output_folder, output_file_name)
results            = pickle.load(open(output_full_path, 'rb'))

pars = results['pars']
df_full = results['df']


plot_folder = os.path.join(DATA_FOLDER, 'plots', 'info', settings_name)

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

#df_full = df_full[df_full['cmi_p_val'] < 0.01]

# --- STATISTICS --------------------------------------------------------------
target_vars = ['speed', 'hdir', 'hdir_change', 'position']

for targ_var in target_vars:
    # TEST MI
    xx = df_full[(df_full['condvar'] == 'none')]
    xx = xx[xx['targetvar']== targ_var]

    x = xx.loc[xx['area'] == 'A1', 'cmi_deb'].as_matrix()
    y = xx.loc[xx['area'] == 'V2L', 'cmi_deb'].as_matrix()

    stat, p = mannwhitneyu(x, y)
    #p = p * len(target_vars)
    print('Mann-Whitney U - {} : {:.4e}'.format(targ_var, p))



print('\n\nDifference from 0 of cmi between spikes and hdir conditional on position')
for area in AREAS:
    xx = df_full[(df_full['condvar'] == 'position')]
    xx = xx[xx['targetvar']== 'hdir']

    x = xx.loc[xx['area'] == area, 'cmi_deb'].as_matrix()
    stat, p = wilcoxon(x)
    print('{} - p={:.4e} (mean: {:.4})'.format(area, p, x.mean()))



for area in AREAS:
    mis = df_full[(df_full['condvar'] == 'none') & (df_full['area'] == area)]
    xx = mis[mis['targetvar']== 'position']
    yy = mis[mis['targetvar']== 'hdir']
    np.testing.assert_array_equal(xx['unit_id'], yy['unit_id'])
    print('\n\nMI of position is larger than MI of head direction for {}'.format(area))
    stat, p = wilcoxon(xx['cmi_deb'], yy['cmi_deb'])
    # p = p * len(target_vars)
    print('Wilcoxon: p={:.4e}'.format(p))



targetvar1 = 'position'
condvar1   = 'none'

targetvar2 = 'position'
condvar2   = 'hdir'

n_sigs = {'A1' : [], 'V2L' : []}

for area in AREAS:
    print('CMI is different from MI for {}'.format(area))
    dfx = df_full.loc[(df_full['area'] == area) &
                     (df_full['targetvar'] == targetvar1) &
                     (df_full['condvar'] == condvar1)]

    dfy = df_full.loc[(df_full['area'] == area) &
                     (df_full['targetvar'] == targetvar2) &
                     (df_full['condvar'] == condvar2)]

    mi_cmi = dfx['cmi_deb'].values - dfy['cmi_deb'].values

    stat, p = wilcoxon(dfx['cmi_deb'], dfy['cmi_deb'])
    print('Wilcoxon: p={:.4e}'.format(p))
    print('units with mi > cmi: {}'.format((mi_cmi >= 0).sum() / mi_cmi.shape[0]))


for i, area in enumerate(AREAS):
    for targ_var in ['speed', 'hdir', 'hdir_change']:
        dfx = df_full.loc[(df_full['area'] == area) &
                         (df_full['targetvar'] == targ_var) &
                         (df_full['condvar'] == 'position')]
        thr = 0.05 / dfx.shape[0]
        n = dfx.shape[0]
        n2 = dfx[dfx['cmi_p_val'] < thr].shape[0]

        print('{} - CMI(spikes, {} | position) - {} out of {} sig ({:.1f}%'
              ')'.format(area, targ_var, n2, n, 100*(n2/n)))


for i, area in enumerate(AREAS):
    for cond_var in ['speed', 'hdir', 'hdir_change']:
        dfx = df_full.loc[(df_full['area'] == area) &
                         (df_full['targetvar'] == 'position') &
                         (df_full['condvar'] == cond_var)]
        thr = 0.05 / dfx.shape[0]
        n = dfx.shape[0]
        n2 = dfx[dfx['cmi_p_val'] < thr].shape[0]

        print('{} - CMI(spikes, position | {}) - {} out of {} sig ({:.1f}%'
              ')'.format(area, cond_var, n2, n, 100*(n2/n)))


# TODO units which have sig about position conditional on all other things
for i, area in enumerate(AREAS):
    dfs = df_full.loc[(df_full['area'] == area) &
                     (df_full['targetvar'] == 'position') &
                     (df_full['condvar'] == 'speed')]

    dfh = df_full.loc[(df_full['area'] == area) &
                     (df_full['targetvar'] == 'position') &
                     (df_full['condvar'] == 'hdir')]

    dfhc = df_full.loc[(df_full['area'] == area) &
                     (df_full['targetvar'] == 'position') &
                     (df_full['condvar'] == 'hdir_change')]

    n = dfs.shape[0]
    thr = 0.05 / n
    dfs = dfs[dfs['cmi_p_val'] < thr]
    dfh = dfh[dfh['cmi_p_val'] < thr]
    dfhc = dfhc[dfhc['cmi_p_val'] < thr]

    comb = (set(dfs['unit_id']).intersection(dfh['unit_id'])).intersection(dfhc['unit_id'])
    n2 = comb.__len__()
    print('{} - Sig. CMI about pos conditional on all predictors - {} out of {} sig ({:.1f}%'
          ')'.format(area, n2, n, 100 * (n2 / n)))

# --- VENN DIAGRAM ------------------------------------------------------------

colors = [sns.xkcd_rgb['medium grey'], sns.xkcd_rgb['light grey'], sns.xkcd_rgb['purple grey']]
f, ax = plt.subplots(1, 2, figsize=[5, 2])
for i, area in enumerate(AREAS):
    dfx = df_full.loc[(df_full['area'] == area) &
                     (df_full['targetvar'] == 'position') &
                     (df_full['condvar'] == 'hdir')]

    dfy = df_full.loc[(df_full['area'] == area) &
                     (df_full['targetvar'] == 'hdir') &
                     (df_full['condvar'] == 'position')]
    thr = 0.05 / dfx.shape[0]
    dfx = dfx[dfx['cmi_p_val'] < thr]
    dfy = dfy[dfy['cmi_p_val'] < thr]

    sig_pos = set(dfx['unit_id']).__len__()
    sig_hdir = set(dfy['unit_id']).__len__()
    sigboth = set(dfx['unit_id']).intersection(set(dfy['unit_id'])).__len__()

    import matplotlib_venn
    matplotlib_venn.venn2([set(dfx['unit_id']), set(dfy['unit_id'])],
                          set_labels=['', ''], ax=ax[i],
                          set_colors=colors)
ax[0].set_title(AREAS[0])
ax[1].set_title(AREAS[1])



if save_plots:
    plot_name = 'info_venn_diagram_pos_hdir_{}.{}'.format(settings_name,
                                                           plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



# --- DISTPLOT ----------------------------------------------------------------
from plotting_style import *
log = True

target_vars = ['speed', 'hdir', 'hdir_change', 'position']

f, axes = plt.subplots(2, 2, figsize=[2*small_square_side, 2*small_square_side],
                       sharex=True, sharey=False)

for targetvar, ax in zip(target_vars, axes.flatten()):
    df = df_full[(df_full['targetvar'] == targetvar) &
                 (df_full['condvar'] == 'none')]
    df = df[df['cmi_p_val'] < 0.001]

    if log:
        df['cmi_deb'] = np.log10(df['cmi_deb'])
    bins = np.arange(df['cmi_deb'].min(), df['cmi_deb'].max(), 0.05)

    for area in AREAS:
        dfx = df[df['area'] == area]
        #dfx = dfx[dfx['cmi_p_val'] < 0.01]
        g = sns.distplot(dfx['cmi_deb'], ax=ax, hist=True,
                        color=area_colormap[area], bins=bins, norm_hist=True)
    ax.set_xlim([df['cmi_deb'].min(), ax.get_xlim()[1]])
    sns.despine()
    ax.set_ylabel('Density')
    ax.set_xlabel('MI(spikes, {})'.format(predictors_labels[targetvar]))
    plt.tight_layout()

if save_plots:
    plot_name = 'info_distplot_targets_nocond_{}.{}'.format(settings_name,
                                                           plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


# --- DISTPLOT CONDITIONAL ----------------------------------------------------
from plotting_style import *
log = True
cond_vars = ['none', 'speed', 'hdir', 'hdir_change']

f, axes = plt.subplots(2, 2, figsize=[2*small_square_side, 2*small_square_side],
                       sharex=True, sharey=True)

for condvar, ax in zip(cond_vars, axes.flatten()):
    df = df_full[(df_full['targetvar'] == 'position') &
                 (df_full['condvar'] == condvar)]
    df = df[df['cmi_p_val'] < 0.001]

    if log:
        df['cmi_deb'] = np.log10(df['cmi_deb'])
    bins = np.arange(df['cmi_deb'].min(), df['cmi_deb'].max(), 0.05)

    for area in AREAS:
        dfx = df[df['area'] == area]
        #dfx = dfx[dfx['cmi_p_val'] < 0.01]
        g = sns.distplot(dfx['cmi_deb'], ax=ax, hist=True,
                        color=area_colormap[area], bins=bins, norm_hist=True)
    ax.set_xlim([df['cmi_deb'].min(), ax.get_xlim()[1]])
    sns.despine()
    ax.set_ylabel('Density')
    ax.set_title(condvar)
    plt.tight_layout()





# --- BARPLOT TARGET VARIABLES ------------------------------------------------

target_vars = ['speed',
               'hdir',
               'hdir_change',
               'position']

f, ax = plt.subplots(1, 1, figsize=[small_square_side, 1*small_square_side],
                     sharey=True)

xx = df_full[(df_full['condvar'] == 'none')]
xx = xx[np.isin(xx['targetvar'], target_vars)]

g = sns.barplot(ax=ax, data=xx, x='targetvar', y='cmi_deb',
                hue='area', hue_order=['A1', 'V2L'],
                order=target_vars,
                palette=[area_colormap['A1'], area_colormap['V2L']])
sns.despine()
    #ax[ii].set_ylim([-0.5, ax[ii].get_ylim()[1]])
ax.set_ylabel('MI (spikes, $\\bf{\cdot}$) [bits]')
ax.get_legend().remove()
ax.set_xticklabels([predictors_labels_cap[t] for t in target_vars],
                       rotation=45, ha="center")
ax.set_xlabel('')
plt.tight_layout()

if save_plots:
    plot_name = 'info_barplot_targets_nocond_{}.{}'.format(settings_name,
                                                           plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


# --- BOXPLOT CONDITIONAL -----------------------------------------------------

cond_vars = ['speed',
             'hdir',
             'hdir_change']

f, ax = plt.subplots(1, 1, figsize=[small_square_side, 1*small_square_side],
                     sharey=True)

xx = df_full[(df_full['targetvar'] == 'position')]
xx = xx[np.isin(xx['condvar'], cond_vars)]

g = sns.barplot(ax=ax, data=xx, x='condvar', y='cmi_deb',
                order=cond_vars, hue='area', hue_order=['A1', 'V2L'],
                palette=[area_colormap['A1'], area_colormap['V2L']])
sns.despine()
#ax[ii].set_ylim([-0.5, ax[ii].get_ylim()[1]])
ax.set_xlabel('')
#ax.get_legend().remove()
ax.get_legend().remove()
ax.set_xticklabels([predictors_labels_cap[t] for t in cond_vars],
                       rotation=45, ha="center")
ax.set_ylabel('CMI(spikes, position | $\cdot$)')
plt.tight_layout()

if save_plots:
    plot_name = 'info_barplot_cond_{}.{}'.format(settings_name,
                                                           plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


# --- BOXPLOT CONDITIONAL ON POSITION -----------------------------------------

targ_vars = ['speed',
             'hdir',
             'hdir_change']

f, ax = plt.subplots(1, 1, figsize=[small_square_side, 1*small_square_side],
                     sharey=True)

xx = df_full[(df_full['condvar'] == 'position')]
xx = xx[np.isin(xx['targetvar'], targ_vars)]

g = sns.barplot(ax=ax, data=xx, x='targetvar', y='cmi_deb',
                order=targ_vars, hue='area', hue_order=['A1', 'V2L'],
                palette=[area_colormap['A1'], area_colormap['V2L']])
sns.despine()
#ax[ii].set_ylim([-0.5, ax[ii].get_ylim()[1]])
ax.set_xlabel('')
#ax.get_legend().remove()
ax.get_legend().remove()
ax.set_xticklabels([predictors_labels_cap[t] for t in targ_vars],
                       rotation=45, ha="center")
ax.set_ylabel('CMI(spikes, $\\bf{\cdot}$ | position) [bits]')
plt.tight_layout()

if save_plots:
    plot_name = 'info_barplot_cond_{}.{}'.format(settings_name,
                                                           plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)




# --- BOXPLOT DIFFERENCE ------------------------------------------------------

cond_vars = ['speed',
             'hdir',
             'hdir_change']

dfs = []
mi = df_full[(df_full['targetvar'] == 'position') & (df_full['condvar'] == 'none')]
mi = mi.sort_values(by='unit_id')
for condvar in cond_vars:
    cmi = df_full[(df_full['targetvar'] == 'position') & (df_full['condvar'] == condvar)]
    cmi = cmi.sort_values(by='unit_id')
    np.testing.assert_array_equal(mi['unit_id'], cmi['unit_id'])
    diff = mi['cmi_deb'].as_matrix() - cmi['cmi_deb'].as_matrix()

    df = pd.DataFrame({'diff' : diff,
                       'condvar' : condvar,
                       'area' : mi['area']})
    dfs.append(df)
xx = pd.concat(dfs)

f, ax = plt.subplots(1, 1, figsize=[small_square_side, 1*small_square_side],
                     sharey=True)


g = sns.barplot(ax=ax, data=xx, x='condvar', y='diff',
                order=cond_vars, hue='area', hue_order=['A1', 'V2L'],
                palette=[area_colormap['A1'], area_colormap['V2L']])
sns.despine()
#ax[ii].set_ylim([-0.5, ax[ii].get_ylim()[1]])
ax.set_xlabel('')
#ax.get_legend().remove()
ax.get_legend().remove()
ax.set_xticklabels([predictors_labels_cap[t] for t in cond_vars],
                       rotation=45, ha="center")
ax.set_ylabel('Information contributed \non top of position [bits]')
plt.tight_layout()

if save_plots:
    plot_name = 'info_barplot_diff_{}.{}'.format(settings_name,
                                                           plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



# --- SCATTERPLOTS ------------------------------------------------------------

targetvar1 = 'position'
condvar1   = 'none'

targetvar2 = 'position'
condvar2   = 'hdir'


n_sigs = {'A1' : [], 'V2L' : []}

f, axes = plt.subplots(1, 2, figsize=[2*small_square_side, small_square_side],
                       sharey=True, sharex=True)
plt.setp(axes, aspect=1.0, adjustable='box-forced')

for area, ax in zip(AREAS, axes):
    dfx = df_full.loc[(df_full['area'] == area) &
                     (df_full['targetvar'] == targetvar1) &
                     (df_full['condvar'] == condvar1)]

    dfy = df_full.loc[(df_full['area'] == area) &
                     (df_full['targetvar'] == targetvar2) &
                     (df_full['condvar'] == condvar2)]

    thr = 0.05 / dfx.shape[0]

    sig_ids = dfy.loc[dfy['cmi_p_val'] < thr, 'unit_id']

    yns = dfy.loc[~np.isin(dfy['unit_id'], sig_ids), 'cmi_deb']
    xns = dfx.loc[~np.isin(dfx['unit_id'], sig_ids), 'cmi_deb']
    ys = dfy.loc[np.isin(dfy['unit_id'], sig_ids), 'cmi_deb']
    xs = dfx.loc[np.isin(dfx['unit_id'], sig_ids), 'cmi_deb']

    n_sigs[area] = [xs.shape[0], xns.shape[0]]

    ax.scatter(np.log10(xns), np.log10(yns), zorder=2, s=20, facecolors=sns.xkcd_rgb['grey'], alpha=0.8,
               edgecolors='none')
    ax.scatter(np.log10(xs), np.log10(ys), zorder=2, s=20, facecolors=area_colormap[area], alpha=0.8,
               edgecolors='none')

    plt.locator_params(axis='y', nbins=5)
    plt.locator_params(axis='x', nbins=5)
    ax.grid(ls=':', zorder=-1)
    logticks(ax)
    ax.set_xlabel(get_label(targetvar1, condvar1))

xlim, ylim = axes[0].get_xlim(), axes[0].get_ylim()
for area, ax in zip(AREAS, axes):
    ax.plot([-10, 10],  [-10, 10], ls="--", c='k')
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)

axes[0].set_ylabel(get_label(targetvar2, condvar2))
axes[1].set_ylabel('')
sns.despine()
plt.tight_layout()


if save_plots:
    plot_name = 'info_scatter_{}_{}_{}_{}_{}.{}'.format(settings_name,
                                                     targetvar1,
                                                     condvar1,
                                                     targetvar2,
                                                     condvar2,
                                                     plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


# --- SIGNIFICANCE DONUTS -----------------------------------------------------

size = 0.2
outer_colors = [area_colormap['A1'], sns.xkcd_rgb['grey']]
inner_colors = [area_colormap['V2L'], sns.xkcd_rgb['grey']]

f, ax = plt.subplots()

ax.pie(n_sigs['A1'], radius=1, colors=outer_colors,
       wedgeprops=dict(width=size, edgecolor='w'))
ax.pie(n_sigs['V2L'], radius=1 - size, colors=inner_colors,
       wedgeprops=dict(width=size, edgecolor='w'))
ax.axis('equal')
plt.show()

print('{} sign A1 out of {}'.format(n_sigs['A1'][0], np.sum(n_sigs['A1'])))
print('{} sign V2L out of {}'.format(n_sigs['V2L'][0], np.sum(n_sigs['V2L'])))

if save_plots:
    plot_name = 'info_scatter_donut_{}_{}_{}_{}_{}.{}'.format(settings_name,
                                                     targetvar1,
                                                     condvar1,
                                                     targetvar2,
                                                     condvar2,
                                                     plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
