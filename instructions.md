

# DECODING
- __decode_compute.py__ runs the main decoding, run with decoder
set to 'dkw' for Bayesian decoding, and to 'rf' for Random forest.

- To generate the plots, run (always specifying settings name
and decoder at the top of the script)
1. __decode_load.py__, which generates the confusion matrices and
some panels on the error correlations between A1 and V2L.
2. __decode_load_errcorr.py__, which loads the decoding results and
looks at the correlation between the decoding errors in the two areas.
Here there is parameter called `include_sessions` which determines whether
in the analysis of error correlation you want to include only sessions
which are good (=enough neurons) in both areas, or all sessions in
which at least on area has enough neurons. Since the computation relies
on random shuffling, we set a random seed to ensure that results
are reproducible.

- In __inverse_decoding.py__ we look at the error in predicting the
predictions of an area with vs without the predictions of the other area.
This script needs to load previously run decoding results, so you
need to set the decoding settings name + decoder name at the top. The
inverse decoding is then performed with a random forest.

- __decode_load_noisecorr.py__ loads the outputs of two decoding runs
and plots the errors together (the idea is to compare normal decoding
to a decoding in which we destroy noise correlations).


# INFORMATION THEORETIC ANALYSIS

- __info_compute.py__ computes mutual and conditional mutual information.

- __info_load.py__ generates the plots and statistics.


# ENCODING

- __encode_compute.py__ runs the encoding across all neurons.

-