import pickle
from constants import *
from utils import *
import quantities as pq
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from prepare_decoding_data import PrepareDecodingData
from sklearn.model_selection import train_test_split

# decoding parameters
settings_name = 'aug23'
min_spikes = 5
min_speed = 0.1
decoder = 'rf'
test_split = 0.2
shuffle_split = True
n_group_size_reps=50
min_group_size = 5
group_size_step = 1
binsize_in_ms = 400
coarsify_bins=True
coarse_bin_size=3
sessions = GOOD_SESSIONS_ONE_AREA


output_file_name = 'decode_bootstrap_{}_{}.pkl'.format(settings_name, decoder)
output_folder = os.path.join(DATA_FOLDER, 'results', 'dec')
output_full_path = os.path.join(output_folder, output_file_name)

if not os.path.isdir(output_folder):
    os.makedirs(output_folder)


# --- COLLECT PARAMETERS ------------------------------------------------------

pars = {'setting_name'      : settings_name,
        'sessions'          : sessions,
        'min_spikes'        : min_spikes,
        'min_speed'         : min_speed,
        'decoder'           : decoder,
        'test_split'        : test_split,
        'shuffle_split'     : shuffle_split,
        'n_group_size_reps' : n_group_size_reps,
        'min_group_size'    : min_group_size,
        'group_size_step'   : group_size_step,
        'binsize_in_ms'     : binsize_in_ms,
        'coarsify_bins'     : coarsify_bins,
        'coarse_bin_size'   : coarse_bin_size}

# --- DECODE ------------------------------------------------------------------

rs = {area : {s : {} for s in GOOD_SESSIONS_BY_AREA[area]} for area in ['A1', 'V2L']}

for sess_ind in sessions:

    # --- LOAD DATA ---------------------------------------------------------------

    file_name = 'navig8_session_{}.pkl'.format(sess_ind)
    bl = pickle.load(open(os.path.join(DATA_FOLDER, file_name), 'rb'))
    segment = bl.segments[0]
    A1_ind = [i for i, t in enumerate(segment.spiketrains)
                       if t.annotations['area'] == 'A1']
    V2L_ind = [i for i, t in enumerate(segment.spiketrains)
                       if t.annotations['area'] == 'V2L']
    area_unit_ind = {'A1' : A1_ind, 'V2L' : V2L_ind}
    lin_speed = get_analogsignal_by_name(segment, 'lin_speed')
    lin_bin = get_analogsignal_by_name(segment, 'lin_bin')
    xy_pos = get_analogsignal_by_name(segment, 'xy_pos')

    lin_bin_arr = lin_bin.__array__()[:, 0]
    xy_pos_arr = xy_pos.__array__()
    lin_speed_arr = lin_speed.__array__()[:, 0]


    # --- PREPARE DATA FOR DECODING -----------------------------------------------

    pdd = PrepareDecodingData(spike_trains=segment.spiketrains,
                              position=xy_pos_arr,
                              linearized_position=lin_bin_arr,
                              speed=lin_speed_arr,
                              times=lin_bin.times.rescale(pq.ms))

    if coarsify_bins:
        pdd.coarsify_bins(new_bin_size=coarse_bin_size)

    pdd._build_distance_matrix()
    pdd.bin_spikes(binsize_in_ms=binsize_in_ms)
    pdd.interpolate_position_and_speed()

    pdd.filter_nans()
    pdd.filter_min_speed(min_speed)
    pdd.filter_min_spikes(min_spikes)

    X = pdd.binned_spikes
    y = pdd.interp_linearized_position
    z = pdd.interp_speed

    # z = gaussian_filter1d(z, sigma=5)
    # y = pd.qcut(z, 10, labels=False, retbins=False)


    # --- DECODE ------------------------------------------------------------------

    for area in GOOD_SESSIONS_ONE_AREA[sess_ind]:
        unit_ind = area_unit_ind[area]

        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = test_split,
                                                            shuffle=shuffle_split,
                                                            random_state=sess_ind)

        X_train = X_train[:, unit_ind]
        X_test = X_test[:, unit_ind]

        group_sizes = np.arange(min_group_size, X_train.shape[1], group_size_step)

        rs[area][sess_ind]['group_sizes'] = group_sizes
        rs[area][sess_ind]['error'] = np.zeros([len(group_sizes), n_group_size_reps])

        for gsz_ind, sz in enumerate(group_sizes):

            print(' -- Decoding session {} - group size {}'.format(sess_ind, sz))

            for boot_ind in range(n_group_size_reps):

                bootstrap_units = np.random.choice(range(X_train.shape[1]), size=sz,
                                                   replace=False)

                X_train_group = X_train[:, bootstrap_units]
                X_test_group = X_test[:, bootstrap_units]

                if decoder == 'rf':
                    dec = RandomForestClassifier(n_estimators=100,
                                                 random_state=92)

                elif decoder == 'dkw':
                    dec = DKW_decoder(bin_size_train=binsize_in_ms,
                                      bin_size_test=binsize_in_ms)

                elif decoder == 'lr':
                    dec = LogisticRegression()

                dec.fit(X_train_group, y_train)
                y_pred = dec.predict(X_test_group)

                error = np.zeros(y_test.shape[0])
                for i, (true_bin, pred_bin) in enumerate(zip(y_test, y_pred)):
                    error[i] = pdd.get_error_distance(true_bin, pred_bin)

                rs[area][sess_ind]['error'][gsz_ind, boot_ind] = np.mean(error)



output = {'decoding_pars' : pars,
         'decoding_results' : rs}

print('Saving output to {}'.format(output_full_path))
pickle.dump(output, open(output_full_path, 'wb'))


