import numpy as np
from sklearn.model_selection import KFold
from DKW_decoder import DKW_decoder
from sklearn.preprocessing import MinMaxScaler
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
import quantities as pq

def z_score(X, neuron_axis=1):
    if neuron_axis == 1:
        X_mean = np.mean(X, axis=0)
        X_std  = np.std(X, axis=0)
        X_std[X_std == 0] = 1
        Xz = (X - X_mean) / X_std
    elif neuron_axis == 0:
        X_mean = np.mean(X, axis=1)[:, np.newaxis]
        X_std  = np.std(X, axis=1)[:, np.newaxis]
        X_std[X_std == 0] = 1
        Xz = (X - X_mean) / X_std
    return Xz


def min_max_scale(X, min=0, max=1, neuron_axis=1):
    mm = MinMaxScaler(feature_range=(min, max))

    if neuron_axis == 1:
        Xm = mm.fit_transform(X)
    elif neuron_axis == 0:
        Xm = mm.fit_transform(X.T).T
    return Xm


def bin_spikes_sliding_window(trains, t_start, t_stop, wdw_size, slide_by,
                              fully_within=False):
    """
    If fully_within is set to True, all bins must be within t_start and
    t_stop. Otherwise we allow for the last bin to end past t_stop
    """

    t_start = t_start.rescale(pq.ms)
    t_stop = t_stop.rescale(pq.ms)
    wdw_size = wdw_size.rescale(pq.ms)
    slide_by = slide_by.rescale(pq.ms)

    left_edges = np.arange(t_start, t_stop, slide_by) * pq.ms
    bins = [(e, e + wdw_size) for e in left_edges]

    # make sure that all the bin centers are within the event
    bins = [b for b in bins if (b[0] + b[1]) / 2  <= t_stop]

    # make sure that all bins are fully within the event
    if fully_within:
        bins = [b for b in bins if b[1] <= t_stop]

    # prepare the bin centers (to return)
    bin_centers = [(b1 + b2) / 2 for b1, b2 in bins]

    num_bins = len(bins)  # Number of bins
    num_neurons = len(trains)  # Number of neurons
    neural_data = np.empty([num_bins, num_neurons])

    for i, train in enumerate(trains):
        spike_times = train.times.rescale(pq.ms)
        for t, bin in enumerate(bins):
            neural_data[t, i] = np.histogram(spike_times, bin)[0]
    return neural_data, bin_centers


def bayesian_cross_validate(X, y, bin_size, n_splits=2):
    kf = KFold(n_splits=n_splits)
    y_pred_all, y_true_all = [], []
    for train_index, test_index in kf.split(X):
        X_train, X_test = X[train_index, :], X[test_index, :]
        y_train, y_test = y[train_index], y[test_index]

        dec = DKW_decoder(bin_size_train=bin_size,
                          bin_size_test=bin_size)

        dec.fit(X_train, y_train)
        y_pred = dec.predict(X_test)
        y_pred_all.append(y_pred)
        y_true_all.append(y_test)

    y_pred = np.hstack(y_pred_all)
    y_true = np.hstack(y_true_all)

    return y_true, y_pred



def interpolate_outputs(outputs, output_times, new_times):
    output_dim = outputs.shape[1]  # Number of output features
    outputs_binned = np.empty([new_times.shape[0], output_dim])  # Initialize matrix of binned outputs

    for k in range(output_dim):
        outputs_binned[:, k] = np.interp(new_times, output_times, outputs[:, k])
    return outputs_binned


def get_analogsignal_by_name(segment, name):
    for i, sig in enumerate(segment.analogsignals):
        if sig.name == name:
            ind = i
    return segment.analogsignals[ind]


def chop_analogsignal(analogsignal, t_start, t_stop):
    times = analogsignal.times
    units = analogsignal.units
    m = np.logical_and(times >= t_start, times < t_stop)
    output =  analogsignal.__array__()[m] * units
    return output


def running_mean(x, N):
    cumsum = np.cumsum(np.insert(x, 0, 0))
    return (cumsum[N:] - cumsum[:-N]) / float(N)


def compute_decoding_error(X_train, y_train, X_test, y_test,
                           decoder, distance_df, n_estimators=100, binsize_in_ms=None):

    if decoder == 'random_forest':
        dec = RandomForestClassifier(n_estimators=n_estimators)

    elif decoder == 'dkw':
        dec = DKW_decoder(bin_size_train=binsize_in_ms,
                          bin_size_test=binsize_in_ms)

    elif decoder == 'logistic_regression':
        dec = LogisticRegression()

    dec.fit(X_train, y_train)
    y_pred = dec.predict(X_test)

    error = np.zeros(y_test.shape[0])
    for i, (true_bin, pred_bin) in enumerate(zip(y_test, y_pred)):
        error[i] = distance_df.loc[true_bin, pred_bin]

    return error.mean()


def cohend(d1, d2):
	# calculate the size of samples
	n1, n2 = len(d1), len(d2)
	# calculate the variance of the samples
	s1, s2 = np.var(d1, ddof=1), np.var(d2, ddof=1)
	# calculate the pooled standard deviation
	s = np.sqrt(((n1 - 1) * s1 + (n2 - 1) * s2) / (n1 + n2 - 2))
	# calculate the means of the samples
	u1, u2 = np.mean(d1), np.mean(d2)
	# calculate the effect size
	return (u1 - u2) / s


hedges_g_paired = """hedges_g_paired = function(df)
    {
    library('effsize')
    x = df$x
    y = df$y
    cohend = cohen.d(x, y, pooled=TRUE, paired=TRUE, hedges.correction=TRUE)
    out = c(cohend$estimate, cohend$conf.int[1], cohend$conf.int[2])
    return (out)
    }
    """

hedges_g_not_paired = """hedges_g_not_paired = function(df)
    {
    library('effsize')
    x = df$x
    y = df$y
    cohend = cohen.d(x, y, pooled=TRUE, paired=FALSE, hedges.correction=TRUE)
    out = c(cohend$estimate, cohend$conf.int[1], cohend$conf.int[2])
    return (out)
    }
    """

cohens_d = """cohens_d = function(df)
    {
    library('effsize')
    x = df$x
    y = df$y
    cohend = cohen.d(x, y, pooled=TRUE, paired=TRUE, hedges.correction=FALSE)
    out = c(cohend$estimate, cohend$conf.int[1], cohend$conf.int[2])
    return (out)
    }
    """






def destroy_noise_correlations(X, y):

    """
    Perform a shuffling of the columns of X per bin of y.
    """

    X_shuf = np.zeros_like(X)
    bins = np.unique(y)

    for neu_ind in range(X.shape[1]):
        shuffled_neuron = []
        for bin in bins:
            ind = np.where(y == bin)[0]
            ind_shuf = np.random.permutation(ind)
            shuffled_neuron.append(X[ind_shuf, neu_ind])

        X_shuf[:, neu_ind] = np.hstack(shuffled_neuron)

    y_shuf = []
    for bin in bins:
        ind = np.where(y == bin)[0]
        y_shuf.append(y[ind])
    y_shuf = np.hstack(y_shuf)

    return X_shuf, y_shuf


def order_by_bin(X, y):

    """
    Order by bin of y.
    """

    X_shuf = np.zeros_like(X)
    bins = np.unique(y)

    for neu_ind in range(X.shape[1]):
        shuffled_neuron = []
        for bin in bins:
            ind = np.where(y == bin)[0]
            shuffled_neuron.append(X[ind, neu_ind])
        X_shuf[:, neu_ind] = np.hstack(shuffled_neuron)

    y_shuf = []
    for bin in bins:
        ind = np.where(y == bin)[0]
        y_shuf.append(y[ind])
    y_shuf = np.hstack(y_shuf)

    return X_shuf, y_shuf




if __name__ == '__main__':
    X = np.array([[0, 1, 0, 1, 2, 3, 2, 3, 10, 11, 10, 11],
                  [0, 1, 0, 1, 2, 3, 2, 3, 10, 11, 10, 11]]).T
    y = np.array([1, 1, 1, 1, 2, 2, 2, 2, 1, 1, 1, 1])

    X, y = destroy_noise_correlations(X, y)