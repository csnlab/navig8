import seaborn as sns
from matplotlib.ticker import FuncFormatter
import pandas as pd
import seaborn as sns
from matplotlib.lines import Line2D

desired_width = 600
pd.set_option('display.width', desired_width)
pd.set_option("display.max_columns", 14)


area_colormap = {'A1' : sns.xkcd_rgb['faded blue'],
                 'V2L' : sns.xkcd_rgb['faded red']}


small_square_side = 3


error_labels = {'error_x' : 'error ($x$ direction)',
                'error_y': 'error ($y$ direction)',
                'error' : 'error',
                'maze_error' : 'error (maze)'}

predictors_labels = {'position'    : 'position',
                     'hdir'        :  r'$\theta_{head}$',
                     'hdir_change' : r'$\Delta \theta_{head}$',
                     'speed'       : 'speed'}

predictors_labels_cap = {'position'    : 'Position',
                         'hdir'        :  r'$\theta_{head}$',
                         'hdir_change' : r'$\Delta \theta_{head}$',
                         'speed'       : 'Speed',
                         'position+speed+hdir+hdir_change' : 'All predictor'}

predictors_colormap = {'position'  : sns.xkcd_rgb['bright purple'],
                       'speed'     : sns.xkcd_rgb['bright blue'],
                       'hdir'      : sns.xkcd_rgb['bright orange'],
                       'hdir_change' : sns.xkcd_rgb['bright yellow'],
                       'position+speed+hdir+hdir_change' : sns.xkcd_rgb['bright red']}


def get_label(targetvar, condvar):

    if condvar == 'none':
        label = 'MI(spikes, {})'.format(predictors_labels[targetvar])
    else:
        label = 'CMI(spikes, {} | {})'.format(predictors_labels[targetvar],
                                              predictors_labels[condvar])
    return label


def myticks(x, pos):
    if x == 0: return "$0$"
    return r"$10^{{ {:2d} }}$".format(int(x))


def logticks(ax):
    ax.xaxis.set_major_formatter(FuncFormatter(myticks))
    ax.yaxis.set_major_formatter(FuncFormatter(myticks))