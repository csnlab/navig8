import os
import pickle
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from constants import *
from plotting_style import *
from utils import *
from scipy.stats import ttest_rel
import rpy2
from rpy2.robjects import pandas2ri
from scipy.stats import wilcoxon

output_folder = os.path.join(DATA_FOLDER, 'results', 'encode')

settings_name = 'final_nostim'
encoding_model = 'random_forest'
paired_effect_size = True
plot_hist = False
plot_kde = True
# plotting_parameters
save_plots=True
plot_format = 'svg'
dpi = 400
plot_folder = os.path.join(DATA_FOLDER, 'plots', 'encode', settings_name)


if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

output_file_name_r2   = 'encode_{}_{}.pkl'.format(settings_name, encoding_model)
output_file_name_pred = 'encode_{}_{}_predictions.pkl'.format(settings_name, encoding_model)
output_folder         = os.path.join(DATA_FOLDER, 'results', 'encode')
output_full_path_r2   = os.path.join(output_folder, output_file_name_r2)
output_full_path_pred = os.path.join(output_folder, output_file_name_pred)
results_predictions   = pickle.load(open(output_full_path_pred, 'rb'))
results_r2            = pickle.load(open(output_full_path_r2, 'rb'))

pars = results_r2['pars']
df_full = results_r2['df']
sessions = pars['sessions']
binsize_in_ms = pars['binsize_in_ms']


#df_full[df_full['area'] == 'V2L'].sort_values(by='r2')['unit_id']

# --- SELECT UNITS ------------------------------------------------------------
#
# sel_neurons = df_full.loc[(df_full['predictor'] == 'position+speed+hdir+hdir_change') &
#               (df_full['r2'] > 0), 'unit_id'].as_matrix()
#
# df_full = df_full[np.isin(df_full['unit_id'], sel_neurons)]


# --- STATISTICS ON ENCODING QUALITY ------------------------------------------
print('Statistics on encoding quality')
comparisons = [['position+speed+hdir+hdir_change', 'position'],
               ['position', 'speed'],
               ['position', 'hdir'],
               ['position', 'hdir_change']]
n_comparisons = len(comparisons)

for pred1, pred2 in comparisons:
    xs = df_full.loc[(df_full.predictor == pred1)].sort_values(by='unit_id')
    ys = df_full.loc[(df_full.predictor == pred2)].sort_values(by='unit_id')

    np.testing.assert_array_equal(xs['unit_id'], ys['unit_id'])

    x = xs['r2']
    y = ys['r2']

    stat, p = wilcoxon(x, y)
    #p = p * n_comparisons
    print('{} vs {} : {:.4e}, {:3f}:{:3f}'.format(pred1, pred2, p, x.mean(), y.mean()))




# --- BOXPLOT IMPROVEMENT IN ENCODING QUALITY ---------------------------------

single_predictors = ['position', 'speed', 'hdir', 'hdir_change']
all_predictors = '+'.join(single_predictors)

dfs = []
for predictor in single_predictors:

    other_predictors = [i for i in single_predictors if i != predictor]
    other_predictors = '+'.join(other_predictors)
    print(predictor, other_predictors)

    df = df_full
    for area in AREAS:
        xs = df.loc[(df.area == area) & (df.predictor == all_predictors)].sort_values(by='unit_id')
        ys = df.loc[(df.area == area) & (df.predictor == other_predictors)].sort_values(by='unit_id')

        np.testing.assert_array_equal(xs.unit_id, ys.unit_id)

        newdf = pd.DataFrame(columns=['unit_id', 'area', 'predictor', 'r2'])
        newdf['unit_id'] = xs.unit_id
        newdf['area'] = area
        newdf['predictor'] = predictor
        newdf['r2'] = xs['r2'].as_matrix() - ys['r2'].as_matrix()
        dfs.append(newdf)

resdf = pd.concat(dfs)


predictor_order = ['speed','hdir', 'hdir_change','position']

f, ax = plt.subplots(1, 1, figsize=[small_square_side, small_square_side])
sns.despine()
g = sns.barplot(ax=ax, data=resdf, x='predictor', y='r2', hue='area',
            order=predictor_order,
            palette=[area_colormap['A1'], area_colormap['V2L']],
            hue_order=['A1', 'V2L'])
ax.set_xlabel('')
ax.get_legend().remove()
ax.set_ylabel('Improvement in encoding above\n all other predictors (pseudo $R^2$)')
ax.set_xticklabels([predictors_labels_cap[t] for t in predictor_order],
                       rotation=45, ha="center")
plt.tight_layout()

if save_plots:
    plot_name = 'encode_barplot_improvement_{}_{}.{}'.format(settings_name, encoding_model,
                                                           plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



# --- STATISTICS ON IMPROVEMENT IN ENCODING QUALITY ---------------------------

print('\n\nStatistics on improvement in encoding quality')

comparisons = [['position', 'speed'],
               ['position', 'hdir'],
               ['position', 'hdir_change']]

n_comparisons = len(comparisons)

for pred1, pred2 in comparisons:
    xs = resdf.loc[(resdf.predictor == pred1)].sort_values(by='unit_id')
    ys = resdf.loc[(resdf.predictor == pred2)].sort_values(by='unit_id')

    np.testing.assert_array_equal(xs['unit_id'], ys['unit_id'])

    x = xs['r2']
    y = ys['r2']

    stat, p = wilcoxon(x, y)
    #p = p * n_comparisons
    print('{} vs {} : {:.4e}, {:3f}:{:3f}'.format(pred1, pred2, p, x.mean(), y.mean()))



print('\n\nDifference from 0 of improvement in enc for head direction')
for area in AREAS:
    xx = resdf.loc[(resdf.predictor == 'hdir') & (resdf.area == area), 'r2'].values
    stat, p = wilcoxon(xx)
    print('{} - p={:.4e} (mean: {:.4})'.format(area, p, xx.mean()))



# --- BARPLOT -----------------------------------------------------------------

predictors = ['speed',
              'hdir',
              'hdir_change',
              'position',
              'position+speed+hdir+hdir_change']

f, ax = plt.subplots(1, 1, figsize=[small_square_side, small_square_side],
                     sharey=True)

xx = df_full[np.isin(df_full['predictor'], predictors)]

g = sns.barplot(ax=ax, data=xx, x='predictor', y='r2',
                order=predictors, hue='area', hue_order=['A1', 'V2L'],
                palette=[area_colormap['A1'], area_colormap['V2L']])
sns.despine()
ax.set_xlabel('')
ax.set_ylabel('Encoding quality (pseudo $R^2$)')
ax.get_legend().remove()
ax.set_xticklabels([predictors_labels_cap[t] for t in predictors],
                       rotation=45, ha="center")
plt.tight_layout()


if save_plots:
    plot_name = 'encode_barplot_predictors_{}_{}.{}'.format(settings_name, encoding_model,
                                                           plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)

# --- DISTPLOT ----------------------------------------------------------------

predictor = 'position'

df = df_full[df_full['predictor'] == predictor]
bins = np.arange(df['r2'].min(), df['r2'].max(), 0.05)

f, ax = plt.subplots(1, 1, figsize=[small_square_side, small_square_side],
                     sharey=True)
for area in AREAS:
    dfx = df[df['area'] == area]
    g = sns.distplot(dfx['r2'], ax=ax,
                    color=area_colormap[area], bins=bins, norm_hist=True)
sns.despine()
ax.set_xlim([df['r2'].min(), ax.get_xlim()[1]])
ax.set_xlabel('Encoding with {} (pseudo $R^2$)'.format(predictor))
ax.set_ylabel('Density')
plt.tight_layout()




# --- EFFECT SIZE -------------------------------------------------------------

"""
For every session, we take the R2s of all neurons with all predictor and
with all predictor minus one, and we compute the paired effect size. 
"""

if paired_effect_size:
    rpy2.robjects.r(hedges_g_paired)
    hedges_g_r_fun = rpy2.robjects.globalenv['hedges_g_paired']

else:
    rpy2.robjects.r(hedges_g_not_paired)
    hedges_g_r_fun = rpy2.robjects.globalenv['hedges_g_not_paired']

pandas2ri.activate()

effsz_rs = pd.DataFrame(columns=['predictor', 'session', 'area', 'p_val',
                           'hedges_g', 'ci1', 'ci2', 'is_significant'])


single_predictors = ['position', 'speed', 'hdir', 'hdir_change']
all_predictors = '+'.join(single_predictors)

for predictor in single_predictors:

    other_predictors = [i for i in single_predictors if i != predictor]
    other_predictors = '+'.join(other_predictors)
    print(predictor, other_predictors)

    for sess_ind in sessions:

        if isinstance(sess_ind, int):
            df = df_full.loc[df_full['session'] == sess_ind]
        elif sess_ind == 'all':
            df = df_full


        for area in ['A1', 'V2L']:

            xs = df.loc[(df.area == area) & (df.predictor == all_predictors), 'r2']
            ys = df.loc[(df.area == area) & (df.predictor == other_predictors), 'r2']

            if (xs.shape[0] > 0) and (ys.shape[0] > 0):
                tstat, pval = ttest_rel(xs, ys)

                rdf = pd.DataFrame(columns=['x', 'y'])
                rdf.loc[:, 'x'] = xs.__array__()
                rdf.loc[:, 'y'] = ys.__array__()
                g_estimate, ci1, ci2 = hedges_g_r_fun(rdf)
                #g_estimate, ci1, ci2 = cohend(ys, xs), 0, 0
                sig = not ci1 < 0 < ci2

                effsz_rs.loc[effsz_rs.shape[0], :] = [predictor, sess_ind,
                                                      area, pval, g_estimate,
                                                      ci1, ci2, sig]



# --- EFFECT SIZE PLOT --------------------------------------------------------

predictors = ['speed',
              'hdir',
              'hdir_change',
              'position']

f, ax = plt.subplots(1, 1, figsize=[small_square_side, small_square_side])
sns.despine(ax=ax)
sns.barplot(data=effsz_rs, x='predictor', y='hedges_g', hue='area', ax=ax,
            palette=[area_colormap['A1'], area_colormap['V2L']], order=predictors,
            hue_order=['A1', 'V2L'])
ax.get_legend().remove()
ax.set_ylabel('Effect size')
ax.set_xlabel('')
ax.set_xticklabels([predictors_labels_cap[t] for t in predictors],
                   rotation=45, ha="center")
plt.tight_layout()

if save_plots:
    plot_name = 'encode_barplot_effectsize_{}_{}.{}'.format(settings_name, encoding_model,
                                                           plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)

# esa1 = effsz_rs.loc[(effsz_rs.session == 'all') & (effsz_rs.area == 'A1')]
# esv2l = effsz_rs.loc[(effsz_rs.session == 'all') & (effsz_rs.area == 'V2L')]
#
# x1 = float(esa1['hedges_g'])
# x2 = float(esv2l['hedges_g'])

# # BAR PLOT --------------------------------------------------------------------
# f, ax = plt.subplots(1, 1, figsize=[3, 3])
# ax.bar(0, x1, yerr=[[x1 - float(esa1['ci1']), x1 + float(esa1['ci2'])]],
#        color=sns.desaturate(area_colormap['A1'],0.75))
# ax.bar(1, x2, yerr=[[x2 - float(esv2l['ci1']), x2 + float(esv2l['ci2'])]],
#        color=sns.desaturate(area_colormap['V2L'], 0.75))
# sns.despine()
# ax.set_ylabel('Hedge\'s g')
# ax.set_xticks([0,1])
# ax.set_xticklabels(['A1', 'V2L'])
# plt.tight_layout()
# if save_plots:
#     plot_name = 'encode_hedges_g_all_sessions_{}_{}ms_multiplesessions.{}'.format(encoding_model,
#                                                                        binsize_in_ms, plot_format)
#     f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


# SCATTER PLOT OF EFFECT SIZE -------------------------------------------------
# x = effsz_rs[effsz_rs['area'] == 'A1']['hedges_g']
# y = effsz_rs[effsz_rs['area'] == 'V2L']['hedges_g']
# f, ax = plt.subplots(1, 1, figsize=[3, 3])
# ax.scatter(x, y, edgecolor='k', facecolor='w', zorder=5, s=28)
# ax.plot([0, 1], [0, 1], transform=ax.transAxes, ls='--', color='k', alpha=0.5)
# ax.axis('square')
# ax.set_xlabel('A1 (Hedge\'s g)')
# ax.set_ylabel('V2L (Hedge\'s g)')
# sns.despine()
# plt.tight_layout()
#
# # df = pd.DataFrame(columns=['x', 'y'])
# # df['x'], df['y'] = rs['cohensd_A1'].astype(float), rs['cohensd_V2L'].astype(float)
# # g_estimate, ci1, ci2 = hedges_g_r_fun(df)
#
# if save_plots:
#     plot_name = 'encode_hedges_g_w_{}_{}ms_multiplesessions.{}'.format(encoding_model,
#                                                                        binsize_in_ms, plot_format)
#     f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


# ENCODING SCATTER PLOT -------------------------------------------------------

single_predictors = ['position', 'speed', 'hdir', 'hdir_change']

for predictor in single_predictors:

    other_predictors = [i for i in single_predictors if i != predictor]
    other_predictors = '+'.join(other_predictors)
    print(predictor, other_predictors)


    df = df_full

    xs_a1 = df.loc[(df.area == 'A1') & (df.predictor == other_predictors), 'r2']
    ys_a1 = df.loc[(df.area == 'A1') & (df.predictor == all_predictors), 'r2']

    xs_v2l = df.loc[(df.area == 'V2L') & (df.predictor == other_predictors), 'r2']
    ys_v2l = df.loc[(df.area == 'V2L') & (df.predictor == all_predictors), 'r2']

    p = sns.JointGrid(xs_a1, ys_a1, height=4)
    p.ax_joint.scatter(xs_v2l, ys_v2l, color=area_colormap['V2L'], s=24, alpha=0.8,
                       edgecolors='none')
    p.ax_joint.scatter(xs_a1, ys_a1, color=area_colormap['A1'], s=24, alpha=0.8,
                       edgecolors='none')

    p.ax_joint.set_xlim([-0.2, 1])
    p.ax_joint.set_ylim([-0.2, 1])

    x_lim, y_lim = p.ax_joint.get_xlim(), p.ax_joint.get_ylim()
    sns.distplot(xs_a1, ax=p.ax_marg_x, vertical=False, color=area_colormap['A1'],
                 kde=plot_kde, hist=plot_hist)
    sns.distplot(ys_a1, ax=p.ax_marg_y, vertical=True, color=area_colormap['A1'],
                kde=plot_kde, hist=plot_hist)
    sns.distplot(xs_v2l, ax=p.ax_marg_x, vertical=False, color=area_colormap['V2L'],
                kde=plot_kde, hist=plot_hist)
    sns.distplot(ys_v2l, ax=p.ax_marg_y, vertical=True, color=area_colormap['V2L'],
                kde=plot_kde, hist=plot_hist)
    # p.ax_joint.plot([0, 1], [0, 1], transform=p.ax_joint.transAxes, ls='--',
    #                 color='k', alpha=0.8)
    p.ax_joint.plot([-0.5, 1], [-0.5, 1],
                    color='k', alpha=0.8, ls='--')
    p.ax_joint.set_xticks(np.arange(-0.5, 1+0.5, 0.5))
    p.ax_joint.set_yticks(np.arange(-0.5, 1+0.5, 0.5))
    p.ax_marg_y.set_ylim(y_lim)
    p.ax_marg_x.set_xlim(x_lim)
    p.ax_joint.set_xlabel('Encoding with all predictor\nexcept {} [pseudo $R^2$]'.format(predictors_labels[predictor]))
    p.ax_joint.set_ylabel('Encoding with all predictor [pseudo $R^2$]')
    p.ax_marg_x.set_xlabel('')
    p.ax_marg_y.set_ylabel('')
    p.ax_joint.grid(ls=':')

    plt.tight_layout()

    if save_plots:
        if isinstance(sess_ind, int):
            plot_name = 'encode_jointplot_{}_w_{}_{}ms_sess_{:02}.{}'.format(predictor, encoding_model, binsize_in_ms,
                                                                    sess_ind, plot_format)
        else:
            plot_name = 'encode_jointplot_{}_w_{}_{}ms_multiplesessions.{}'.format(predictor, encoding_model,
                                                                    binsize_in_ms,  plot_format)

        p.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)




