import os
from loadmat import loadmat
from constants import *
import mat73
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

xcpath = os.path.join(DATA_FOLDER, 'XCData_combo.mat')

xc = mat73.loadmat(xcpath)


plot_folder = os.path.join(DATA_FOLDER, 'plots', 'xcorr')
if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

df = pd.DataFrame(columns=['session_id', 'rat_id', 'AC_id', 'V2L_id'])

df['session_id'] = [int(id) for id in xc['Session_id']]
df['rat_id'] = [int(id) for id in xc['Rat_id']]
df['AC_id'] = [int(id) for id in xc['AC_id']]
df['V2L_id'] = [int(id) for id in xc['V2L_id']]
# df['AC_id'] = ['{}_{}'.format(int(s), int(u)) for s, u in zip(xc['Session_id'], xc['AC_id'])]
# df['V2L_id'] = ['{}_{}'.format(int(s), int(u)) for s, u in zip(xc['Session_id'], xc['V2L_id'])]

xcarr = xc['XCorrs']

t = np.arange(xcarr.shape[1]) - 151


# xc_before = xcarr[:, 140:150]
# xc_after  = xcarr[:, 151:161]

# --- OPTION 1 -----------------------------------------------------------------
xcarr_for_df = np.vstack([np.hstack((t[:, np.newaxis], x[:, np.newaxis])) for x in xcarr])
xcarr_df = pd.DataFrame(xcarr_for_df, columns=['t', 'xcorr'])

f, ax = plt.subplots(1, 1)
sns.lineplot(data=xcarr_df, x='t', y='xcorr', ax=ax)
sns.despine()
plt.tight_layout()


xcarr_df['epoch'] = 'none'
xcarr_df.loc[np.isin(xcarr_df['t'], np.arange(-10, 0)), 'epoch'] = 'before'
xcarr_df.loc[np.isin(xcarr_df['t'], np.arange(1, 11)), 'epoch'] = 'after'

f, ax = plt.subplots(1, 1)
sns.barplot(data=xcarr_df, x='epoch', y='xcorr', ax=ax)



# --- OPTION 2 -----------------------------------------------------------------
# average the 10 ms before and after 0 for every pair

xc_before = xcarr[:, 140:150].mean(axis=1)
xc_after  = xcarr[:, 151:161].mean(axis=1)

xcarr_for_df = np.hstack([xc_before, xc_after])
label = np.hstack((np.repeat('before', xc_before.shape[0]), np.repeat('after', xc_after.shape[0])))
xcarr_df = pd.DataFrame(columns=['label', 'xcorr'])
xcarr_df['label'] = label
xcarr_df['xcorr'] = xcarr_for_df

f, ax = plt.subplots(1, 1)
sns.barplot(data=xcarr_df, x='label', y='xcorr', ax=ax)


# --- OPTION 3 -----------------------------------------------------------------
# for every AC unit, average the xcorr across pairs
# average the xcorr across the 10 ms before and after

du = pd.DataFrame(columns=['AC_id', 'label', 'xcorr'])
for acid in df['AC_id'].unique():
    idx = np.where(df['AC_id'] == acid)[0]

    xbf = xcarr[idx, 140:150].mean(axis=0).mean(axis=0)
    xaf = xcarr[idx, 151:161].mean(axis=0).mean(axis=0)

    du.loc[du.shape[0], :] = [acid, 'before', xbf]
    du.loc[du.shape[0], :] = [acid, 'after', xaf]

f, ax = plt.subplots(1, 1, figsize=[2, 3])
sns.barplot(data=du, x='label', y='xcorr', ax=ax)
sns.despine()
plt.tight_layout()

# lineplot averaging per unit

xcarr_per_unit = []
for acid in df['AC_id'].unique():
    idx = np.where(df['AC_id'] == acid)[0]
    xperu = xcarr[idx, :].mean(axis=0)
    xcarr_per_unit.append(xperu)

arr = np.vstack(xcarr_per_unit)

xcarr_for_df = np.vstack([np.hstack((t[:, np.newaxis], x[:, np.newaxis])) for x in arr])
xcarr_df = pd.DataFrame(xcarr_for_df, columns=['t', 'xcorr'])

f, ax = plt.subplots(1, 1)
sns.lineplot(data=xcarr_df, x='t', y='xcorr', ax=ax)
sns.despine()
plt.tight_layout()




# --- OPTION 4 -----------------------------------------------------------------
# is the xcorr value per AC cell (averaged across pairs) larger in the 10 ms
# before zero than after?

du = pd.DataFrame(columns=['AC_id', 'xbf', 'xaf', 'leading'])
for acid in df['AC_id'].unique():
    idx = np.where(df['AC_id'] == acid)[0]

    xbf = xcarr[idx, 140:150].mean(axis=0).mean(axis=0)
    xaf = xcarr[idx, 151:161].mean(axis=0).mean(axis=0)
    lead = xbf > xaf
    du.loc[du.shape[0], :] = [acid, xbf, xaf, lead]

du.leading.value_counts()


# --- OPTION 5 -----------------------------------------------------------------
# look at peak times per unit
min_time = -50
max_time = 50
use_min_max_time = True

if use_min_max_time:
    pass
else:
    min_time = -150
    max_time = 150

du = pd.DataFrame(columns=['AC_id', 'peak_time', 'before'])
for acid in df['AC_id'].unique():
    idx = np.where(df['AC_id'] == acid)[0]

    peak_time = t[xcarr[idx, :].mean(axis=0).argmax()]
    before = peak_time < 0

    du.loc[du.shape[0], :] = [acid, peak_time, before]


du = du[(du['peak_time'] >= min_time) & (du['peak_time'] <= max_time)]

n_before = du.before.value_counts().loc[True]
n_after = du.before.value_counts().loc[False]

f, ax = plt.subplots(1, 1, figsize=[3, 3])
sns.distplot(du['peak_time'], hist=True, kde=False, ax=ax,
             bins=np.arange(min_time, max_time, 5), norm_hist=True)
ax.axvline(0, c='k', ls='--')
sns.despine()
plt.tight_layout()

plot_name = 'peak_hist.jpg'
f.savefig(os.path.join(plot_folder, plot_name), dpi=400)

from statsmodels.stats.proportion import proportions_ztest

stat, pval = proportions_ztest(n_before, nobs=n_before+n_after, value=0.5)



# --- OPTION 5 -----------------------------------------------------------------
# look at peak times per unit with bootstrap - count
min_time = -50
max_time = 50
use_min_max_time = True

if use_min_max_time:
    pass
else:
    min_time = -150
    max_time = 150

n_boots = 500
ac_units = df['AC_id'].unique()

dbo = pd.DataFrame(columns=['boot', 'n_before', 'n_after', 'n_tot'])

for n in range(n_boots):
    sel_ids = np.random.choice(ac_units, size=len(ac_units), replace=True)

    du = pd.DataFrame(columns=['AC_id', 'peak_time', 'before'])

    for acid in sel_ids:
        idx = np.where(df['AC_id'] == acid)[0]
        peak_time = t[xcarr[idx, :].mean(axis=0).argmax()]
        before = peak_time < 0
        du.loc[du.shape[0], :] = [acid, peak_time, before]

    du = du[(du['peak_time'] > min_time) & (du['peak_time'] < max_time)]
    n_before = du.before.value_counts().loc[True]
    n_after = du.before.value_counts().loc[False]

    dbo.loc[dbo.shape[0], :] = [n, n_before, n_after, n_before+n_after]


q_bef = dbo['n_before'].quantile(q=[0.05, 0.5, 0.95])
q_aft = dbo['n_after'].quantile(q=[0.05, 0.5, 0.95])

yerr = [[q_bef.loc[0.5] - q_bef.loc[0.05], q_aft.loc[0.5] - q_aft.loc[0.05]],
        [q_bef.loc[0.95] - q_bef.loc[0.5], q_aft.loc[0.95] - q_aft.loc[0.5]]]
yerr = np.array(yerr)


f, ax = plt.subplots(1, 1, figsize=[3, 3])

ax.bar(x=[0, 1], height=[q_bef.loc[0.5],q_aft.loc[0.5]], yerr=yerr)
ax.set_xticks([0, 1])
ax.set_xticklabels(['-50 to 0 ms', '0 to 50 ms'])
ax.set_ylabel('# of cells')
sns.despine()
plt.tight_layout()


plot_name = 'peak_bars.jpg'
f.savefig(os.path.join(plot_folder, plot_name), dpi=400)


# --- OPTION 5 -----------------------------------------------------------------
# look at peak times per unit with bootstrap - PERCENTAGE
min_time = -50
max_time = 50
use_min_max_time = True

if use_min_max_time:
    pass
else:
    min_time = -150
    max_time = 150

n_boots = 500
ac_units = df['AC_id'].unique()

dbo = pd.DataFrame(columns=['boot', 'perc_before', 'perc_after', 'n_tot'])

for n in range(n_boots):
    sel_ids = np.random.choice(ac_units, size=len(ac_units), replace=True)

    du = pd.DataFrame(columns=['AC_id', 'peak_time', 'before'])

    for acid in sel_ids:
        idx = np.where(df['AC_id'] == acid)[0]
        peak_time = t[xcarr[idx, :].mean(axis=0).argmax()]
        before = peak_time < 0
        du.loc[du.shape[0], :] = [acid, peak_time, before]

    du = du[(du['peak_time'] > min_time) & (du['peak_time'] < max_time)]
    n_before = du.before.value_counts().loc[True]
    n_after = du.before.value_counts().loc[False]
    n_tot = n_before + n_after
    perc_before = 100 * n_before / n_tot
    perc_after = 100 * n_after / n_tot

    dbo.loc[dbo.shape[0], :] = [n, perc_before, perc_after, n_tot]

q_bef = dbo['perc_before'].quantile(q=[0.05, 0.5, 0.95])
q_aft = dbo['perc_after'].quantile(q=[0.05, 0.5, 0.95])



yerr = [[q_bef.loc[0.5] - q_bef.loc[0.05], q_aft.loc[0.5] - q_aft.loc[0.05]],
        [q_bef.loc[0.95] - q_bef.loc[0.5], q_aft.loc[0.95] - q_aft.loc[0.5]]]
yerr = np.array(yerr)


f, ax = plt.subplots(1, 1, figsize=[3, 3])

ax.bar(x=[0, 1], height=[q_bef.loc[0.5],q_aft.loc[0.5]], yerr=yerr)
ax.set_xticks([0, 1])
ax.set_xticklabels(['-50 to 0 ms', '0 to 50 ms'])
ax.set_ylabel('% of cells')
sns.despine()
plt.tight_layout()

plot_name = 'peak_bars_percentage.jpg'
f.savefig(os.path.join(plot_folder, plot_name), dpi=400)







# --- OPTION 6 -----------------------------------------------------------------
# look at peak times per pair
min_time = -50
max_time = 50
use_min_max_time = True

if use_min_max_time:
    pass
else:
    min_time = -150
    max_time = 150


du = pd.DataFrame(columns=['AC_id', 'V2L_id', 'peak_time', 'before'])
for idx in np.arange(xcarr.shape[0]):

    acid = df['AC_id'].iloc[idx]
    v2id = df['V2L_id'].iloc[idx]
    peak_time = t[xcarr[idx, :].argmax()]
    before = peak_time < 0

    du.loc[du.shape[0], :] = [acid, v2id, peak_time, before]

du = du[(du['peak_time'] > -50) & (du['peak_time'] < 50)]

du.before.value_counts()


f, ax = plt.subplots(1, 1, figsize=[3, 3])
sns.distplot(du['peak_time'], hist=True, kde=False, ax=ax,
             bins=np.arange(-50, 50, 10), norm_hist=True)
ax.axvline(0, c='k', ls='--')
sns.despine()
plt.tight_layout()




