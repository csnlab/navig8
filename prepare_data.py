import numpy as np
from constants import ORIGINAL_DATA_PATH, DATA_FOLDER, ALL_SESSIONS
from constants import pixels_to_meters, spatial_bins_to_meters
import pickle
from loadmat import loadmat
import quantities as pq
import elephant
import os
import neo

navig8 = loadmat(ORIGINAL_DATA_PATH)

for sess_ind in ALL_SESSIONS:

    bin = pq.UnitQuantity('Spatial Bins', spatial_bins_to_meters * pq.m, symbol='bin')

    xy_times  = navig8['PaulsData'][sess_ind].XY[:, 0] * 1e-4 * pq.s
    xy_pos    = navig8['PaulsData'][sess_ind].XY[:, [1, 2]] * pixels_to_meters * pq.m
    xy_speed  = navig8['PaulsData'][sess_ind].XY[:, 3] * pq.m / pq.s
    hd_sincos = navig8['PaulsData'][sess_ind].XY[:, [4, 5]] * pq.dimensionless
    hd_change = navig8['PaulsData'][sess_ind].XY[:, [6, 7]] * pq.dimensionless
    lin_times = navig8['PaulsData'][sess_ind].Lin[:, 0] * 1e-4 * pq.s
    lin_xy    = navig8['PaulsData'][sess_ind].Lin[:, 1]
    lin_bin   = navig8['PaulsData'][sess_ind].Lin[:, 2]
    lin_bin   = lin_bin * bin
    lin_speed = navig8['PaulsData'][sess_ind].Lin[:, 3] * pq.m / pq.s


    event_names = navig8['PaulsData'][sess_ind].EventName[:-1]
    event_ts, event_labels = [], []

    for i, event_name in enumerate(event_names):
        ts = navig8['PaulsData'][sess_ind].EventTS[i]
        l = np.repeat(event_name, ts.shape[0])
        event_ts.append(ts)
        event_labels.append(l)

    event_ts = np.hstack(event_ts)
    event_labels = np.hstack(event_labels)

    # indx1 = np.where(event_names == 'Start Correct Left Trial')[0][0]
    # indx2 = np.where(event_names == 'End Correct Left Trial')[0][0]
    #
    #navig8['PaulsData'][sess_ind].EventTS[indx2] - navig8['PaulsData'][sess_ind].EventTS[indx1]
    #

    sort_index = np.argsort(event_ts)
    event_ts = event_ts[sort_index]
    event_labels = event_labels[sort_index]

    event_times = event_ts * 1e-4 * pq.s

    t_start, t_stop = xy_times[0], xy_times[-1]

    A1  = navig8['PaulsData'][sess_ind].A1
    V2L = navig8['PaulsData'][sess_ind].V2L

    seg = neo.Segment(t_start=t_start, t_stop=t_stop)

    spike_trains = []
    for area_label, area_data in zip(['A1', 'V2L'], [A1, V2L]):
        spikes = area_data.SpikeTS
        cell_ids = area_data.CellID
        for spike_times, id in zip(spikes, cell_ids):

            spike_times = spike_times * 1e-4 * pq.s

            spike_times = spike_times[(spike_times > t_start) &
                                      (spike_times < t_stop)]

            spike_times = spike_times.rescale(pq.ms)
            train = neo.SpikeTrain(t_start=t_start, t_stop=t_stop,
                           times=spike_times, CellID=id, area=area_label)
            spike_trains.append(train)


    sampling_period = np.median(np.diff(xy_times))

    xy_pos_signal = neo.IrregularlySampledSignal(t_start=t_start, t_stop=t_stop,
                                   times=xy_times, signal=xy_pos, name='xy_pos')

    xy_speed_signal = neo.IrregularlySampledSignal(t_start=t_start, t_stop=t_stop,
                                   times=xy_times, signal=xy_speed, name='xy_speed')

    hd_sincos_signal = neo.IrregularlySampledSignal(t_start=t_start, t_stop=t_stop,
                                   times=xy_times, signal=hd_sincos, name='hd_sincos')

    hd_change_signal = neo.IrregularlySampledSignal(t_start=t_start, t_stop=t_stop,
                                   times=xy_times, signal=hd_change, name='hd_change')

    lin_bin_signal = neo.IrregularlySampledSignal(t_start=t_start, t_stop=t_stop,
                                   times=xy_times, signal=lin_bin, name='lin_bin')

    lin_speed_signal = neo.IrregularlySampledSignal(t_start=t_start, t_stop=t_stop,
                                   times=xy_times, signal=lin_speed, name='lin_speed')

    neo_event = neo.Event(times=event_times, labels=event_labels)


    seg.spiketrains = spike_trains
    seg.analogsignals = [xy_pos_signal, xy_speed_signal,
                         lin_bin_signal, lin_speed_signal,
                         hd_sincos_signal, hd_change_signal]

    seg.events.append(neo_event)
    bl = neo.Block(name='Session {}'.format(sess_ind))
    bl.segments.append(seg)




    output_name = 'navig8_session_{}.pkl'.format(sess_ind)

    full_destination = os.path.join(DATA_FOLDER, output_name)
    print('Saving output to: {}'.format(full_destination))
    with open(full_destination, 'wb') as f:
        pickle.dump(bl, f)


